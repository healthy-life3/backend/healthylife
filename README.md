# HealthyLife

### Создание Docker образа
1. Сборка: `gradle :healthy-life-core:bootJar`
2. Создание образа: `docker build -t healthy-life .`

##Локальный запуск 

### Развертывание базы данных PostgreSQL для тестирования
1. Скачиваем Docker – [https://www.docker.com/products/docker-desktop](https://www.docker.com/products/docker-desktop)
2. Переходим в папку `doc/sample` и запускаем `docker-compose.yml`(либо через IntelliJ IDEA, либо через командную строку – `docker compose up`):
   `docker run --name some-postgres -p 5432:5432 -e POSTGRES_PASSWORD=mysecretpassword -d postgres`
3. Подключаемся к базе данных с помощью IntelliJ IDEA или DataGrip, используя следующие параметры:
- Host: **localhost**
- Port: **5432**
- User: **postgres**
- Password: **mysecretpassword**
- Database: **postgres**
- URL: **jdbc:postgresql://localhost:5432/postgres**

### Настройка конфигурации проекта
1. Создаем папку `config` в  корне проекта 
2. Берем из файл по пути `doc/sample/application.yml` и копируем в папку `config`

### REST API
Для получения REST API взаимодействия с HealthyLife запускаем проект и переходим по ссылке
– [localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html)
