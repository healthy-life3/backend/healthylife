package ru.healthyLife.web3j;

import org.web3j.protocol.Web3j;
import org.web3j.protocol.http.HttpService;
import ru.healthyLife.web3j.serverApi.util.ContractsExplorer;

import java.io.IOException;

public class Main {
    static final String URL = "http://localhost:8545/";

    public static void main(String[] args) throws Exception {
        Web3j web3j = Web3j.build(new HttpService(URL));
        ContractsExplorer contractsExplorer = new ContractsExplorer(web3j);
        contractsExplorer.init();
        web3j.shutdown();
    }
}
