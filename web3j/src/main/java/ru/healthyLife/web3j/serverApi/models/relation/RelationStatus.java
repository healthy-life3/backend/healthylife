package ru.healthyLife.web3j.serverApi.models.relation;

public enum RelationStatus{
    INACTIVE,
    ACTIVE,
    REQUESTED,
    INITIALED
}
