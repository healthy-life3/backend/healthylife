INSERT INTO role
VALUES (1, 'ROLE_DOCTOR');

INSERT INTO role
VALUES (2, 'ROLE_PATIENT');

INSERT INTO privilege
values (1, 'READ_MEDICAL_RECORDS');

INSERT INTO privilege
values (2, 'WRITE_MEDICAL_RECORDS');

INSERT INTO roles_privileges
values (1, 1);

INSERT INTO roles_privileges
values (2, 2);

INSERT INTO medical_organization
VALUES (1, 'ЦНМТ');

INSERT INTO medical_organization
VALUES (2, 'Клиника Мешалкина');

INSERT INTO international_classification_disease
VALUES ('4532', 'Гайморит');

INSERT INTO international_classification_disease
VALUES ('8729', 'Апендицит');

INSERT INTO international_classification_disease
VALUES ('3920', 'Артроз');

INSERT INTO international_classification_disease
VALUES ('2733', 'Covid-19');
