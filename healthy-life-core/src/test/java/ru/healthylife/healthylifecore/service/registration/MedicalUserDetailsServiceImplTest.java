package ru.healthylife.healthylifecore.service.registration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.util.ReflectionTestUtils;
import ru.healthylife.healthylifecore.domain.patient.PhoneNumberInfo;
import ru.healthylife.healthylifecore.domain.user.MedicalUserRegistrationInfo;
import ru.healthylife.healthylifecore.domain.user.RoleType;
import ru.healthylife.healthylifecore.entity.user.*;
import ru.healthylife.healthylifecore.mapper.MedicalUserMapper;
import ru.healthylife.healthylifecore.mapper.PhoneNumberMapper;
import ru.healthylife.healthylifecore.mapper.RoleMapper;
import ru.healthylife.healthylifecore.repository.MedicalUserRepository;
import ru.healthylife.healthylifecore.repository.RoleRepository;
import ru.healthylife.healthylifecore.service.registration.exceptions.UserAlreadyExistException;

import java.util.*;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class MedicalUserDetailsServiceImplTest {

    @InjectMocks
    private MedicalUserDetailsServiceImpl medicalUserDetailsService;

    @Mock
    private PasswordEncoder passwordEncoder;

    @Spy
    private MedicalUserMapper medicalUserMapper = Mappers.getMapper(MedicalUserMapper.class);

    @Spy
    private RoleMapper roleMapper = Mappers.getMapper(RoleMapper.class);

    @Mock
    private RoleRepository roleRepository;

    @Mock
    private MedicalUserRepository medicalUserRepository;

    private MedicalUser preExistedMedicalUser;

    @BeforeEach
    void setUp() {
        HashSet<Privilege> privileges = new HashSet<>();
        var privilege = new Privilege(PrivilegeName.READ_MEDICAL_RECORDS);
        privilege.setId(1L);
        privileges.add(privilege);

        HashSet<Role> roles = new HashSet<>();
        Role role = new Role();
        role.setId(1L);
        role.setName(RoleName.ROLE_PATIENT);
        role.setPrivileges(privileges);
        roles.add(role);
        preExistedMedicalUser = new MedicalUser();
        preExistedMedicalUser.setId(1L);
        preExistedMedicalUser.setEmail("ptushkin@gmail.com");
        preExistedMedicalUser.setPassword("password");
        preExistedMedicalUser.setRoles(roles);
        ReflectionTestUtils.setField(medicalUserMapper, "phoneNumberMapper", Mappers.getMapper(PhoneNumberMapper.class));
        ReflectionTestUtils.setField(medicalUserMapper, "roleMapper", roleMapper);
    }

    @Test
    void should_ThrowUsernameNotFoundException_When_LoadUserWithEmailThatDoesntExist() {
        when(medicalUserRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        Assertions.assertThrows(UsernameNotFoundException.class, () ->
                medicalUserDetailsService.loadUserByUsername("ethereum@gmail.com")
        );
    }

    @Test
    void should_GetMedicalUser_When_LoadUserByEmail() {
        when(medicalUserRepository.findByEmail(anyString())).thenReturn(Optional.of(preExistedMedicalUser));

        UserDetails userDetails = medicalUserDetailsService.loadUserByUsername("ptushkin@gmail.com");

        assertThat(userDetails.getUsername(), is("ptushkin@gmail.com"));
        assertThat(userDetails.getPassword(), is("password"));
        Collection<? extends GrantedAuthority> authorities = userDetails.getAuthorities();
        assertThat(authorities, hasSize(1));
        String authority = authorities.stream().findFirst().get().getAuthority();
        assertThat(authority, is(RoleName.ROLE_PATIENT.name()));
    }

    @Test
    void should_ThrowUserAlreadyExistException_When_NewMedicalUserWithoutRoles() {
        var newMedicalUserInfo = new MedicalUserRegistrationInfo();
        newMedicalUserInfo.setEmail("ptushkin@gmail.com");

        when(medicalUserRepository.existsByEmail(anyString())).thenReturn(true);

        Assertions.assertThrows(
                UserAlreadyExistException.class,
                () -> medicalUserDetailsService.registerNewMedicalUser(newMedicalUserInfo),
                "User has already existed:ptushkin@gmail.com"
        );
    }

    @Test
    void should_ThrowIllegalArgumentException_When_NewMedicalUserWithoutRoles() {
        var newMedicalUserInfo = new MedicalUserRegistrationInfo();
        newMedicalUserInfo.setEmail("ptushkin@gmail.com");

        when(medicalUserRepository.existsByEmail(anyString())).thenReturn(false);

        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> medicalUserDetailsService.registerNewMedicalUser(newMedicalUserInfo),
                "User must has role"
        );
    }

    @Test
    void should_ThrowIllegalArgumentException_When_ExpectedRolesDoesntExitsInDatabase() {
        var newMedicalUserInfo = new MedicalUserRegistrationInfo();
        newMedicalUserInfo.setEmail("ptushkin@gmail.com");
        var roles = new HashSet<RoleType>(1);
        roles.add(RoleType.PATIENT);
        newMedicalUserInfo.setRoles(roles);

        when(medicalUserRepository.existsByEmail(anyString())).thenReturn(false);

        Assertions.assertThrows(
                IllegalArgumentException.class,
                () -> medicalUserDetailsService.registerNewMedicalUser(newMedicalUserInfo)
        );
    }

    @Test
    void should_RegisteredNewMedicalUser_When_RegisterNewMedicalUser() throws UserAlreadyExistException {
        var newMedicalUserInfo = new MedicalUserRegistrationInfo();
        newMedicalUserInfo.setEmail("ptushkin@gmail.com");
        newMedicalUserInfo.setPassword("password");
        newMedicalUserInfo.setGender(Gender.MALE);
        newMedicalUserInfo.setLastName("Птушкин");
        newMedicalUserInfo.setFirstName("Андрей");
        newMedicalUserInfo.setMiddleName("Павлович");
        var phoneNumber = new PhoneNumberInfo();
        phoneNumber.setFullNumber("79131234567");
        phoneNumber.setSubscriberNumber("9131234567");
        phoneNumber.setCountryCode("7");
        newMedicalUserInfo.setPhoneNumber(phoneNumber);
        HashSet<RoleType> roles = new HashSet<>();
        roles.add(RoleType.PATIENT);
        newMedicalUserInfo.setRoles(roles);
        var patientRole = new Role();
        patientRole.setId(1L);
        patientRole.setName(RoleName.ROLE_PATIENT);

        when(medicalUserRepository.existsByEmail(anyString())).thenReturn(false);
        when(roleRepository.findByNameIn(List.of(RoleName.ROLE_PATIENT))).thenReturn(Set.of(patientRole));
        when(medicalUserRepository.save(Mockito.any(MedicalUser.class))).thenAnswer(i -> i.getArguments()[0]);
        when(passwordEncoder.encode(anyString())).thenAnswer(i -> i.getArguments()[0]);

        var medicalUserInfo = medicalUserDetailsService.registerNewMedicalUser(newMedicalUserInfo);

        assertThat(medicalUserInfo, notNullValue());
        assertThat(medicalUserInfo.getEmail(), is("ptushkin@gmail.com"));
        assertThat(medicalUserInfo.getFirstName(), is("Андрей"));
        assertThat(medicalUserInfo.getLastName(), is("Птушкин"));
        assertThat(medicalUserInfo.getMiddleName(), is("Павлович"));
        assertThat(medicalUserInfo.getRoles(), hasItem(RoleType.PATIENT));
    }

    @Test
    void should_ThrowUserNotFoundException_When_AddRoleForNonExistMedicalUser() {
        when(medicalUserRepository.findByEmail(anyString())).thenReturn(Optional.empty());

        Assertions.assertThrows(
                UsernameNotFoundException.class,
                () -> medicalUserDetailsService.addRoleForMedicalUser("ptushkin@gmail.com", RoleType.PATIENT)
        );
    }

    @Test
    void should_AddedNewRoleForMedicalUser_When_AddRoleForMedicalUser() {
        var patientUser = new MedicalUser();
        patientUser.setEmail("ptushkin@gmail.com");
        var patientRoles = new HashSet<Role>(1);
        var patientRole = new Role();
        patientRoles.add(patientRole);
        patientUser.setRoles(patientRoles);
        var doctorRoleInDatabase = new Role();
        doctorRoleInDatabase.setName(RoleName.ROLE_DOCTOR);

        when(medicalUserRepository.findByEmail(anyString())).thenReturn(Optional.of(patientUser));
        when(roleRepository.getRoleByName(RoleName.ROLE_DOCTOR)).thenReturn(doctorRoleInDatabase);
        when(medicalUserRepository.save(Mockito.any(MedicalUser.class))).thenAnswer(i -> i.getArguments()[0]);

        var medicalUserInfo = medicalUserDetailsService.addRoleForMedicalUser("ptushkin@gmail.com", RoleType.DOCTOR);

        assertThat(medicalUserInfo.getRoles(), hasSize(2));
        assertThat(medicalUserInfo.getRoles(), hasItem(RoleType.DOCTOR));
    }
}