package ru.healthylife.healthylifecore.service.registration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.healthylife.healthylifecore.domain.doctor.DoctorRegistrationInfo;
import ru.healthylife.healthylifecore.domain.patient.PhoneNumberInfo;
import ru.healthylife.healthylifecore.domain.user.MedicalUserInfo;
import ru.healthylife.healthylifecore.domain.user.MedicalUserRegistrationInfo;
import ru.healthylife.healthylifecore.domain.user.RoleType;
import ru.healthylife.healthylifecore.entity.doctor.Doctor;
import ru.healthylife.healthylifecore.entity.doctor.DoctorProfession;
import ru.healthylife.healthylifecore.entity.ehr.MedicalOrganization;
import ru.healthylife.healthylifecore.entity.user.Gender;
import ru.healthylife.healthylifecore.entity.user.MedicalUser;
import ru.healthylife.healthylifecore.mapper.DoctorMapper;
import ru.healthylife.healthylifecore.mapper.MedicalUserMapper;
import ru.healthylife.healthylifecore.repository.DoctorProfessionRepository;
import ru.healthylife.healthylifecore.repository.DoctorRepository;
import ru.healthylife.healthylifecore.repository.MedicalOrganizationRepository;
import ru.healthylife.healthylifecore.repository.MedicalUserRepository;
import ru.healthylife.healthylifecore.service.registration.exceptions.UserAlreadyExistException;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class DoctorRegistrationServiceTest {

    @InjectMocks
    private DoctorRegistrationService doctorRegistrationService;

    @Spy
    private DoctorMapper doctorMapper = Mappers.getMapper(DoctorMapper.class);

    @Spy
    private MedicalUserMapper medicalUserMapper = Mappers.getMapper(MedicalUserMapper.class);

    @Mock
    private DoctorRepository doctorRepository;

    @Mock
    private MedicalUserRepository medicalUserRepository;

    @Mock
    private MedicalUserDetailsService medicalUserDetailsService;

    @Mock
    private DoctorProfessionRepository doctorProfessionRepository;

    @Mock
    private MedicalOrganizationRepository medicalOrganizationRepository;

    @Test
    void should_ThrowUserAlreadyExistException_When_RegisterExistedDoctor() {
        DoctorRegistrationInfo newMedicalUser = getDoctorRegistrationInfo();

        var registeredMedicalUser = new MedicalUserInfo();
        registeredMedicalUser.setEmail("doctor@gmail.com");
        var roles = new HashSet<RoleType>();
        roles.add(RoleType.DOCTOR);
        registeredMedicalUser.setRoles(roles);

        when(medicalUserDetailsService.findByEmail("doctor@gmail.com")).thenReturn(Optional.of(registeredMedicalUser));

        Assertions.assertThrows(
                UserAlreadyExistException.class,
                () -> doctorRegistrationService.registerNewMedicalUser(newMedicalUser)
        );
    }

    @Test
    void should_RegisterNewDoctorUser_When_RegisterNewDoctor() throws UserAlreadyExistException {
        DoctorRegistrationInfo doctorRegistrationInfo = getDoctorRegistrationInfo();
        var medicalUserInfo = new MedicalUserInfo();
        var roles = new HashSet<RoleType>();
        roles.add(RoleType.DOCTOR);
        medicalUserInfo.setId(1L);
        medicalUserInfo.setRoles(roles);
        medicalUserInfo.setEmail("doctor@gmail.com");
        var medicalUser = new MedicalUser();
        medicalUser.setId(1L);
        medicalUser.setEmail("doctor@gmail.com");

        when(medicalUserDetailsService.findByEmail("doctor@gmail.com")).thenReturn(Optional.empty());
        when(medicalUserDetailsService.registerNewMedicalUser(any(MedicalUserRegistrationInfo.class)))
                .thenReturn(medicalUserInfo);
        when(medicalUserRepository.getById(1L)).thenReturn(medicalUser);
        var doctorProfession = new DoctorProfession();
        doctorProfession.setId(1L);
        doctorProfession.setName("Хирург");
        when(doctorProfessionRepository.findById(1L)).thenReturn(Optional.of(doctorProfession));
        var medicalOrganization = new MedicalOrganization();
        medicalOrganization.setId(1L);
        medicalOrganization.setName("INVITRO");
        when(medicalOrganizationRepository.findById(1L)).thenReturn(Optional.of(medicalOrganization));

        var registeredNewMedicalUser = doctorRegistrationService.registerNewMedicalUser(doctorRegistrationInfo);

        verify(doctorRepository).save(any(Doctor.class));

        assertThat(registeredNewMedicalUser.getEmail(), is("doctor@gmail.com"));
    }

    @Nonnull
    private DoctorRegistrationInfo getDoctorRegistrationInfo() {
        var newMedicalUser = new DoctorRegistrationInfo();
        newMedicalUser.setEmail("doctor@gmail.com");
        newMedicalUser.setGender(Gender.MALE);
        newMedicalUser.setPassword("password");
        var phoneNumber = new PhoneNumberInfo();
        phoneNumber.setFullNumber("7123456789");
        newMedicalUser.setFirstName("Иван");
        newMedicalUser.setLastName("Иванов");
        newMedicalUser.setPhoneNumber(phoneNumber);
        newMedicalUser.setBirthday(LocalDate.now().minusYears(30));
        newMedicalUser.setDoctorProfessionId(1L);
        newMedicalUser.setMedicalOrganizationId(1L);
        return newMedicalUser;
    }
}