package ru.healthylife.healthylifecore.service.registration;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mapstruct.factory.Mappers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;
import ru.healthylife.healthylifecore.domain.patient.PatientRegistrationInfo;
import ru.healthylife.healthylifecore.domain.patient.PhoneNumberInfo;
import ru.healthylife.healthylifecore.domain.user.MedicalUserInfo;
import ru.healthylife.healthylifecore.domain.user.MedicalUserRegistrationInfo;
import ru.healthylife.healthylifecore.domain.user.RoleType;
import ru.healthylife.healthylifecore.entity.patient.Patient;
import ru.healthylife.healthylifecore.entity.user.Gender;
import ru.healthylife.healthylifecore.entity.user.MedicalUser;
import ru.healthylife.healthylifecore.mapper.MedicalUserMapper;
import ru.healthylife.healthylifecore.mapper.PatientMapper;
import ru.healthylife.healthylifecore.repository.MedicalUserRepository;
import ru.healthylife.healthylifecore.repository.PatientRepository;
import ru.healthylife.healthylifecore.service.registration.exceptions.UserAlreadyExistException;

import javax.annotation.Nonnull;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class PatientRegistrationServiceTest {

    @InjectMocks
    private PatientRegistrationService patientRegistrationService;

    @Spy
    private PatientMapper patientMapper = Mappers.getMapper(PatientMapper.class);

    @Spy
    private MedicalUserMapper medicalUserMapper = Mappers.getMapper(MedicalUserMapper.class);

    @Mock
    private PatientRepository patientRepository;

    @Mock
    private MedicalUserRepository medicalUserRepository;

    @Mock
    private MedicalUserDetailsService medicalUserDetailsService;

    @Test
    void should_ThrowUserAlreadyExistException_When_RegisterExistedPatient() {
        PatientRegistrationInfo newMedicalUser = getPatientRegistrationInfo();

        var registeredMedicalUser = new MedicalUserInfo();
        registeredMedicalUser.setEmail("patient@gmail.com");
        var roles = new HashSet<RoleType>();
        roles.add(RoleType.PATIENT);
        registeredMedicalUser.setRoles(roles);

        when(medicalUserDetailsService.findByEmail("patient@gmail.com")).thenReturn(Optional.of(registeredMedicalUser));

        Assertions.assertThrows(
                UserAlreadyExistException.class,
                () -> patientRegistrationService.registerNewMedicalUser(newMedicalUser)
        );
    }

    @Test
    void should_RegisterNewPatientUser_When_RegisterNewPatient() throws UserAlreadyExistException {
        PatientRegistrationInfo patientRegistrationInfo = getPatientRegistrationInfo();
        var medicalUserInfo = new MedicalUserInfo();
        var roles = new HashSet<RoleType>();
        roles.add(RoleType.PATIENT);
        medicalUserInfo.setId(1L);
        medicalUserInfo.setRoles(roles);
        medicalUserInfo.setEmail("patient@gmail.com");
        var medicalUser = new MedicalUser();
        medicalUser.setId(1L);
        medicalUser.setEmail("patient@gmail.com");

        when(medicalUserDetailsService.findByEmail("patient@gmail.com")).thenReturn(Optional.empty());
        when(medicalUserDetailsService.registerNewMedicalUser(any(MedicalUserRegistrationInfo.class)))
                .thenReturn(medicalUserInfo);
        when(medicalUserRepository.getById(1L)).thenReturn(medicalUser);
        when(patientRepository.existsBySnils(anyString())).thenReturn(false);

        var registeredNewMedicalUser = patientRegistrationService.registerNewMedicalUser(patientRegistrationInfo);

        verify(patientRepository).save(any(Patient.class));

        assertThat(registeredNewMedicalUser.getEmail(), is("patient@gmail.com"));
    }

    @Test
    void should_ThrowUserAlreadyExistException_When_PatientsSnilsHasExisted() throws UserAlreadyExistException {
        PatientRegistrationInfo patientRegistrationInfo = getPatientRegistrationInfo();
        var medicalUserInfo = new MedicalUserInfo();
        var roles = new HashSet<RoleType>();
        roles.add(RoleType.PATIENT);
        medicalUserInfo.setId(1L);
        medicalUserInfo.setRoles(roles);
        medicalUserInfo.setEmail("patient@gmail.com");
        var medicalUser = new MedicalUser();
        medicalUser.setId(1L);
        medicalUser.setEmail("patient@gmail.com");

        when(medicalUserDetailsService.findByEmail("patient@gmail.com")).thenReturn(Optional.empty());
        when(medicalUserDetailsService.registerNewMedicalUser(any(MedicalUserRegistrationInfo.class)))
                .thenReturn(medicalUserInfo);
        when(medicalUserRepository.getById(1L)).thenReturn(medicalUser);
        when(patientRepository.existsBySnils(anyString())).thenReturn(true);

        Assertions.assertThrows(
                UserAlreadyExistException.class,
                () -> patientRegistrationService.registerNewMedicalUser(patientRegistrationInfo)
        );
    }

    @Nonnull
    private PatientRegistrationInfo getPatientRegistrationInfo() {
        var newMedicalUser = new PatientRegistrationInfo();
        newMedicalUser.setEmail("patient@gmail.com");
        newMedicalUser.setGender(Gender.MALE);
        newMedicalUser.setPassword("password");
        var phoneNumber = new PhoneNumberInfo();
        phoneNumber.setFullNumber("7123456789");
        newMedicalUser.setFirstName("Иван");
        newMedicalUser.setLastName("Иванов");
        newMedicalUser.setSnils("12345678901");
        newMedicalUser.setPhoneNumber(phoneNumber);
        newMedicalUser.setBirthday(LocalDate.now().minusYears(30));
        return newMedicalUser;
    }
}