package ru.healthylife.healthylifecore.controller;

import com.nimbusds.jose.JOSEException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import ru.healthylife.healthylifecore.config.WebConfiguration;
import ru.healthylife.healthylifecore.domain.auth.JwtTokenInfo;
import ru.healthylife.healthylifecore.domain.doctor.DoctorRegistrationInfo;
import ru.healthylife.healthylifecore.domain.patient.PatientRegistrationInfo;
import ru.healthylife.healthylifecore.domain.user.MedicalUserInfo;
import ru.healthylife.healthylifecore.domain.user.RoleType;
import ru.healthylife.healthylifecore.entity.user.PrivilegeName;
import ru.healthylife.healthylifecore.security.JwtProvider;
import ru.healthylife.healthylifecore.service.TokenService;
import ru.healthylife.healthylifecore.service.registration.DoctorRegistrationService;
import ru.healthylife.healthylifecore.service.registration.PatientRegistrationService;
import ru.healthylife.healthylifecore.service.registration.exceptions.UserAlreadyExistException;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Locale;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(AuthController.class)
@ContextConfiguration(classes = WebConfiguration.class)
class AuthControllerTest {
    protected static final String ACCESS_TOKEN = "access_token";
    protected static final String REFRESH_TOKEN = "refresh_token";
    protected static final String AUTHORIZATION = "Authorization";
    protected static final String BASIC = "Basic ";

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @MockBean
    private DoctorRegistrationService doctorRegistrationService;

    @MockBean
    private PatientRegistrationService patientRegistrationService;

    @MockBean
    private JwtProvider jwtProvider;

    @MockBean
    private UserDetailsService userDetailsService;

    @MockBean
    private TokenService tokenService;

    @BeforeEach
    void setUp() throws JOSEException {
        var jwtTokenInfo = new JwtTokenInfo(ACCESS_TOKEN, REFRESH_TOKEN);

        when(tokenService.getJwtTokens(any(Authentication.class))).thenReturn(jwtTokenInfo);
        when(tokenService.getJwtTokens(any(MedicalUserInfo.class))).thenReturn(jwtTokenInfo);
        when(tokenService.refresh(any(), anyString())).thenReturn(jwtTokenInfo);
        when(jwtProvider.generateAccessToken(anyString(), anyMap())).thenReturn(ACCESS_TOKEN);
    }

    @Test
    void should_GetToken_When_RegisterNewDoctor() throws Exception {
        var path = Paths.get(ClassLoader.getSystemResource("web/newDoctorRegistrationRequest.json").toURI());
        String doctorRegistrationRequest = Files.readString(path);

        var medicalUserInfo = new MedicalUserInfo();
        medicalUserInfo.setEmail("somemail@gmail.com");

        when(doctorRegistrationService.registerNewMedicalUser(any(DoctorRegistrationInfo.class))).thenReturn(medicalUserInfo);

        mockMvc.perform(
                        post(AuthController.SIGNUP_DOCTOR)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(doctorRegistrationRequest)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.accessToken", is(equalTo(ACCESS_TOKEN))))
                .andExpect(jsonPath("$.refreshToken", is(equalTo(REFRESH_TOKEN))));
    }

    @Test
    void should_GetBadRequest_When_RegistrationNewDoctorRequestInvalid() throws Exception {
        var path = Paths.get(ClassLoader.getSystemResource("web/invalidNewDoctorRegistrationRequest.json").toURI());
        String doctorRegistrationRequest = Files.readString(path);

        mockMvc.perform(
                        post(AuthController.SIGNUP_DOCTOR)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(doctorRegistrationRequest)
                )
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.firstName", is(equalTo("must not be blank"))))
                .andExpect(jsonPath("$.lastName", is(equalTo("must not be blank"))))
                .andExpect(jsonPath("$.['phoneNumber.fullNumber']", is(equalTo("size must be between 10 and 15"))))
                .andExpect(jsonPath("$.email", is(equalTo("must be a well-formed email address"))));
    }

    @Test
    void should_GetBadRequest_When_DoctorHasAlreadyExisted() throws Exception {
        var path = Paths.get(ClassLoader.getSystemResource("web/newDoctorRegistrationRequest.json").toURI());
        String doctorRegistrationRequest = Files.readString(path);

        when(doctorRegistrationService.registerNewMedicalUser(any(DoctorRegistrationInfo.class)))
                .thenThrow(new UserAlreadyExistException("User has already registered"));

        mockMvc.perform(
                        post(AuthController.SIGNUP_DOCTOR)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(doctorRegistrationRequest)
                )
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.error", is(equalTo("User has already registered"))));
    }

    @Test
    void should_GetToken_When_DoctorAuthenticate() throws Exception {

        String credentials = "petr@gmail.com:password";
        String credentialsInBase64 = Base64.getEncoder().encodeToString(credentials.getBytes());
        var userDetails = User.builder()
                .username("petr@gmail.com")
                .password(passwordEncoder.encode("password"))
                .authorities(PrivilegeName.WRITE_MEDICAL_RECORDS.toString())
                .roles(RoleType.DOCTOR.toString())
                .build();

        when(userDetailsService.loadUserByUsername("petr@gmail.com")).thenReturn(userDetails);

        mockMvc.perform(
                        post(AuthController.SIGNIN)
                                .header(AUTHORIZATION, BASIC + credentialsInBase64)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.accessToken", is(equalTo(ACCESS_TOKEN))))
                .andExpect(jsonPath("$.refreshToken", is(equalTo(REFRESH_TOKEN))));
    }

    @Test
    void should_GetBadRequest_When_RegistrationNewPatientRequestInvalid() throws Exception {
        var path = Paths.get(ClassLoader.getSystemResource("web/invalidNewPatientRegistrationRequest.json").toURI());
        String doctorRegistrationRequest = Files.readString(path);

        mockMvc.perform(
                        post(AuthController.SIGNUP_PATIENT)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(doctorRegistrationRequest)
                                .locale(Locale.ENGLISH)
                )
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.firstName", is(equalTo("must not be blank"))))
                .andExpect(jsonPath("$.lastName", is(equalTo("must not be blank"))))
                .andExpect(jsonPath("$.snils", is(equalTo("Snils's size must has 11 digits"))))
                .andExpect(jsonPath("$.['phoneNumber.fullNumber']", is(equalTo("size must be between 10 and 15"))))
                .andExpect(jsonPath("$.email", is(equalTo("must be a well-formed email address"))));
    }

    @Test
    void should_GetBadRequest_When_PatientHasAlreadyExisted() throws Exception {
        var path = Paths.get(ClassLoader.getSystemResource("web/newPatientRegistrationRequest.json").toURI());
        String doctorRegistrationRequest = Files.readString(path);

        when(patientRegistrationService.registerNewMedicalUser(any(PatientRegistrationInfo.class)))
                .thenThrow(new UserAlreadyExistException("User has already registered"));

        mockMvc.perform(
                        post(AuthController.SIGNUP_PATIENT)
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(doctorRegistrationRequest)
                )
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.error", is(equalTo("User has already registered"))));
    }

    @Test
    void should_GetToken_When_PatientAuthenticate() throws Exception {

        String credentials = "petr@gmail.com:password";
        String credentialsInBase64 = Base64.getEncoder().encodeToString(credentials.getBytes());
        var userDetails = User.builder()
                .username("petr@gmail.com")
                .password(passwordEncoder.encode("password"))
                .authorities(PrivilegeName.WRITE_MEDICAL_RECORDS.toString())
                .roles(RoleType.PATIENT.toString())
                .build();

        when(userDetailsService.loadUserByUsername("petr@gmail.com")).thenReturn(userDetails);

        mockMvc.perform(
                        post(AuthController.SIGNIN)
                                .header(AUTHORIZATION, BASIC + credentialsInBase64)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.accessToken", is(equalTo(ACCESS_TOKEN))))
                .andExpect(jsonPath("$.refreshToken", is(equalTo(REFRESH_TOKEN))));
    }
}