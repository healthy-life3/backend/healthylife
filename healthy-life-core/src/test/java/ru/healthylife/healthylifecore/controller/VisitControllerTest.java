package ru.healthylife.healthylifecore.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import ru.healthylife.healthylifecore.config.WebConfiguration;
import ru.healthylife.healthylifecore.exception.NotFoundEntityException;
import ru.healthylife.healthylifecore.service.VisitService;

import java.util.Locale;
import java.util.UUID;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(VisitController.class)
@ContextConfiguration(classes = WebConfiguration.class)
class VisitControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private VisitService visitService;

    @Test
    @WithMockUser
    void should_GetNotFoundVisitError_When_VisitWithProvideUUIDDoesntExist() throws Exception {
        when(visitService.findVisitByUuid(any())).thenThrow(new NotFoundEntityException("error.visit.notfound"));

        mockMvc.perform(
                        get(VisitController.VISITS_PATH + "/" + UUID.randomUUID())
                                .contentType(MediaType.APPLICATION_JSON)
                                .locale(Locale.forLanguageTag("ru"))
                )
                .andDo(print())
                .andExpect(status().is4xxClientError())
                .andExpect(jsonPath("$.error", is(equalTo("Прием не был найден"))));
    }
}