package ru.healthylife.healthylifecore.controller;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import ru.healthylife.healthylifecore.config.WebConfiguration;
import ru.healthylife.healthylifecore.domain.doctor.DoctorInfo;
import ru.healthylife.healthylifecore.domain.ehr.FileInfo;
import ru.healthylife.healthylifecore.domain.ehr.MedicalRecordFileInfo;
import ru.healthylife.healthylifecore.service.MedicalRecordFileService;

import java.time.Clock;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.UUID;

import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;
import static ru.healthylife.healthylifecore.controller.MedicalRecordFileController.*;

@Import(WebConfiguration.class)
@WebMvcTest(MedicalRecordFileController.class)
class MedicalRecordFileControllerTest {
    protected static final String MEDICAL_RECORD_INFO_REQUEST_PART = "{\"patientEmail\": \"ivanov@gmail.com\", \"title\":\"Blood test\"}";
    protected static final String PDF_FILE_STRING = "PDF FILE STRING";
    protected static final String MEDICAL_RECORD_UUID = "6f39d5de-f842-4642-9b56-18794177c321";
    protected static final String TITLE = "Blood test";
    protected static final String MEDICAL_ORGANIZATION = "INVITRO";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private MedicalRecordFileService medicalRecordFileService;

    @Test
    @WithMockUser(username = "doctor@gmail.com", roles = "DOCTOR")
    void should_UploadNewMedicalRecordFileSuccessfully() throws Exception {
        var medicalRecord = new MockMultipartFile(
                "medicalRecord",
                null,
                MediaType.APPLICATION_JSON_VALUE,
                MEDICAL_RECORD_INFO_REQUEST_PART.getBytes()
        );
        var medicalRecordFile = new MockMultipartFile(
                "file",
                "ivanovBloodTest.pdf",
                MediaType.APPLICATION_PDF_VALUE,
                PDF_FILE_STRING.getBytes()
        );
        var medicalRecordFileInfo = new MedicalRecordFileInfo();
        medicalRecordFileInfo.setMedicalRecordUuid(UUID.fromString(MEDICAL_RECORD_UUID));
        var clock = Clock.fixed(Instant.parse("2022-02-02T10:00:00.001Z"), ZoneOffset.UTC);
        medicalRecordFileInfo.setDateTime(LocalDateTime.now(clock));
        medicalRecordFileInfo.setTitle(TITLE);
        medicalRecordFileInfo.setMedicalOrganization(MEDICAL_ORGANIZATION);
        var doctor = new DoctorInfo();
        medicalRecordFileInfo.setDoctor(doctor);
        var file = new FileInfo();
        file.setType(MediaType.APPLICATION_PDF_VALUE);
        file.setData(PDF_FILE_STRING.getBytes());
        medicalRecordFileInfo.setFile(file);

        when(medicalRecordFileService.createNewMedicalRecord(any())).thenReturn(medicalRecordFileInfo);

        mockMvc.perform(
                        multipart(MEDICAL_RECORD_FILES + UPLOAD)
                                .file(medicalRecord)
                                .file(medicalRecordFile)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.title", is(equalTo(TITLE))))
                .andExpect(jsonPath("$.medicalRecordUuid", is(equalTo(MEDICAL_RECORD_UUID))))
                .andExpect(jsonPath("$.dateTime", is(equalTo("2022-02-02T10:00:00"))))
                .andExpect(jsonPath("$.medicalOrganization", is(equalTo(MEDICAL_ORGANIZATION))))
                .andExpect(jsonPath("$.fileMetaData.type", is(equalTo(MediaType.APPLICATION_PDF_VALUE))))
                .andExpect(jsonPath("$.fileMetaData.name", nullValue()))
                .andExpect(jsonPath("$.fileMetaData.path", is(equalTo("http://localhost/download/6f39d5de-f842-4642-9b56-18794177c321"))));
    }

    @Test
    @WithMockUser(username = "ivanov@gmail.com", roles = "PATIENT")
    void should_DownloadMedicalRecordFileInfo() throws Exception {

        var uuid = UUID.fromString(MEDICAL_RECORD_UUID);

        var medicalRecordFileInfo = new MedicalRecordFileInfo();
        var file = new FileInfo();
        file.setName("ivanovBloodTest.pdf");
        file.setType(MediaType.APPLICATION_PDF_VALUE);
        file.setData(PDF_FILE_STRING.getBytes());
        medicalRecordFileInfo.setFile(file);

        when(medicalRecordFileService.getMedicalRecordFileInfo(uuid)).thenReturn(medicalRecordFileInfo);

        mockMvc.perform(get(MEDICAL_RECORD_FILES + DOWNLOAD + "/" + MEDICAL_RECORD_UUID))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_PDF_VALUE))
                .andExpect(header().string(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"ivanovBloodTest.pdf\""))
        ;
    }
}