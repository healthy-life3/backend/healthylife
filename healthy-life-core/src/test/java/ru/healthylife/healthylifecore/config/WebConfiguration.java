package ru.healthylife.healthylifecore.config;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import ru.healthylife.healthylifecore.configuration.MessageLocalizedConfiguration;

@TestConfiguration
@ComponentScan("ru.healthylife.healthylifecore.mapper")
@Import(value = {WhitelistSecurityConfig.class, MessageLocalizedConfiguration.class, JwtTokensConfig.class})
public class WebConfiguration {
}
