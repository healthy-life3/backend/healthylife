package ru.healthylife.healthylifecore.repository;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import ru.healthylife.healthylifecore.entity.user.Role;
import ru.healthylife.healthylifecore.entity.user.RoleName;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;

@DataJpaTest
class RoleRepositoryTest {

    @Autowired
    private RoleRepository roleRepository;

    @Test
    void testGetAllRoleInName() {
        var roleNames = new ArrayList<RoleName>(2);
        roleNames.add(RoleName.ROLE_DOCTOR);
        roleNames.add(RoleName.ROLE_PATIENT);

        var rolesInDatabase = roleRepository.findByNameIn(roleNames);

        assertThat(rolesInDatabase, hasSize(2));
        List<RoleName> roleNamesInDb = rolesInDatabase.stream().map(Role::getName).collect(Collectors.toList());
        assertThat(roleNamesInDb, hasSize(2));
        assertThat(roleNamesInDb, hasItem(RoleName.ROLE_DOCTOR));
        assertThat(roleNamesInDb, hasItem(RoleName.ROLE_PATIENT));
    }
}