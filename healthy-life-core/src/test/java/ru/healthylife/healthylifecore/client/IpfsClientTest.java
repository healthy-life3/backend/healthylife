package ru.healthylife.healthylifecore.client;

import feign.Response;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import ru.healthylife.healthylifecore.dto.ipfs.responses.AddResponse;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

@SpringBootTest
@Testcontainers
class IpfsClientTest {

    @Container
    private final static GenericContainer ipfsContainer = new GenericContainer(
            DockerImageName.parse("ipfs/go-ipfs")
    )
            .withExposedPorts(5001);

    @Autowired
    private IpfsClient ipfsClient;

    @DynamicPropertySource
    static void overrideProperties(DynamicPropertyRegistry dynamicPropertyRegistry) {
        String url = ipfsContainer.getHost() + ":" + ipfsContainer.getMappedPort(5001);
        dynamicPropertyRegistry.add("feign.ipfs.host", () -> url);
    }

    @Test
    void should_AddAndGetFileFromIpfs() throws IOException {
        String fileName = "file.txt";
        String originalString = "text";

        MockMultipartFile multipartFile = new MockMultipartFile(
                "path",
                fileName,
                MediaType.APPLICATION_JSON_VALUE,
                originalString.getBytes()
        );
        AddResponse addResponse = ipfsClient.addFile(multipartFile);

        assertThat(addResponse, notNullValue());
        assertThat(addResponse.getHash(), notNullValue());
        assertThat(addResponse.getSize(), notNullValue());
        assertThat(addResponse.getName(), is(fileName));

        Response response = ipfsClient.getFile(addResponse.getHash());
        var targetString = new String(response.body().asInputStream().readAllBytes(), StandardCharsets.UTF_8);

        assertThat(originalString, is(targetString));
    }
}