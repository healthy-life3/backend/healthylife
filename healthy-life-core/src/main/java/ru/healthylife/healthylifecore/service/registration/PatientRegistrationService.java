package ru.healthylife.healthylifecore.service.registration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.healthylife.healthylifecore.domain.patient.PatientRegistrationInfo;
import ru.healthylife.healthylifecore.domain.user.MedicalUserRegistrationInfo;
import ru.healthylife.healthylifecore.domain.user.RoleType;
import ru.healthylife.healthylifecore.entity.patient.Patient;
import ru.healthylife.healthylifecore.entity.user.MedicalUser;
import ru.healthylife.healthylifecore.mapper.MedicalUserMapper;
import ru.healthylife.healthylifecore.mapper.PatientMapper;
import ru.healthylife.healthylifecore.repository.MedicalUserRepository;
import ru.healthylife.healthylifecore.repository.PatientRepository;
import ru.healthylife.healthylifecore.service.registration.exceptions.UserAlreadyExistException;

import javax.annotation.Nonnull;

@Slf4j
@Service
public class PatientRegistrationService extends AbstractRegistrationService<PatientRegistrationInfo> {

    private final PatientMapper patientMapper;
    private final MedicalUserMapper medicalUserMapper;
    private final PatientRepository patientRepository;

    public PatientRegistrationService(
            PatientMapper patientMapper,
            MedicalUserMapper medicalUserMapper,
            PatientRepository patientRepository,
            MedicalUserRepository medicalUserRepository,
            MedicalUserDetailsService medicalUserDetailsService
    ) {
        super(medicalUserDetailsService, medicalUserRepository);
        this.patientMapper = patientMapper;
        this.medicalUserMapper = medicalUserMapper;
        this.patientRepository = patientRepository;
    }

    @Override
    protected void saveNewMedicalUser(
            @Nonnull PatientRegistrationInfo patientRegistrationInfo,
            @Nonnull MedicalUser medicalUser
    ) throws UserAlreadyExistException {
        validateNewPatientRequest(patientRegistrationInfo);
        Patient patient = patientMapper.convert(patientRegistrationInfo);
        patient.setMedicalUser(medicalUser);
        patientRepository.save(patient);
        log.debug("Patient with id {} saved successfully", patient.getId());
    }

    private void validateNewPatientRequest(@Nonnull PatientRegistrationInfo patientRegistrationInfo) throws UserAlreadyExistException {
        var snils = patientRegistrationInfo.getSnils();
        if (patientRepository.existsBySnils(snils)) {
            throw new UserAlreadyExistException("Patient with such snils (" + snils + ") has already existed");
        }
    }

    @Nonnull
    @Override
    protected RoleType getNewRole() {
        return RoleType.PATIENT;
    }

    @Nonnull
    @Override
    protected MedicalUserRegistrationInfo convertToMedicalUserRegistration(@Nonnull PatientRegistrationInfo patientRegistrationInfo) {
        return medicalUserMapper.convert(patientRegistrationInfo);
    }
}
