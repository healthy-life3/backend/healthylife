package ru.healthylife.healthylifecore.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.healthylife.healthylifecore.domain.ehr.LaboratoryTestInfo;
import ru.healthylife.healthylifecore.domain.ehr.NewLaboratoryTest;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecord;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecordType;
import ru.healthylife.healthylifecore.exception.NotFoundEntityException;
import ru.healthylife.healthylifecore.mapper.LaboratoryTestMapper;
import ru.healthylife.healthylifecore.repository.DoctorRepository;
import ru.healthylife.healthylifecore.repository.LaboratoryTestRepository;
import ru.healthylife.healthylifecore.repository.PatientRepository;

import javax.annotation.Nonnull;
import java.time.Clock;
import java.util.UUID;

@Service
@Transactional
public class LaboratoryTestService extends AbstractMedicalRecordService<NewLaboratoryTest, LaboratoryTestInfo> {

    private final LaboratoryTestMapper laboratoryTestMapper;
    private final LaboratoryTestRepository laboratoryTestRepository;

    public LaboratoryTestService(
            Clock clock,
            DoctorRepository doctorRepository,
            PatientRepository patientRepository,
            LaboratoryTestMapper laboratoryTestMapper,
            LaboratoryTestRepository laboratoryTestRepository
    ) {
        super(clock, doctorRepository, patientRepository);
        this.laboratoryTestMapper = laboratoryTestMapper;
        this.laboratoryTestRepository = laboratoryTestRepository;
    }

    @Nonnull
    @Transactional(readOnly = true)
    public LaboratoryTestInfo getLaboratoryTestByUuid(@Nonnull UUID uuid) {
        var laboratoryTest = laboratoryTestRepository.findById(uuid)
                .orElseThrow(() -> new NotFoundEntityException("error.laboratorytest.notfound"));
        return laboratoryTestMapper.convertToDomain(laboratoryTest);
    }

    @Nonnull
    @Override
    protected MedicalRecordType getMedicalRecordType() {
        return MedicalRecordType.LAB;
    }

    @Nonnull
    @Override
    protected LaboratoryTestInfo saveDetails(@Nonnull NewLaboratoryTest newMedicalRecordInfo, @Nonnull MedicalRecord medicalRecord) {
        var laboratoryTest = laboratoryTestMapper.convertToEntity(newMedicalRecordInfo);
        laboratoryTest.setMedicalRecord(medicalRecord);
        var createdVisit = laboratoryTestRepository.save(laboratoryTest);
        return laboratoryTestMapper.convertToDomain(createdVisit);
    }
}
