package ru.healthylife.healthylifecore.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.healthylife.healthylifecore.domain.ehr.IcdInfo;
import ru.healthylife.healthylifecore.mapper.IcdMapper;
import ru.healthylife.healthylifecore.repository.IcdRepository;

import javax.annotation.Nonnull;
import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class IcdService {
    private final IcdMapper icdMapper;
    private final IcdRepository icdRepository;

    @Nonnull
    public List<IcdInfo> findAllIcds() {
        return icdMapper.convertToDomain(icdRepository.findAll());
    }
}
