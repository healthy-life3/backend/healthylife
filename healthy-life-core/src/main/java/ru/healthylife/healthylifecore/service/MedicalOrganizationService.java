package ru.healthylife.healthylifecore.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.healthylife.healthylifecore.domain.doctor.MedicalOrganizationInfo;
import ru.healthylife.healthylifecore.mapper.MedicalOrganizationMapper;
import ru.healthylife.healthylifecore.repository.MedicalOrganizationRepository;

import javax.annotation.Nonnull;
import java.util.Collection;

@Service
@Transactional
@AllArgsConstructor
public class MedicalOrganizationService {
    private final MedicalOrganizationMapper medicalOrganizationMapper;
    private final MedicalOrganizationRepository medicalOrganizationRepository;

    @Nonnull
    @Transactional(readOnly = true)
    public Collection<MedicalOrganizationInfo> findAllMedicalOrganizations() {
        return medicalOrganizationMapper.convertToDomain(medicalOrganizationRepository.findAll());
    }
}
