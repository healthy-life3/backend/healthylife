package ru.healthylife.healthylifecore.service.registration;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.healthylife.healthylifecore.domain.user.MedicalUserInfo;
import ru.healthylife.healthylifecore.domain.user.MedicalUserRegistrationInfo;
import ru.healthylife.healthylifecore.domain.user.RoleType;
import ru.healthylife.healthylifecore.entity.user.MedicalUser;
import ru.healthylife.healthylifecore.entity.user.Role;
import ru.healthylife.healthylifecore.entity.user.RoleName;
import ru.healthylife.healthylifecore.mapper.MedicalUserMapper;
import ru.healthylife.healthylifecore.mapper.RoleMapper;
import ru.healthylife.healthylifecore.repository.MedicalUserRepository;
import ru.healthylife.healthylifecore.repository.RoleRepository;
import ru.healthylife.healthylifecore.service.registration.exceptions.UserAlreadyExistException;

import javax.annotation.Nonnull;
import java.util.*;

@Slf4j
@Service
@Transactional
@AllArgsConstructor
public class MedicalUserDetailsServiceImpl implements UserDetailsService, MedicalUserDetailsService {

    private final PasswordEncoder passwordEncoder;
    private final MedicalUserMapper medicalUserMapper;
    private final RoleMapper roleMapper;
    private final RoleRepository roleRepository;
    private final MedicalUserRepository medicalUserRepository;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        Optional<MedicalUser> medicalUserOptional = medicalUserRepository.findByEmail(email);

        if (medicalUserOptional.isEmpty()) {
            log.error("Unknown user with email: {}", email);
            throw new UsernameNotFoundException("Unknown user: " + email);
        }

        MedicalUser medicalUser = medicalUserOptional.get();
        log.debug("User {} founded", medicalUser.getEmail());
        return User.builder()
                .username(medicalUser.getEmail())
                .password(medicalUser.getPassword())
                .authorities(getAuthorities(medicalUser))
                .roles(getRolesNames(medicalUser).toArray(new String[0]))
                .build();
    }

    @Nonnull
    @Override
    @Transactional(readOnly = true)
    public Optional<MedicalUserInfo> findByEmail(@Nonnull String email) {
        MedicalUserInfo medicalUserInfo = medicalUserMapper.convert(
                medicalUserRepository.findByEmail(email).orElse(null)
        );
        return Optional.ofNullable(medicalUserInfo);
    }

    @Nonnull
    @Override
    @Transactional(readOnly = true, propagation = Propagation.MANDATORY)
    public MedicalUserInfo registerNewMedicalUser(@Nonnull MedicalUserRegistrationInfo newMedicalUserInfo) throws UserAlreadyExistException {
        validateNonExistedMedicalUser(newMedicalUserInfo);

        MedicalUser newMedicalUser = medicalUserMapper.convert(newMedicalUserInfo);

        var newRoles = newMedicalUserInfo.getRoles();
        Set<Role> roles = findAllRoles(newRoles);
        if (roles.size() != newRoles.size()) {
            throw new IllegalArgumentException("Expected roles:" + newRoles + " but received:" + roles);
        }
        newMedicalUser.setRoles(roles);
        newMedicalUser.getPhoneNumber().setMedicalUser(newMedicalUser);
        newMedicalUser.setPassword(passwordEncoder.encode(newMedicalUser.getPassword()));
        return medicalUserMapper.convert(medicalUserRepository.save(newMedicalUser));
    }

    private void validateNonExistedMedicalUser(@Nonnull MedicalUserRegistrationInfo medicalUserRegistrationInfo)
            throws UserAlreadyExistException {

        if (medicalUserRepository.existsByEmail(medicalUserRegistrationInfo.getEmail())) {
            log.error("User has already existed: {}", medicalUserRegistrationInfo);
            throw new UserAlreadyExistException("User has already existed:" + medicalUserRegistrationInfo.getEmail());
        }

        if (medicalUserRegistrationInfo.getRoles().isEmpty()) {
            log.error("User must has role");
            throw new IllegalArgumentException("User must has role");
        }
    }

    @Nonnull
    @Override
    public MedicalUserInfo addRoleForMedicalUser(@Nonnull String email, @Nonnull RoleType newRole) {
        MedicalUser medicalUser = medicalUserRepository.findByEmail(email).orElseThrow(() ->
                new UsernameNotFoundException("Medical user must exist: " + email)
        );

        RoleName newRoleName = roleMapper.convert(newRole);
        Role roleByName = roleRepository.getRoleByName(newRoleName);
        medicalUser.getRoles().add(roleByName);

        return medicalUserMapper.convert(medicalUserRepository.save(medicalUser));
    }

    @Nonnull
    private Set<Role> findAllRoles(@Nonnull Set<RoleType> roleInfos) {
        log.debug("Find all roles...");
        var roleNames = roleInfos.stream()
                .map(roleMapper::convert)
                .toList();
        return roleRepository.findByNameIn(roleNames);
    }

    @Nonnull
    private Collection<? extends GrantedAuthority> getAuthorities(@Nonnull MedicalUser medicalUser) {
        log.debug("Get all authorities...");
        return getPrivileges(medicalUser.getRoles()).stream()
                .map(SimpleGrantedAuthority::new)
                .toList();
    }

    @Nonnull
    private Collection<String> getPrivileges(@Nonnull Collection<Role> roles) {
        log.debug("Get all privileges...");
        return roles.stream()
                .map(Role::getPrivileges)
                .flatMap(Collection::stream)
                .map(Objects::toString)
                .toList();
    }

    @Nonnull
    private List<String> getRolesNames(@Nonnull MedicalUser medicalUser) {
        log.debug("Get roles names...");
        return medicalUser.getRoles()
                .stream()
                .map(Role::getName)
                .map(roleMapper::convert)
                .map(Objects::toString)
                .toList();
    }
}
