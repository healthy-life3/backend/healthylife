package ru.healthylife.healthylifecore.service.registration;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.healthylife.healthylifecore.domain.doctor.DoctorRegistrationInfo;
import ru.healthylife.healthylifecore.domain.user.MedicalUserRegistrationInfo;
import ru.healthylife.healthylifecore.domain.user.RoleType;
import ru.healthylife.healthylifecore.entity.doctor.Doctor;
import ru.healthylife.healthylifecore.entity.user.MedicalUser;
import ru.healthylife.healthylifecore.exception.NotFoundEntityException;
import ru.healthylife.healthylifecore.mapper.MedicalUserMapper;
import ru.healthylife.healthylifecore.repository.DoctorProfessionRepository;
import ru.healthylife.healthylifecore.repository.DoctorRepository;
import ru.healthylife.healthylifecore.repository.MedicalOrganizationRepository;
import ru.healthylife.healthylifecore.repository.MedicalUserRepository;

import javax.annotation.Nonnull;

@Slf4j
@Service
public class DoctorRegistrationService extends AbstractRegistrationService<DoctorRegistrationInfo> {

    private final DoctorRepository doctorRepository;
    private final MedicalOrganizationRepository medicalOrganizationRepository;
    private final MedicalUserMapper medicalUserMapper;
    private final DoctorProfessionRepository doctorProfessionRepository;

    public DoctorRegistrationService(
            MedicalUserDetailsService medicalUserDetailsService,
            DoctorRepository doctorRepository,
            MedicalOrganizationRepository medicalOrganizationRepository,
            MedicalUserMapper medicalUserMapper,
            MedicalUserRepository medicalUserRepository,
            DoctorProfessionRepository doctorProfessionRepository
    ) {
        super(medicalUserDetailsService, medicalUserRepository);
        this.doctorRepository = doctorRepository;
        this.medicalOrganizationRepository = medicalOrganizationRepository;
        this.medicalUserMapper = medicalUserMapper;
        this.doctorProfessionRepository = doctorProfessionRepository;
    }

    @Override
    protected void saveNewMedicalUser(
            @Nonnull DoctorRegistrationInfo doctorRegistrationInfo,
            @Nonnull MedicalUser medicalUser
    ) {
        var medicalOrganization = medicalOrganizationRepository.findById(doctorRegistrationInfo.getMedicalOrganizationId())
                .orElseThrow(() -> new NotFoundEntityException("error.medicalorganization.notfound"));
        var doctorProfession = doctorProfessionRepository.findById(doctorRegistrationInfo.getDoctorProfessionId())
                .orElseThrow(() -> new NotFoundEntityException("error.doctorprofession.notfound"));
        var doctor = new Doctor();
        doctor.setProfession(doctorProfession);
        doctor.setMedicalUser(medicalUser);
        doctor.setMedicalOrganization(medicalOrganization);
        doctorRepository.save(doctor);
        log.debug("Doctor with id {} saved successfully", doctor.getId());
    }

    @Nonnull
    @Override
    protected RoleType getNewRole() {
        return RoleType.DOCTOR;
    }

    @Nonnull
    @Override
    protected MedicalUserRegistrationInfo convertToMedicalUserRegistration(@Nonnull DoctorRegistrationInfo doctorRegistrationInfo) {
        return medicalUserMapper.convert(doctorRegistrationInfo);
    }
}
