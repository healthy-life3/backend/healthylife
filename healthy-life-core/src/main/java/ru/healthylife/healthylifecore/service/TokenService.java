package ru.healthylife.healthylifecore.service;

import com.nimbusds.jose.JOSEException;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.stereotype.Service;
import ru.healthylife.healthylifecore.consts.AuthConstants;
import ru.healthylife.healthylifecore.domain.auth.JwtTokenInfo;
import ru.healthylife.healthylifecore.domain.user.MedicalUserInfo;
import ru.healthylife.healthylifecore.security.JwtProvider;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.stream.Collectors;

@Service
public class TokenService {
    private static final String DELIMITER = ",";

    private final JwtDecoder jwtRefreshTokenDecoder;
    private final JwtProvider jwtProvider;

    public TokenService(
            @Qualifier("refreshJwtDecoder") JwtDecoder jwtRefreshTokenDecoder,
            JwtProvider jwtProvider
    ) {
        this.jwtRefreshTokenDecoder = jwtRefreshTokenDecoder;
        this.jwtProvider = jwtProvider;
    }

    @Nonnull
    public JwtTokenInfo getJwtTokens(@Nonnull MedicalUserInfo medicalUserInfo) throws JOSEException {
        var email = medicalUserInfo.getEmail();
        var roles = medicalUserInfo.getRoles()
                .stream()
                .map(Enum::name)
                .collect(Collectors.joining(DELIMITER));
        return getJwtTokens(email, roles);
    }

    @Nonnull
    public JwtTokenInfo refresh(@Nonnull Authentication authentication, @Nonnull String refreshToken) throws JOSEException {
        jwtRefreshTokenDecoder.decode(refreshToken);
        var jwtTokens = getJwtTokens(authentication);
        return new JwtTokenInfo(jwtTokens.getAccessToken(), refreshToken);
    }

    @Nonnull
    public JwtTokenInfo getJwtTokens(@Nonnull Authentication authentication) throws JOSEException {
        var email = authentication.getName();
        var roles = authentication.getAuthorities()
                .stream()
                .map(GrantedAuthority::getAuthority)
                .collect(Collectors.joining(DELIMITER));
        return getJwtTokens(email, roles);
    }

    @Nonnull
    private JwtTokenInfo getJwtTokens(@Nonnull String email, @Nonnull String roles) throws JOSEException {
        var rolesMap = new HashMap<String, String>();
        rolesMap.put(AuthConstants.ROLES, roles);
        String accessToken = jwtProvider.generateAccessToken(email, rolesMap);
        String refreshToken = jwtProvider.generateRefreshToken(email, rolesMap);
        return new JwtTokenInfo(accessToken, refreshToken);
    }

}
