package ru.healthylife.healthylifecore.service;

import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import ru.healthylife.healthylifecore.client.IpfsClusterReadClient;
import ru.healthylife.healthylifecore.client.IpfsClusterWriteClient;
import ru.healthylife.healthylifecore.dto.ipfs.responses.AddIpfsClusterResponse;

import javax.annotation.Nonnull;

@Service
@AllArgsConstructor
public class IpfsServiceImpl implements IpfsService {

    private final IpfsClusterReadClient ipfsClusterReadClient;
    private final IpfsClusterWriteClient ipfsClusterWriteClient;

    @Nonnull
    @Override
    public AddIpfsClusterResponse addFile(@Nonnull MultipartFile file) {
        return ipfsClusterWriteClient.addFile(file);
    }

    @Nonnull
    @Override
    @SneakyThrows
    public byte[] getFile(@Nonnull String cid) {
        return ipfsClusterReadClient.getFile(cid).body().asInputStream().readAllBytes();
    }
}
