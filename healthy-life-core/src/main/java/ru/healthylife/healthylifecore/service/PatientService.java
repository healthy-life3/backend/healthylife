package ru.healthylife.healthylifecore.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.healthylife.healthylifecore.domain.patient.MedicalRecordInfo;
import ru.healthylife.healthylifecore.domain.patient.PatientProfile;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecord;
import ru.healthylife.healthylifecore.mapper.MedicalRecordMapper;
import ru.healthylife.healthylifecore.mapper.PatientMapper;
import ru.healthylife.healthylifecore.repository.MedicalRecordRepository;
import ru.healthylife.healthylifecore.repository.PatientRepository;

import javax.annotation.Nonnull;
import java.util.List;


@Service
@Transactional
@AllArgsConstructor
public class PatientService {

    private final PatientMapper patientMapper;
    private final PatientRepository patientRepository;
    private final MedicalRecordMapper medicalRecordMapper;
    private final MedicalRecordRepository medicalRecordRepository;

    @Transactional(readOnly = true)
    public PatientProfile getProfileByEmail(@Nonnull String email) {
        return patientMapper.convert(patientRepository.getByMedicalUserEmail(email));
    }

    @Transactional(readOnly = true)
    public List<MedicalRecordInfo> getAllMedicalRecords(@Nonnull String email) {
        List<MedicalRecord> medicalRecords = medicalRecordRepository.findAllByPatientMedicalUserEmailOrderByDateTimeDesc(email);
        return medicalRecordMapper.convertToDomain(medicalRecords);
    }
}
