package ru.healthylife.healthylifecore.service.registration;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.transaction.annotation.Transactional;
import ru.healthylife.healthylifecore.domain.user.MedicalUserInfo;
import ru.healthylife.healthylifecore.domain.user.MedicalUserRegistrationInfo;
import ru.healthylife.healthylifecore.domain.user.RoleType;
import ru.healthylife.healthylifecore.entity.user.MedicalUser;
import ru.healthylife.healthylifecore.repository.MedicalUserRepository;
import ru.healthylife.healthylifecore.service.registration.exceptions.UserAlreadyExistException;

import javax.annotation.Nonnull;
import java.util.Optional;

@Slf4j
@Transactional
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public abstract class AbstractRegistrationService<T> {

    private final MedicalUserDetailsService medicalUserDetailsService;
    private final MedicalUserRepository medicalUserRepository;

    @Nonnull
    public MedicalUserInfo registerNewMedicalUser(@Nonnull T newMedicalUser) {
        MedicalUserRegistrationInfo medicalUser = convertToMedicalUserRegistration(newMedicalUser);
        Optional<MedicalUserInfo> medicalUserInfo = medicalUserDetailsService.findByEmail(medicalUser.getEmail());

        if (medicalUserInfo.isPresent()) {
            log.error("User with such role({}) has already registered with id: {}", getNewRole(), medicalUserInfo);
            throwUserExistedException(newMedicalUser);
        }
        medicalUser.getRoles().add(getNewRole());
        MedicalUserInfo registeredMedicalUser = medicalUserDetailsService.registerNewMedicalUser(medicalUser);
        saveNewMedicalUser(newMedicalUser, medicalUserRepository.getById(registeredMedicalUser.getId()));
        return registeredMedicalUser;
    }

    protected abstract void saveNewMedicalUser(@Nonnull T newMedicalUserAdditionalInfo, @Nonnull MedicalUser medicalUser) throws UserAlreadyExistException;

    @Nonnull
    protected abstract RoleType getNewRole();

    @Nonnull
    protected abstract MedicalUserRegistrationInfo convertToMedicalUserRegistration(T additionalMedicalRegistrationInfo);

    protected void throwUserExistedException(T medicalUserInfo) {
        throw new UserAlreadyExistException("User has already registered:" + medicalUserInfo);
    }
}
