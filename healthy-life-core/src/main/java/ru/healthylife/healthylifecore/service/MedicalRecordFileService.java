package ru.healthylife.healthylifecore.service;

import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import ru.healthylife.healthylifecore.domain.ehr.MedicalRecordFileInfo;
import ru.healthylife.healthylifecore.domain.ehr.NewMedicalRecordFile;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecord;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecordFile;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecordType;
import ru.healthylife.healthylifecore.exception.MedicalRecordFileUploadingException;
import ru.healthylife.healthylifecore.exception.NotFoundEntityException;
import ru.healthylife.healthylifecore.exception.OriginalFileNameException;
import ru.healthylife.healthylifecore.mapper.MedicalRecordFileMapper;
import ru.healthylife.healthylifecore.repository.DoctorRepository;
import ru.healthylife.healthylifecore.repository.MedicalRecordFileRepository;
import ru.healthylife.healthylifecore.repository.PatientRepository;

import javax.annotation.Nonnull;
import java.io.IOException;
import java.time.Clock;
import java.util.UUID;

@Slf4j
@Service
@Transactional
public class MedicalRecordFileService extends AbstractMedicalRecordService<NewMedicalRecordFile, MedicalRecordFileInfo> {
    private final MedicalRecordFileMapper medicalRecordFileMapper;
    private final MedicalRecordFileRepository medicalRecordFileRepository;

    public MedicalRecordFileService(
            Clock clock,
            DoctorRepository doctorRepository,
            PatientRepository patientRepository,
            MedicalRecordFileMapper medicalRecordFileMapper,
            MedicalRecordFileRepository medicalRecordFileRepository
    ) {
        super(clock, doctorRepository, patientRepository);
        this.medicalRecordFileMapper = medicalRecordFileMapper;
        this.medicalRecordFileRepository = medicalRecordFileRepository;
    }

    @Nonnull
    @Transactional(readOnly = true)
    public MedicalRecordFileInfo getMedicalRecordFileInfo(@Nonnull UUID medicalRecordUuid) {
        var medicalRecordFile = medicalRecordFileRepository.findById(medicalRecordUuid)
                .orElseThrow(() -> new NotFoundEntityException("error.medicalrecordfile.notfound"));
        return medicalRecordFileMapper.convertToDomain(medicalRecordFile);
    }

    @Nonnull
    @Override
    protected MedicalRecordType getMedicalRecordType() {
        return MedicalRecordType.FILE;
    }

    @Nonnull
    @Override
    protected MedicalRecordFileInfo saveDetails(@Nonnull NewMedicalRecordFile newMedicalRecordInfo, @NonNull MedicalRecord medicalRecord) {
        var medicalRecordFile = new MedicalRecordFile();
        medicalRecordFile.setMedicalRecord(medicalRecord);

        var multipartFile = newMedicalRecordInfo.getMultipartFile();
        try {
            prepareMedicalRecordFile(medicalRecordFile, multipartFile);
        } catch (IOException e) {
            log.error("Failed while medical record file was uploading", e);
            throw new MedicalRecordFileUploadingException("error.uploadingfile", e);
        }
        return medicalRecordFileMapper.convertToDomain(medicalRecordFileRepository.save(medicalRecordFile));
    }

    private void prepareMedicalRecordFile(@Nonnull MedicalRecordFile medicalRecordFile, @Nonnull MultipartFile multipartFile) throws IOException {
        var originalFilename = multipartFile.getOriginalFilename();
        if (originalFilename == null) {
            throw new OriginalFileNameException("error.originfilename");
        }

        medicalRecordFile.setFileName(StringUtils.cleanPath(originalFilename));
        medicalRecordFile.setFileType(multipartFile.getContentType());
        medicalRecordFile.setData(multipartFile.getBytes());
    }
}
