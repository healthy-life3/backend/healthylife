package ru.healthylife.healthylifecore.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.healthylife.healthylifecore.domain.doctor.DoctorProfessionInfo;
import ru.healthylife.healthylifecore.mapper.DoctorProfessionMapper;
import ru.healthylife.healthylifecore.repository.DoctorProfessionRepository;

import java.util.List;

@Service
@Transactional
@AllArgsConstructor
public class DoctorProfessionService {
    private final DoctorProfessionMapper doctorProfessionMapper;
    private final DoctorProfessionRepository doctorProfessionRepository;

    @Transactional(readOnly = true, propagation = Propagation.SUPPORTS)
    public List<DoctorProfessionInfo> findAllProfessions() {
        return doctorProfessionMapper.mapToDomain(doctorProfessionRepository.findAll());
    }
}
