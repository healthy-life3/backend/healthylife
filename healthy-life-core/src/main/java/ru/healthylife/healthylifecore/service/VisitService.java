package ru.healthylife.healthylifecore.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.healthylife.healthylifecore.domain.ehr.NewVisit;
import ru.healthylife.healthylifecore.domain.ehr.VisitInfo;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecord;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecordType;
import ru.healthylife.healthylifecore.exception.NotFoundEntityException;
import ru.healthylife.healthylifecore.mapper.VisitMapper;
import ru.healthylife.healthylifecore.repository.DoctorRepository;
import ru.healthylife.healthylifecore.repository.PatientRepository;
import ru.healthylife.healthylifecore.repository.VisitRepository;

import javax.annotation.Nonnull;
import java.time.Clock;
import java.util.UUID;

@Service
@Transactional
public class VisitService extends AbstractMedicalRecordService<NewVisit, VisitInfo> {

    private final VisitMapper visitMapper;
    private final VisitRepository visitRepository;

    public VisitService(
            Clock clock,
            DoctorRepository doctorRepository,
            PatientRepository patientRepository,
            VisitMapper visitMapper,
            VisitRepository visitRepository
    ) {
        super(clock, doctorRepository, patientRepository);
        this.visitMapper = visitMapper;
        this.visitRepository = visitRepository;
    }

    @Nonnull
    @Transactional(readOnly = true)
    public VisitInfo findVisitByUuid(@Nonnull UUID uuid) {
        var visit = visitRepository.findById(uuid)
                .orElseThrow(() -> new NotFoundEntityException("error.visit.notfound"));
        return visitMapper.convertToDomain(visit);
    }

    @Nonnull
    @Override
    protected MedicalRecordType getMedicalRecordType() {
        return MedicalRecordType.VISIT;
    }

    @Nonnull
    @Override
    protected VisitInfo saveDetails(@Nonnull NewVisit newMedicalRecordInfo, @Nonnull MedicalRecord medicalRecord) {
        var visit = visitMapper.convertToEntity(newMedicalRecordInfo);
        visit.setMedicalRecord(medicalRecord);
        var createdVisit = visitRepository.save(visit);
        return visitMapper.convertToDomain(createdVisit);
    }
}
