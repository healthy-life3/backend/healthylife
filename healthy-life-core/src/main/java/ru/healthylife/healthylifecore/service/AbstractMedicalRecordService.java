package ru.healthylife.healthylifecore.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.healthylife.healthylifecore.domain.ehr.NewMedicalRecord;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecord;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecordType;
import ru.healthylife.healthylifecore.repository.DoctorRepository;
import ru.healthylife.healthylifecore.repository.PatientRepository;

import javax.annotation.Nonnull;
import java.time.Clock;
import java.time.LocalDateTime;

/**
 * @param <T> new medical record information
 * @param <R> domain medical record information after medical record was created
 */
@Service
@Transactional
@AllArgsConstructor
public abstract class AbstractMedicalRecordService<T extends NewMedicalRecord, R> {
    private final Clock clock;
    private final DoctorRepository doctorRepository;
    private final PatientRepository patientRepository;

    @Nonnull
    public R createNewMedicalRecord(@Nonnull T newMedicalRecordInfo) {
        var doctor = doctorRepository.getByMedicalUserEmail(newMedicalRecordInfo.getDoctorEmail());
        var patient = patientRepository.getByMedicalUserEmail(newMedicalRecordInfo.getPatientEmail());
        var medicalRecord = new MedicalRecord();
        medicalRecord.setTitle(newMedicalRecordInfo.getTitle());
        medicalRecord.setDoctor(doctor);
        medicalRecord.setPatient(patient);
        medicalRecord.setMedicalRecordType(getMedicalRecordType());
        medicalRecord.setDateTime(LocalDateTime.now(clock));

        return saveDetails(newMedicalRecordInfo, medicalRecord);
    }

    @Nonnull
    protected abstract MedicalRecordType getMedicalRecordType();

    @Nonnull
    protected abstract R saveDetails(@Nonnull T newMedicalRecordInfo, @Nonnull MedicalRecord medicalRecord);
}
