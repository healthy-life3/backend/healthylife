package ru.healthylife.healthylifecore.service;

import org.springframework.web.multipart.MultipartFile;
import ru.healthylife.healthylifecore.dto.ipfs.responses.AddIpfsClusterResponse;

import javax.annotation.Nonnull;

public interface IpfsService {

    @Nonnull
    AddIpfsClusterResponse addFile(@Nonnull MultipartFile file);

    @Nonnull
    byte[] getFile(@Nonnull String cid);
}
