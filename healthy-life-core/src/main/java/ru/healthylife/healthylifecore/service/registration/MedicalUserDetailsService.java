package ru.healthylife.healthylifecore.service.registration;

import lombok.NonNull;
import ru.healthylife.healthylifecore.domain.user.MedicalUserInfo;
import ru.healthylife.healthylifecore.domain.user.MedicalUserRegistrationInfo;
import ru.healthylife.healthylifecore.domain.user.RoleType;
import ru.healthylife.healthylifecore.service.registration.exceptions.UserAlreadyExistException;

import java.util.Optional;

public interface MedicalUserDetailsService {

    @NonNull
    Optional<MedicalUserInfo> findByEmail(@NonNull String email);

    @NonNull
    MedicalUserInfo registerNewMedicalUser(@NonNull MedicalUserRegistrationInfo newMedicalUser) throws UserAlreadyExistException;

    @NonNull
    MedicalUserInfo addRoleForMedicalUser(@NonNull String email, @NonNull RoleType role);
}
