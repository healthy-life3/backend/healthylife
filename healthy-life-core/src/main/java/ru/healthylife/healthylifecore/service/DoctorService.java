package ru.healthylife.healthylifecore.service;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.healthylife.healthylifecore.domain.doctor.DoctorProfile;
import ru.healthylife.healthylifecore.mapper.DoctorMapper;
import ru.healthylife.healthylifecore.repository.DoctorRepository;

import javax.annotation.Nonnull;

@Service
@Transactional
@AllArgsConstructor
public class DoctorService {

    private final DoctorMapper doctorMapper;
    private final DoctorRepository doctorRepository;

    @Transactional(readOnly = true)
    public DoctorProfile getProfileByEmail(@Nonnull String email) {
        return doctorMapper.convert(doctorRepository.getByMedicalUserEmail(email));
    }
}
