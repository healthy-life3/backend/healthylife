package ru.healthylife.healthylifecore.service.registration;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.healthylife.healthylifecore.domain.check.EmailCheckInfo;
import ru.healthylife.healthylifecore.domain.check.PhoneNumberCheckInfo;
import ru.healthylife.healthylifecore.repository.MedicalUserRepository;
import ru.healthylife.healthylifecore.repository.PatientRepository;
import ru.healthylife.healthylifecore.repository.PhoneNumberRepository;

import javax.annotation.Nonnull;

@Service
@AllArgsConstructor
@Transactional(readOnly = true)
public class RegistrationCheckerService {
    private final PatientRepository patientRepository;
    private final PhoneNumberRepository phoneNumberRepository;
    private final MedicalUserRepository medicalUserRepository;

    public boolean checkEmail(@Nonnull EmailCheckInfo emailCheckInfo) {
        return medicalUserRepository.existsByEmail(emailCheckInfo.getEmail());
    }

    public boolean checkPhoneNumber(@Nonnull PhoneNumberCheckInfo phoneNumberCheckInfo) {
        return phoneNumberRepository.existsByFullNumber(phoneNumberCheckInfo.getFullNumber());
    }

    public boolean checkSnils(@Nonnull String snils) {
        return patientRepository.existsBySnils(snils);
    }
}
