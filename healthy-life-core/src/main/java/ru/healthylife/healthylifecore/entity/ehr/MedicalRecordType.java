package ru.healthylife.healthylifecore.entity.ehr;

public enum MedicalRecordType {
    LAB,
    VISIT,
    FILE
}
