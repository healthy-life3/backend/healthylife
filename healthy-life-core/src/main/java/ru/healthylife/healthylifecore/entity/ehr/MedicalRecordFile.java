package ru.healthylife.healthylifecore.entity.ehr;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.UUID;

@Entity
@Getter
@Setter
@Table(name = MedicalRecordFile.TABLE_NAME)
public class MedicalRecordFile {
    protected static final String TABLE_NAME = "medical_record_file";

    protected static final String FILE_NAME = "file_name";
    protected static final String FILE_TYPE = "file_type";

    @Id
    private UUID medicalRecordUuid;

    @MapsId
    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = MedicalRecord.UUID_MEDICAL_RECORD, nullable = false)
    private MedicalRecord medicalRecord;

    @Column(name = FILE_NAME, nullable = false)
    private String fileName;

    @Column(name = FILE_TYPE, nullable = false)
    private String fileType;

    @Lob
    @Column(nullable = false)
    private byte[] data;
}
