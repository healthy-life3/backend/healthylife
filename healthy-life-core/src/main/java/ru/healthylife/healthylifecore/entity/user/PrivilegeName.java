package ru.healthylife.healthylifecore.entity.user;

public enum PrivilegeName {
    READ_MEDICAL_RECORDS,
    WRITE_MEDICAL_RECORDS
}
