package ru.healthylife.healthylifecore.entity.patient;

public enum BloodGroupType {
    I_GROUP,
    II_GROUP,
    III_GROUP,
    IV_GROUP
}