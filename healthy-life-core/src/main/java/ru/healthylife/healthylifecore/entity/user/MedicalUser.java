package ru.healthylife.healthylifecore.entity.user;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NaturalId;
import ru.healthylife.healthylifecore.consts.DatabaseConstants;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@Table(name = MedicalUser.TABLE_NAME)
public class MedicalUser {
    public static final String TABLE_NAME = "medical_user";
    public static final String GENERATOR_NAME = TABLE_NAME + DatabaseConstants.GENERATOR_SUFFIX;
    public static final String SEQUENCE_NAME = TABLE_NAME + DatabaseConstants.SEQUENCE_SUFFIX;
    public static final String MEDICAL_USER_ID = TABLE_NAME + DatabaseConstants.ID_SUFFIX;
    public static final String LAST_NAME = "last_name";
    public static final String FIRST_NAME = "first_name";
    public static final String MIDDLE_NAME = "middle_name";
    public static final String USERS_ROLES = "users_roles";

    @Id
    @Column(name = MEDICAL_USER_ID, nullable = false)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR_NAME)
    @SequenceGenerator(name = GENERATOR_NAME, sequenceName = SEQUENCE_NAME)
    private Long id;

    @Column(name = LAST_NAME, nullable = false, length = 85)
    private String lastName;

    @Column(name = FIRST_NAME, nullable = false, length = 85)
    private String firstName;

    @Column(name = MIDDLE_NAME, length = 85)
    private String middleName;

    @NaturalId
    @Column(nullable = false, length = 128, unique = true)
    private String email;

    @Column(nullable = false)
    private LocalDate birthday;

    @Enumerated(value = EnumType.STRING)
    @Column(nullable = false)
    private Gender gender;

    @PrimaryKeyJoinColumn
    @OneToOne(mappedBy = "medicalUser", cascade = CascadeType.ALL)
    private PhoneNumber phoneNumber;

    @Column(nullable = false)
    private String password;

    @ToString.Exclude
    @ManyToMany
    @JoinTable(
            name = USERS_ROLES,
            joinColumns = @JoinColumn(name = MEDICAL_USER_ID),
            inverseJoinColumns = @JoinColumn(name = Role.ROLE_ID)
    )
    private Set<Role> roles = new HashSet<>();
}
