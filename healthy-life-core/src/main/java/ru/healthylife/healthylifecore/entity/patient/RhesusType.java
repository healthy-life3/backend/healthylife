package ru.healthylife.healthylifecore.entity.patient;

public enum RhesusType {
    POSITIVE,
    NEGATIVE
}
