package ru.healthylife.healthylifecore.entity.doctor;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.healthylife.healthylifecore.entity.ehr.MedicalOrganization;
import ru.healthylife.healthylifecore.entity.user.MedicalUser;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = Doctor.TABLE_NAME)
public class Doctor {
    public static final String TABLE_NAME = "doctor";
    public static final String PROFESSION_ID = "profession_id";

    @Id
    private Long id;

    @MapsId
    @OneToOne(optional = false)
    @JoinColumn(name = MedicalUser.MEDICAL_USER_ID)
    private MedicalUser medicalUser;

    @ManyToOne
    @JoinColumn(name = PROFESSION_ID)
    private DoctorProfession profession;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = MedicalOrganization.MEDICAL_ORGANIZATION_ID)
    private MedicalOrganization medicalOrganization;
}
