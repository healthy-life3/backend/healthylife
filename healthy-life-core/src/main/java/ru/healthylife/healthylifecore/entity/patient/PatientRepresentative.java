package ru.healthylife.healthylifecore.entity.patient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.healthylife.healthylifecore.consts.DatabaseConstants;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = PatientRepresentative.TABLE_NAME)
public class PatientRepresentative {
    public static final String TABLE_NAME = "patient_representative";
    public static final String GENERATOR_NAME = TABLE_NAME + DatabaseConstants.GENERATOR_SUFFIX;
    public static final String SEQUENCE_NAME = TABLE_NAME + DatabaseConstants.SEQUENCE_SUFFIX;
    public static final String PATIENT_REPRESENTATIVE_ID = TABLE_NAME + DatabaseConstants.ID_SUFFIX;
    public static final String RELATION_ID = "relation_id";

    @Id
    @Column(name = PATIENT_REPRESENTATIVE_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR_NAME)
    @SequenceGenerator(name = GENERATOR_NAME, sequenceName = SEQUENCE_NAME)
    private String id;

    @ManyToOne
    @JoinColumn(name = RELATION_ID)
    private RelationDegree relationDegree;
}
