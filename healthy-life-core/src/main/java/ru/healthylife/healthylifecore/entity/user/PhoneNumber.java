package ru.healthylife.healthylifecore.entity.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.annotations.NaturalId;
import ru.healthylife.healthylifecore.consts.DatabaseConstants;

import javax.persistence.*;

/**
 * Standard – E.164
 */
@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = PhoneNumber.TABLE_NAME)
public class PhoneNumber {
    public static final String TABLE_NAME = "phone_number";
    public static final String PHONE_NUMBER_ID = TABLE_NAME + DatabaseConstants.ID_SUFFIX;
    public static final String FULL_NUMBER = "full_number";
    public static final String COUNTRY_CODE = "country_code";
    public static final String SUBSCRIBER_NUMBER = "subscriber_number";

    @Id
    private Long id;

    @MapsId
    @OneToOne
    @ToString.Exclude
    @JoinColumn(name = MedicalUser.MEDICAL_USER_ID)
    private MedicalUser medicalUser;

    @NaturalId
    @Column(name = FULL_NUMBER, nullable = false, length = 15)
    private String fullNumber;

    @Column(name = COUNTRY_CODE, nullable = false, length = 3)
    private String countryCode;

    @Column(name = SUBSCRIBER_NUMBER, nullable = false, length = 12)
    private String subscriberNumber;
}
