package ru.healthylife.healthylifecore.entity.ehr;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Entity
@ToString
@Table(name = Visit.TABLE_NAME)
public class Visit {
    public static final String TABLE_NAME = "visit";
    public static final String TREATMENT_PLAN = "treatment_plan";
    public static final String PRESCRIPTIONS = "prescriptions";
    public static final String ICD_ID = "icd_id";
    public static final String COMPLAINTS = "complaints";
    public static final String DIAGNOSIS = "diagnosis";
    public static final String VISIT_ID = "visit_id";
    public static final String VISITS_ICDS_TABLE_NAME = "visits_icds";

    @Id
    private UUID medicalRecordUuid;

    @MapsId
    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = MedicalRecord.UUID_MEDICAL_RECORD, nullable = false)
    private MedicalRecord medicalRecord;

    @ManyToMany
    @JoinTable(
            name = VISITS_ICDS_TABLE_NAME,
            joinColumns = @JoinColumn(name = VISIT_ID),
            inverseJoinColumns = @JoinColumn(name = ICD_ID)
    )
    @ToString.Exclude
    private List<Icd> icds;

    @Lob
    @Column(name = COMPLAINTS)
    private String complaints;

    @Column(name = DIAGNOSIS, nullable = false)
    private String diagnosis;

    @Lob
    @Column(name = TREATMENT_PLAN)
    private String treatmentPlan;

    @Lob
    @Column(name = PRESCRIPTIONS)
    private String prescriptions;
}
