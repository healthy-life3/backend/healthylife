package ru.healthylife.healthylifecore.entity.ehr;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.healthylife.healthylifecore.consts.DatabaseConstants;

import javax.persistence.*;

import static ru.healthylife.healthylifecore.entity.ehr.MedicalOrganization.TABLE_NAME;

@Getter
@Setter
@Entity
@ToString
@Table(name = TABLE_NAME)
public class MedicalOrganization {
    public static final String TABLE_NAME = "medical_organization";
    public static final String MEDICAL_ORGANIZATION_ID = TABLE_NAME + DatabaseConstants.ID_SUFFIX;
    public static final String GENERATOR_NAME = TABLE_NAME + DatabaseConstants.GENERATOR_SUFFIX;
    public static final String SEQUENCE_NAME = TABLE_NAME + DatabaseConstants.SEQUENCE_SUFFIX;
    public static final String NAME = "name";

    @Id
    @Column(name = MEDICAL_ORGANIZATION_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR_NAME)
    @SequenceGenerator(name = GENERATOR_NAME, sequenceName = SEQUENCE_NAME)
    private Long id;

    @Column(name = NAME, nullable = false, length = 100)
    private String name;
}

