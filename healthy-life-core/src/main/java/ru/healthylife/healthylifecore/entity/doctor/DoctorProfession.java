package ru.healthylife.healthylifecore.entity.doctor;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.healthylife.healthylifecore.consts.DatabaseConstants;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = DoctorProfession.TABLE_NAME)
public class DoctorProfession {
    public static final String TABLE_NAME = "doctor_profession";
    public static final String DOCTOR_PROFESSION_ID = TABLE_NAME + DatabaseConstants.ID_SUFFIX;
    public static final String GENERATOR_NAME = TABLE_NAME + DatabaseConstants.GENERATOR_SUFFIX;
    public static final String SEQUENCE_NAME = TABLE_NAME + DatabaseConstants.SEQUENCE_SUFFIX;
    public static final String NAME = "name";

    @Id
    @Column(name = DOCTOR_PROFESSION_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR_NAME)
    @SequenceGenerator(name = GENERATOR_NAME, sequenceName = SEQUENCE_NAME)
    private Long id;

    @Column(name = NAME, nullable = false)
    private String name;
}
