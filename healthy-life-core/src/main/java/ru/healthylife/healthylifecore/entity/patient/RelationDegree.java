package ru.healthylife.healthylifecore.entity.patient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.healthylife.healthylifecore.consts.DatabaseConstants;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = RelationDegree.TABLE_NAME)
public class RelationDegree {
    public static final String TABLE_NAME = "relation_degree";
    public static final String GENERATOR_NAME = TABLE_NAME + DatabaseConstants.GENERATOR_SUFFIX;
    public static final String SEQUENCE_NAME = TABLE_NAME + DatabaseConstants.SEQUENCE_SUFFIX;
    public static final String RELATION_DEGREE_ID = TABLE_NAME + DatabaseConstants.ID_SUFFIX;
    public static final String NAME = "name";

    @Id
    @Column(name = RELATION_DEGREE_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR_NAME)
    @SequenceGenerator(name = GENERATOR_NAME, sequenceName = SEQUENCE_NAME)
    private Integer id;

    @Column(name = NAME, nullable = false, length = 40)
    private String name;
}



