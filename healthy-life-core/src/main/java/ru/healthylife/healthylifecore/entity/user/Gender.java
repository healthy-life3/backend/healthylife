package ru.healthylife.healthylifecore.entity.user;

public enum Gender {
    UNKNOWN,
    MALE,
    FEMALE
}
