package ru.healthylife.healthylifecore.entity.user;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.healthylife.healthylifecore.consts.DatabaseConstants;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = Role.TABLE_NAME)
public class Role {
    public static final String TABLE_NAME = "role";
    public static final String GENERATOR_NAME = TABLE_NAME + DatabaseConstants.GENERATOR_SUFFIX;
    public static final String SEQUENCE_NAME = TABLE_NAME + DatabaseConstants.SEQUENCE_SUFFIX;
    public static final String ROLE_ID = TABLE_NAME + DatabaseConstants.ID_SUFFIX;
    public static final String ROLES_PRIVILEGES = "roles_privileges";

    @Id
    @Column(name = ROLE_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR_NAME)
    @SequenceGenerator(name = GENERATOR_NAME, sequenceName = SEQUENCE_NAME)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false, unique = true)
    private RoleName name;

    @ManyToMany(cascade = CascadeType.PERSIST)
    @JoinTable(
            name = ROLES_PRIVILEGES,
            joinColumns = @JoinColumn(name = ROLE_ID),
            inverseJoinColumns = @JoinColumn(name = Privilege.PRIVILEGE_ID)
    )
    private Set<Privilege> privileges = new HashSet<>();
}
