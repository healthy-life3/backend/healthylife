package ru.healthylife.healthylifecore.entity.ehr;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.*;
import java.util.UUID;

@Getter
@Setter
@Entity
@ToString
@Table(name = LaboratoryTest.TABLE_NAME)
public class LaboratoryTest {
    public static final String TABLE_NAME = "laboratory_test";
    public static final String PARAM_NAME = "param_name";
    public static final String PARAM_VALUE = "param_value";
    public static final String NORMAL_PARAM_VALUE = "normal_param_value";
    public static final String UNIT_MEASURE = "unit_measure";
    public static final String COMMENT = "comment";

    @Id
    private UUID medicalRecordUuid;

    @MapsId
    @OneToOne(optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = MedicalRecord.UUID_MEDICAL_RECORD, nullable = false)
    private MedicalRecord medicalRecord;

    @Column(name = PARAM_NAME, nullable = false)
    private String paramName;

    @Column(name = PARAM_VALUE, nullable = false)
    private String paramValue;

    @Column(name = NORMAL_PARAM_VALUE, nullable = false)
    private String normalParamValue;

    @Column(name = UNIT_MEASURE, nullable = false)
    private String unitMeasure;

    @Lob
    @Column(name = COMMENT)
    private String comment;
}
