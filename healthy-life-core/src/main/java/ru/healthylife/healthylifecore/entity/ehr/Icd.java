package ru.healthylife.healthylifecore.entity.ehr;

import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * International Classification of Diseases
 *
 * @see <a href="https://en.wikipedia.org/wiki/International_Classification_of_Diseases">ICD</a>
 */
@Getter
@Setter
@Entity
@ToString
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = Icd.TABLE_NAME)
public class Icd {
    public static final String TABLE_NAME = "international_classification_disease";
    public static final String CODE = "code";
    public static final String DESCRIPTION = "description";

    @Id
    @Column(name = CODE, nullable = false, length = 10)
    private String code;

    @Column(name = DESCRIPTION, nullable = false)
    private String description;
}
