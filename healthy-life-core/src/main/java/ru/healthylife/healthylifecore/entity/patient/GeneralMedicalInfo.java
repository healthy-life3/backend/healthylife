package ru.healthylife.healthylifecore.entity.patient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.healthylife.healthylifecore.consts.DatabaseConstants;

import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor
@Table(name = GeneralMedicalInfo.TABLE_NAME)
public class GeneralMedicalInfo {
    public static final String TABLE_NAME = "general_medical_info";
    public static final String GENERATOR_NAME = TABLE_NAME + DatabaseConstants.GENERATOR_SUFFIX;
    public static final String SEQUENCE_NAME = TABLE_NAME + DatabaseConstants.SEQUENCE_SUFFIX;
    public static final String GENERAL_MEDICAL_INFO_ID = TABLE_NAME + DatabaseConstants.ID_SUFFIX;
    public static final String BLOOD_GROUP_TYPE = "blood_group_type";
    public static final String RHESUS_TYPE = "rhesus_type";

    @Id
    @Column(name = GENERAL_MEDICAL_INFO_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR_NAME)
    @SequenceGenerator(name = GENERATOR_NAME, sequenceName = SEQUENCE_NAME)
    private Long id;

    @Column(name = BLOOD_GROUP_TYPE)
    private BloodGroupType bloodGroupType;

    @Column(name = RHESUS_TYPE)
    private RhesusType rhesusType;
}
