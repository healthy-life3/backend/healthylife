package ru.healthylife.healthylifecore.entity.patient;

import lombok.*;
import org.hibernate.annotations.NaturalId;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecord;
import ru.healthylife.healthylifecore.entity.user.MedicalUser;

import javax.annotation.Nonnull;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = Patient.TABLE_NAME)
public class Patient {
    public static final String TABLE_NAME = "patient";
    public static final String PATIENT_ID = "patient_id";
    public static final String SNILS = "snils";
    public static final String GENERAL_MEDICAL_INFO_ID = "general_medical_info_id";
    public static final String PATIENTS_AND_REPRESENTATIVES_TABLE_NAME = "patients_and_representatives";
    public static final String REPRESENTATIVE_ID = "representative_id";

    @Id
    private Long id;

    @MapsId
    @OneToOne(optional = false)
    @JoinColumn(name = MedicalUser.MEDICAL_USER_ID)
    private MedicalUser medicalUser;

    @NaturalId
    @Column(name = SNILS, nullable = false, length = 11)
    private String snils;

    @ManyToOne
    @JoinColumn(name = GENERAL_MEDICAL_INFO_ID)
    private GeneralMedicalInfo generalMedicalInfo;

    @ManyToMany
    @JoinTable(
            name = PATIENTS_AND_REPRESENTATIVES_TABLE_NAME,
            joinColumns = @JoinColumn(name = PATIENT_ID),
            inverseJoinColumns = @JoinColumn(name = PatientRepresentative.PATIENT_REPRESENTATIVE_ID)
    )
    private List<PatientRepresentative> patientRepresentatives;

    @OneToMany(mappedBy = "patient")
    private List<MedicalRecord> medicalRecords = new ArrayList<>();

    public Patient(@Nonnull String snils) {
        this.snils = snils;
    }
}
