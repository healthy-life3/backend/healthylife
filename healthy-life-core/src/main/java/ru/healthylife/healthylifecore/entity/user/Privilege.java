package ru.healthylife.healthylifecore.entity.user;

import lombok.*;
import ru.healthylife.healthylifecore.consts.DatabaseConstants;

import javax.annotation.Nonnull;
import javax.persistence.*;

@Entity
@Getter
@Setter
@ToString
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = Privilege.TABLE_NAME)
public class Privilege {
    public static final String TABLE_NAME = "privilege";
    public static final String GENERATOR_NAME = TABLE_NAME + DatabaseConstants.GENERATOR_SUFFIX;
    public static final String SEQUENCE_NAME = TABLE_NAME + DatabaseConstants.SEQUENCE_SUFFIX;
    public static final String PRIVILEGE_ID = TABLE_NAME + DatabaseConstants.ID_SUFFIX;

    @Id
    @Column(name = PRIVILEGE_ID)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = GENERATOR_NAME)
    @SequenceGenerator(name = GENERATOR_NAME, sequenceName = SEQUENCE_NAME)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(unique = true, nullable = false)
    private PrivilegeName name;

    public Privilege(@Nonnull PrivilegeName name) {
        this.name = name;
    }
}


