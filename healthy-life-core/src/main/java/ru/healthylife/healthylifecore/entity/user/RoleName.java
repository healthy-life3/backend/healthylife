package ru.healthylife.healthylifecore.entity.user;

public enum RoleName {
    ROLE_DOCTOR,
    ROLE_PATIENT
}
