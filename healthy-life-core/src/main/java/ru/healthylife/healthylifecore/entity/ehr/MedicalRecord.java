package ru.healthylife.healthylifecore.entity.ehr;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.healthylife.healthylifecore.entity.doctor.Doctor;
import ru.healthylife.healthylifecore.entity.patient.Patient;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.UUID;

@Getter
@Setter
@Entity
@ToString
@Table(name = MedicalRecord.TABLE_NAME)
public class MedicalRecord {
    public static final String TABLE_NAME = "medical_record";
    public static final String UUID_MEDICAL_RECORD = "medical_record_uuid";
    public static final String MEDICAL_RECORD_TYPE = "medical_record_type";
    public static final String TITLE = "title";
    public static final String DATE_TIME = "date_time";
    public static final String PATIENT_ID = "patient_id";
    public static final String DOCTOR_ID = "doctor_id";
    public static final String MEDICAL_ORGANIZATION_ID = "medical_organization_id";

    @Id
    @GeneratedValue
    @Column(name = UUID_MEDICAL_RECORD)
    private UUID medicalRecordUuid;

    @Enumerated(EnumType.STRING)
    @Column(name = MEDICAL_RECORD_TYPE, nullable = false, length = 30)
    private MedicalRecordType medicalRecordType;

    @Column(name = TITLE, nullable = false, length = 50)
    private String title;

    @Column(name = DATE_TIME, nullable = false)
    private LocalDateTime dateTime;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = PATIENT_ID, nullable = false)
    private Patient patient;

    @ToString.Exclude
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = DOCTOR_ID, nullable = false)
    private Doctor doctor;
}
