package ru.healthylife.healthylifecore.client;

import feign.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import ru.healthylife.healthylifecore.configuration.FeignMultipartSupportConfiguration;

@FeignClient(
        name = "${feign.ipfs-cluster-read.name}",
        url = "${feign.ipfs-cluster-read.host}",
        configuration = FeignMultipartSupportConfiguration.class
)
public interface IpfsClusterReadClient {

    @GetMapping(value = "/ipfs/{cid}", consumes = MediaType.TEXT_PLAIN_VALUE)
    Response getFile(@PathVariable String cid);
}
