package ru.healthylife.healthylifecore.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import ru.healthylife.healthylifecore.configuration.FeignMultipartSupportConfiguration;
import ru.healthylife.healthylifecore.dto.ipfs.responses.AddIpfsClusterResponse;

@FeignClient(
        name = "${feign.ipfs-cluster-write.name}",
        url = "${feign.ipfs-cluster-write.host}",
        configuration = FeignMultipartSupportConfiguration.class
)
public interface IpfsClusterWriteClient {

    @PostMapping(value = "/add", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    AddIpfsClusterResponse addFile(@RequestPart(value = "file") MultipartFile file);
}
