package ru.healthylife.healthylifecore.client;

import feign.Response;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;
import ru.healthylife.healthylifecore.configuration.FeignMultipartSupportConfiguration;
import ru.healthylife.healthylifecore.dto.ipfs.responses.AddResponse;

@FeignClient(
        name = "${feign.ipfs.name}",
        url = "${feign.ipfs.host}",
        path = IpfsClient.IPFS_PREFIX,
        configuration = FeignMultipartSupportConfiguration.class
)
public interface IpfsClient {
    String IPFS_PREFIX = "/api/v0";

    @PostMapping(value = "/add", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    AddResponse addFile(@RequestPart(value = "file") MultipartFile file);

    @PostMapping(value = "/cat", consumes = MediaType.TEXT_PLAIN_VALUE/*ipfs api*/)
    Response getFile(@RequestParam("arg") String hash);
}
