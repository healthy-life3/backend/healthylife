package ru.healthylife.healthylifecore.consts;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class ApiProtocolConstants {
    public static final String DATE_TIME_WITHOUT_SECONDS = "yyyy-MM-dd'T'HH:mm:ss";
}
