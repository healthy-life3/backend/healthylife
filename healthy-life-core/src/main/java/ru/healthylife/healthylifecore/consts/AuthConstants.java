package ru.healthylife.healthylifecore.consts;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class AuthConstants {
    public static final String ROLES = "roles";
}
