package ru.healthylife.healthylifecore.consts;

import lombok.experimental.UtilityClass;

@UtilityClass
public final class DatabaseConstants {
    public static final String GENERATOR_SUFFIX = "_gen";
    public static final String SEQUENCE_SUFFIX = "_seq";
    public static final String ID_SUFFIX = "_id";
}
