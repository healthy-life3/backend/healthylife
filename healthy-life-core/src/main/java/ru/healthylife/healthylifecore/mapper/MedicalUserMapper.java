package ru.healthylife.healthylifecore.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.healthylife.healthylifecore.domain.doctor.DoctorRegistrationInfo;
import ru.healthylife.healthylifecore.domain.patient.PatientRegistrationInfo;
import ru.healthylife.healthylifecore.domain.user.MedicalUserInfo;
import ru.healthylife.healthylifecore.domain.user.MedicalUserRegistrationInfo;
import ru.healthylife.healthylifecore.entity.user.MedicalUser;

@Mapper(uses = {RoleMapper.class, PhoneNumberMapper.class})
public interface MedicalUserMapper {

    MedicalUserInfo convert(MedicalUser medicalUser);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "roles", ignore = true)
    MedicalUser convert(MedicalUserRegistrationInfo medicalUserRegistrationInfo);

    @Mapping(target = "roles", ignore = true)
    MedicalUserRegistrationInfo convert(PatientRegistrationInfo patientRegistrationInfo);

    @Mapping(target = "roles", ignore = true)
    MedicalUserRegistrationInfo convert(DoctorRegistrationInfo doctorRegistrationInfo);
}
