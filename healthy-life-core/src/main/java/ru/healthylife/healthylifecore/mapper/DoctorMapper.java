package ru.healthylife.healthylifecore.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.healthylife.healthylifecore.domain.doctor.DoctorInfo;
import ru.healthylife.healthylifecore.domain.doctor.DoctorProfile;
import ru.healthylife.healthylifecore.domain.doctor.DoctorRegistrationInfo;
import ru.healthylife.healthylifecore.dto.doctor.DoctorProfileResponse;
import ru.healthylife.healthylifecore.dto.doctor.DoctorRegistrationRequest;
import ru.healthylife.healthylifecore.dto.doctor.DoctorVisitDto;
import ru.healthylife.healthylifecore.entity.doctor.Doctor;

@Mapper
public interface DoctorMapper {

    DoctorRegistrationInfo convert(DoctorRegistrationRequest doctorRegistrationRequest);

    @Mapping(target = "lastName", source = "medicalUser.lastName")
    @Mapping(target = "firstName", source = "medicalUser.firstName")
    @Mapping(target = "middleName", source = "medicalUser.middleName")
    @Mapping(target = "gender", source = "medicalUser.gender")
    @Mapping(target = "email", source = "medicalUser.email")
    @Mapping(target = "birthday", source = "medicalUser.birthday")
    @Mapping(target = "phoneNumber", source = "medicalUser.phoneNumber")
    DoctorProfile convert(Doctor doctor);

    DoctorProfileResponse convert(DoctorProfile doctor);

    DoctorVisitDto convertToVisitDto(DoctorInfo doctor);

    @Mapping(target = "lastName", source = "medicalUser.lastName")
    @Mapping(target = "firstName", source = "medicalUser.firstName")
    @Mapping(target = "middleName", source = "medicalUser.middleName")
    @Mapping(target = "gender", source = "medicalUser.gender")
    @Mapping(target = "email", source = "medicalUser.email")
    @Mapping(target = "birthday", source = "medicalUser.birthday")
    @Mapping(target = "phoneNumber", source = "medicalUser.phoneNumber")
    DoctorInfo convertToDomain(Doctor doctor);
}
