package ru.healthylife.healthylifecore.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.healthylife.healthylifecore.domain.patient.PhoneNumberInfo;
import ru.healthylife.healthylifecore.dto.PhoneNumberDto;
import ru.healthylife.healthylifecore.entity.user.PhoneNumber;

@Mapper
public interface PhoneNumberMapper {

    PhoneNumberInfo convert(PhoneNumberDto phoneNumberDto);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "medicalUser", ignore = true)
    PhoneNumber convert(PhoneNumberInfo phoneNumberInfo);
}
