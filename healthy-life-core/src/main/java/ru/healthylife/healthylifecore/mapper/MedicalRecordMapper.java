package ru.healthylife.healthylifecore.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.healthylife.healthylifecore.domain.patient.MedicalRecordInfo;
import ru.healthylife.healthylifecore.dto.doctor.DoctorNames;
import ru.healthylife.healthylifecore.dto.patient.MedicalRecordResponse;
import ru.healthylife.healthylifecore.entity.doctor.Doctor;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecord;

import java.util.List;

@Mapper
public interface MedicalRecordMapper {

    @Mapping(target = "medicalOrganization", source = "doctor.medicalOrganization.name")
    MedicalRecordInfo convert(MedicalRecord medicalRecord);

    List<MedicalRecordInfo> convertToDomain(List<MedicalRecord> medicalRecords);

    List<MedicalRecordResponse> convertToDto(List<MedicalRecordInfo> medicalRecordInfos);

    @Mapping(target = "lastName", source = "medicalUser.lastName")
    @Mapping(target = "firstName", source = "medicalUser.firstName")
    @Mapping(target = "middleName", source = "medicalUser.middleName")
    DoctorNames convert(Doctor doctor);
}
