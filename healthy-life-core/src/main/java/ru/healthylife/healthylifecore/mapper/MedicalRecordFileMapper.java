package ru.healthylife.healthylifecore.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.springframework.web.multipart.MultipartFile;
import ru.healthylife.healthylifecore.domain.ehr.MedicalRecordFileInfo;
import ru.healthylife.healthylifecore.domain.ehr.NewMedicalRecordFile;
import ru.healthylife.healthylifecore.dto.ehr.MedicalRecordFileDto;
import ru.healthylife.healthylifecore.dto.ehr.NewMedicalRecordFileDto;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecordFile;

@Mapper(uses = DoctorMapper.class)
public interface MedicalRecordFileMapper {

    @Mapping(target = "file.name", source = "fileName")
    @Mapping(target = "file.type", source = "fileType")
    @Mapping(target = "file.data", source = "data")
    @Mapping(target = "title", source = "medicalRecord.title")
    @Mapping(target = "dateTime", source = "medicalRecord.dateTime")
    @Mapping(target = "doctor", source = "medicalRecord.doctor")
    @Mapping(target = "medicalOrganization", source = "medicalRecord.doctor.medicalOrganization.name")
    MedicalRecordFileInfo convertToDomain(MedicalRecordFile medicalRecordFile);

    @Mapping(target = "fileMetaData.path", ignore = true)
    @Mapping(target = "fileMetaData.name", source = "file.name")
    @Mapping(target = "fileMetaData.type", source = "file.type")
    MedicalRecordFileDto convertToDto(MedicalRecordFileInfo newMedicalRecordFileInfo);


    NewMedicalRecordFile convertToDomain(NewMedicalRecordFileDto medicalRecordFileDto, String doctorEmail, MultipartFile multipartFile);
}
