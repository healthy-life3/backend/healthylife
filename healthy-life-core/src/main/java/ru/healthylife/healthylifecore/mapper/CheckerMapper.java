package ru.healthylife.healthylifecore.mapper;

import org.mapstruct.Mapper;
import ru.healthylife.healthylifecore.domain.check.EmailCheckInfo;
import ru.healthylife.healthylifecore.domain.check.PhoneNumberCheckInfo;
import ru.healthylife.healthylifecore.dto.check.EmailDto;
import ru.healthylife.healthylifecore.dto.check.PhoneNumberCheckDto;

@Mapper
public interface CheckerMapper {

    EmailCheckInfo convertToDomain(EmailDto emailDto);

    PhoneNumberCheckInfo convertToDomain(PhoneNumberCheckDto phoneNumberCheckDto);
}
