package ru.healthylife.healthylifecore.mapper;

import org.mapstruct.Mapper;
import ru.healthylife.healthylifecore.domain.ehr.IcdInfo;
import ru.healthylife.healthylifecore.dto.ehr.IcdDto;
import ru.healthylife.healthylifecore.entity.ehr.Icd;

import java.util.List;

@Mapper
public interface IcdMapper {

    List<IcdInfo> convertToDomain(List<Icd> icds);

    List<IcdDto> convertToDto(List<IcdInfo> icds);
}
