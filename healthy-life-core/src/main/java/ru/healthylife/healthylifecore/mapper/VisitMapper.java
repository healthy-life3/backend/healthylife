package ru.healthylife.healthylifecore.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.healthylife.healthylifecore.domain.ehr.NewVisit;
import ru.healthylife.healthylifecore.domain.ehr.VisitInfo;
import ru.healthylife.healthylifecore.dto.ehr.NewVisitDto;
import ru.healthylife.healthylifecore.dto.ehr.VisitDto;
import ru.healthylife.healthylifecore.entity.ehr.Visit;

@Mapper(uses = {IcdMapper.class, DoctorMapper.class})
public interface VisitMapper {

    @Mapping(target = "title", source = "medicalRecord.title")
    @Mapping(target = "dateTime", source = "medicalRecord.dateTime")
    @Mapping(target = "doctor", source = "medicalRecord.doctor")
    @Mapping(target = "medicalOrganization", source = "medicalRecord.doctor.medicalOrganization.name")
    VisitInfo convertToDomain(Visit visit);

    VisitDto convertToDto(VisitInfo visitInfo);

    NewVisit convertToDomain(NewVisitDto newVisit, String doctorEmail);

    @Mapping(target = "medicalRecord", ignore = true)
    @Mapping(target = "medicalRecordUuid", ignore = true)
    Visit convertToEntity(NewVisit newVisit);
}
