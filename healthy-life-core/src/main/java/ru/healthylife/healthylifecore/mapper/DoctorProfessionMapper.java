package ru.healthylife.healthylifecore.mapper;

import org.mapstruct.Mapper;
import ru.healthylife.healthylifecore.domain.doctor.DoctorProfessionInfo;
import ru.healthylife.healthylifecore.dto.doctor.DoctorProfessionDto;
import ru.healthylife.healthylifecore.entity.doctor.DoctorProfession;

import java.util.List;

@Mapper
public interface DoctorProfessionMapper {

    List<DoctorProfessionInfo> mapToDomain(List<DoctorProfession> doctorProfessions);

    List<DoctorProfessionDto> mapToDto(List<DoctorProfessionInfo> doctorProfessions);
}
