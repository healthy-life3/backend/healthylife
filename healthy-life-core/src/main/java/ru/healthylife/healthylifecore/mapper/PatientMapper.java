package ru.healthylife.healthylifecore.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.healthylife.healthylifecore.domain.patient.PatientProfile;
import ru.healthylife.healthylifecore.domain.patient.PatientRegistrationInfo;
import ru.healthylife.healthylifecore.dto.patient.PatientProfileResponse;
import ru.healthylife.healthylifecore.dto.patient.PatientRegistrationRequest;
import ru.healthylife.healthylifecore.entity.patient.Patient;

@Mapper
public interface PatientMapper {

    PatientRegistrationInfo convert(PatientRegistrationRequest patientRegistrationRequest);

    @Mapping(target = "id", ignore = true)
    @Mapping(target = "address.id", ignore = true)
    @Mapping(target = "medicalUser", ignore = true)
    @Mapping(target = "medicalRecords", ignore = true)
    @Mapping(target = "generalMedicalInfo", ignore = true)
    @Mapping(target = "patientRepresentatives", ignore = true)
    Patient convert(PatientRegistrationInfo patientRegistrationInfo);

    @Mapping(target = "lastName", source = "medicalUser.lastName")
    @Mapping(target = "firstName", source = "medicalUser.firstName")
    @Mapping(target = "middleName", source = "medicalUser.middleName")
    @Mapping(target = "gender", source = "medicalUser.gender")
    @Mapping(target = "email", source = "medicalUser.email")
    @Mapping(target = "birthday", source = "medicalUser.birthday")
    @Mapping(target = "phoneNumber", source = "medicalUser.phoneNumber")
    PatientProfile convert(Patient patient);

    PatientProfileResponse convert(PatientProfile patientProfile);
}
