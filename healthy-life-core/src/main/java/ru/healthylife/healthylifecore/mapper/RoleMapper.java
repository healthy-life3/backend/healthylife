package ru.healthylife.healthylifecore.mapper;

import org.mapstruct.*;
import ru.healthylife.healthylifecore.domain.user.RoleType;
import ru.healthylife.healthylifecore.entity.user.Role;
import ru.healthylife.healthylifecore.entity.user.RoleName;

import javax.annotation.Nonnull;


@Mapper
public interface RoleMapper {

    @Nonnull
    default RoleName convert(@Nonnull Role role) {
        return role.getName();
    }

    @EnumMapping(
            nameTransformationStrategy = MappingConstants.STRIP_PREFIX_TRANSFORMATION,
            configuration = "ROLE_"
    )
    RoleType convert(RoleName roleName);

    @InheritInverseConfiguration
    RoleName convert(RoleType roleType);
}
