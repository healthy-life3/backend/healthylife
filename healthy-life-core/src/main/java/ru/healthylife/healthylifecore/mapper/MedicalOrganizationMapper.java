package ru.healthylife.healthylifecore.mapper;

import org.mapstruct.Mapper;
import ru.healthylife.healthylifecore.domain.doctor.MedicalOrganizationInfo;
import ru.healthylife.healthylifecore.dto.doctor.MedicalOrganizationDto;
import ru.healthylife.healthylifecore.entity.ehr.MedicalOrganization;

import java.util.Collection;

@Mapper
public interface MedicalOrganizationMapper {

    Collection<MedicalOrganizationInfo> convertToDomain(Collection<MedicalOrganization> medicalOrganizations);

    Collection<MedicalOrganizationDto> convertToDto(Collection<MedicalOrganizationInfo> medicalOrganizations);
}
