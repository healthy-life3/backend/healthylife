package ru.healthylife.healthylifecore.mapper;

import org.mapstruct.Mapper;
import ru.healthylife.healthylifecore.domain.auth.JwtTokenInfo;
import ru.healthylife.healthylifecore.dto.auth.JwtToken;

@Mapper
public interface TokenMapper {
    JwtToken convertToDto(JwtTokenInfo jwtTokenInfo);
}
