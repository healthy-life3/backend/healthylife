package ru.healthylife.healthylifecore.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import ru.healthylife.healthylifecore.domain.ehr.LaboratoryTestInfo;
import ru.healthylife.healthylifecore.domain.ehr.NewLaboratoryTest;
import ru.healthylife.healthylifecore.dto.ehr.LaboratoryTestDto;
import ru.healthylife.healthylifecore.dto.ehr.NewLaboratoryTestDto;
import ru.healthylife.healthylifecore.entity.ehr.LaboratoryTest;

@Mapper(uses = DoctorMapper.class)
public interface LaboratoryTestMapper {

    @Mapping(target = "title", source = "medicalRecord.title")
    @Mapping(target = "dateTime", source = "medicalRecord.dateTime")
    @Mapping(target = "doctor", source = "medicalRecord.doctor")
    @Mapping(target = "medicalOrganization", source = "medicalRecord.doctor.medicalOrganization.name")
    LaboratoryTestInfo convertToDomain(LaboratoryTest laboratoryTest);

    LaboratoryTestDto convertToDto(LaboratoryTestInfo visitInfo);

    @Mapping(target = "medicalRecord", ignore = true)
    @Mapping(target = "medicalRecordUuid", ignore = true)
    LaboratoryTest convertToEntity(NewLaboratoryTest newLaboratoryTest);

    NewLaboratoryTest convertToDomain(NewLaboratoryTestDto newLaboratoryTestDto, String doctorEmail);
}
