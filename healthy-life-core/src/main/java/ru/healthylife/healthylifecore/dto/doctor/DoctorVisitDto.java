package ru.healthylife.healthylifecore.dto.doctor;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(name = "DoctorVisit")
public class DoctorVisitDto {
    @Schema(example = "Ivanov")
    private String lastName;
    @Schema(example = "Ivan")
    private String firstName;
    @Schema(example = "Ivanovich")
    private String middleName;
    @Schema(example = "ivanov@gmail.com")
    private String email;
    private DoctorProfessionDto profession;
}
