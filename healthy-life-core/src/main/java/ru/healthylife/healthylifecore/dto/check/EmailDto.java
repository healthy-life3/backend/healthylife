package ru.healthylife.healthylifecore.dto.check;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.Email;

@Data
@Schema(name = "Email")
public class EmailDto {
    @Email
    @Schema(description = "Email", example = "ivanov@gmail.com")
    private String email;
}
