package ru.healthylife.healthylifecore.dto.ehr;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import ru.healthylife.healthylifecore.consts.ApiProtocolConstants;
import ru.healthylife.healthylifecore.dto.doctor.DoctorVisitDto;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
@Schema(name = "Laboratory test")
public class LaboratoryTestDto {
    @Schema(description = "Unique medical record id", example = "123e4567-e89b-42d3-a456-556642440000")
    private UUID medicalRecordUuid;
    @Schema(description = "Visit title", example = "Consultation by general practitioner")
    private String title;
    @Schema(description = "Created laboratory test time (zero offset)")
    @JsonFormat(pattern = ApiProtocolConstants.DATE_TIME_WITHOUT_SECONDS)
    private LocalDateTime dateTime;
    @Schema(description = "Doctor who created visit record")
    private DoctorVisitDto doctor;
    @Schema(description = "Medical organization where visit was registered", example = "INVITRO")
    private String medicalOrganization;
    @Schema(description = "Name parameter", example = "Leukocytes(WBC)")
    private String paramName;
    @Schema(description = "Value parameter", example = "12")
    private String paramValue;
    @Schema(description = "Normal value", example = "4-9")
    private String normalParamValue;
    @Schema(description = "Unit measure", example = "10^9/L")
    private String unitMeasure;
    @Schema(description = "Doctor comment", example = "Should see a therapist")
    private String comment;
}
