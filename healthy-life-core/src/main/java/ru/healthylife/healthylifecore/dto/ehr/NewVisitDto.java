package ru.healthylife.healthylifecore.dto.ehr;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@Schema(name = "NewVisit")
public class NewVisitDto {
    @NotBlank
    @Schema(name = "Patient's email", example = "ivanov@gmail.com")
    private String patientEmail;
    @NotBlank
    @Schema(description = "Visit title", example = "Consultation by general practitioner")
    private String title;
    @NotEmpty
    @Schema(description = "Visit icds")
    private List<IcdDto> icds;
    @NotBlank
    @Schema(description = "Complaints", example = "Severe cough and fever")
    private String complaints;
    @NotBlank
    @Schema(description = "Diagnosis", example = "Bilateral pneumonia")
    private String diagnosis;
    @NotBlank
    @Schema(description = "Treatment plan", example = "Stay at home and take pills")
    private String treatmentPlan;
    @NotBlank
    @Schema(description = "Prescriptions", example = "Antibiotics: Zithromax")
    private String prescriptions;
}
