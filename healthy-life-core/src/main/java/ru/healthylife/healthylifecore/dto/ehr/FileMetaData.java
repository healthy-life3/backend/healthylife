package ru.healthylife.healthylifecore.dto.ehr;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import static ru.healthylife.healthylifecore.controller.MedicalRecordFileController.*;

@Data
@Schema(name = "File meta data")
public class FileMetaData {
    @Schema(description = "File name", example = "ivanovBloodLab.pdf")
    private String name;
    @Schema(description = "File type", example = "application/pdf")
    private String type;
    @Schema(
            description = "Full path to download file",
            example = "http://healthy-life" + MEDICAL_RECORD_FILES + DOWNLOAD + "/123e4567-e89b-42d3-a456-556642440000"
    )
    private String path;
}
