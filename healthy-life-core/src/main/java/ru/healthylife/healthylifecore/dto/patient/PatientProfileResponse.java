package ru.healthylife.healthylifecore.dto.patient;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import ru.healthylife.healthylifecore.dto.PhoneNumberDto;
import ru.healthylife.healthylifecore.entity.user.Gender;

import java.time.LocalDate;

@Data
@Schema(name = "PatientProfile")
public class PatientProfileResponse {
    private String lastName;
    private String firstName;
    private String middleName;
    private Gender gender;
    private String email;
    private LocalDate birthday;
    private PhoneNumberDto phoneNumber;
    private String snils;
}
