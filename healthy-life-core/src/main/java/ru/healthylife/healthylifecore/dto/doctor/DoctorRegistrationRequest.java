package ru.healthylife.healthylifecore.dto.doctor;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;
import ru.healthylife.healthylifecore.dto.PhoneNumberDto;
import ru.healthylife.healthylifecore.entity.user.Gender;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Data
@Schema(name = "DoctorRegistration")
public class DoctorRegistrationRequest {
    @NotBlank
    private String lastName;
    @NotBlank
    private String firstName;
    private String middleName;
    @Valid
    @NotNull
    private PhoneNumberDto phoneNumber;
    @Past
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate birthday;
    @NotBlank
    @ToString.Exclude
    private String password;
    @Email
    @NotBlank
    private String email;
    @NotNull
    private Gender gender;
    @Positive
    @NotNull
    private Long doctorProfessionId;
    @Positive
    @NotNull
    private Long medicalOrganizationId;
}
