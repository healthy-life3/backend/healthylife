package ru.healthylife.healthylifecore.dto.doctor;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import ru.healthylife.healthylifecore.dto.PhoneNumberDto;
import ru.healthylife.healthylifecore.entity.user.Gender;

import java.time.LocalDate;

@Data
@Schema(name = "DoctorProfile")
public class DoctorProfileResponse {
    private String lastName;
    private String firstName;
    private String middleName;
    private LocalDate birthday;
    private Gender gender;
    private String email;
    private PhoneNumberDto phoneNumber;
    private DoctorProfessionDto profession;
}
