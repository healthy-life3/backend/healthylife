package ru.healthylife.healthylifecore.dto.doctor;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(name = "MedicalOrganization")
public class MedicalOrganizationDto {
    @Schema(name = "Medical organization id", example = "1")
    private Long id;
    @Schema(name = "Medical organization name", example = "INVITRO")
    private String name;
}
