package ru.healthylife.healthylifecore.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Schema(name = "PhoneNumber")
public class PhoneNumberDto {
    @NotBlank
    @Size(min = 10, max = 15)
    private String fullNumber;
    @NotBlank
    @Size(min = 1, max = 3)
    private String countryCode;
    @NotBlank
    @Size(min = 9, max = 12)
    private String subscriberNumber;
}
