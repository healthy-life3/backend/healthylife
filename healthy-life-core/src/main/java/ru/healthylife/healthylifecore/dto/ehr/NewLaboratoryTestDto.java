package ru.healthylife.healthylifecore.dto.ehr;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@Schema(name = "NewLaboratoryTest")
public class NewLaboratoryTestDto {
    @NotBlank
    @Schema(name = "Patient's email", example = "ivanov@gmail.com")
    private String patientEmail;
    @NotBlank
    @Schema(description = "Laboratory test title", example = "Blood test")
    private String title;
    @Schema(description = "Name parameter", example = "Leukocytes(WBC)")
    private String paramName;
    @Schema(description = "Value parameter", example = "12")
    private String paramValue;
    @Schema(description = "Normal value", example = "4-9")
    private String normalParamValue;
    @Schema(description = "Unit measure", example = "10^9/L")
    private String unitMeasure;
    @Schema(description = "Doctor comment", example = "Should see a therapist")
    private String comment;
}
