package ru.healthylife.healthylifecore.dto.ehr;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
@Schema(name = "NewMedicalRecordFile")
public class NewMedicalRecordFileDto {
    @NotBlank
    @Schema(name = "Patient's email", example = "ivanov@gmail.com")
    private String patientEmail;
    @NotBlank
    @Schema(description = "Laboratory test title", example = "Blood test")
    private String title;
}
