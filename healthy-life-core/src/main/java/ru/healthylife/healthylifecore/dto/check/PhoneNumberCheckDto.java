package ru.healthylife.healthylifecore.dto.check;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Schema(name = "FullPhoneNumber")
public class PhoneNumberCheckDto {
    @NotBlank
    @Size(min = 10, max = 15)
    @Schema(name = "Full phone number", example = "79134567898")
    private String fullNumber;
}
