package ru.healthylife.healthylifecore.dto.patient;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.springframework.format.annotation.DateTimeFormat;
import ru.healthylife.healthylifecore.dto.PhoneNumberDto;
import ru.healthylife.healthylifecore.entity.user.Gender;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.time.LocalDate;

@Getter
@Setter
@ToString
@NoArgsConstructor
@Schema(name = "PatientRegistration")
public class PatientRegistrationRequest {
    @NotBlank
    private String lastName;
    @NotBlank
    private String firstName;
    private String middleName;
    @Valid
    @NotNull
    private PhoneNumberDto phoneNumber;
    @Past
    @NotNull
    @DateTimeFormat(iso = DateTimeFormat.ISO.DATE)
    private LocalDate birthday;
    @NotBlank
    @ToString.Exclude
    private String password;
    @Email
    @NotBlank
    private String email;
    @NotNull
    private Gender gender;
    @NotBlank
    @Size(min = 11, max = 11, message = "{validation.snils.size}")
    private String snils;
}
