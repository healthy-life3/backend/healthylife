package ru.healthylife.healthylifecore.dto.check;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Schema(name = "Snils")
public class SnilsDto {
    @NotBlank
    @Size(min = 11, max = 11, message = "{validation.snils.size}")
    @Schema(description = "Snils",example = "12345678902")
    private String snils;
}
