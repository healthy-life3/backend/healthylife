package ru.healthylife.healthylifecore.dto.ipfs.responses;

import lombok.Data;

import java.util.List;

@Data
public class AddIpfsClusterResponse {
    private String name;
    private String cid;
    private Integer size;
    private List<String> allocations;
}
