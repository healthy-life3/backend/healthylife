package ru.healthylife.healthylifecore.dto.auth;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Schema(name = "JwtToken")
public class JwtToken {
    @Schema(description = "JWT token", example = "xxxxx.yyyyy.zzzzz")
    private String accessToken;
    @Schema(
            description = "Refresh token. " +
                    "It allows you to have short-lived access tokens without having to collect credentials every time one expires",
            example = "xxxxx.yyyyy.zzzzz"
    )
    private String refreshToken;
}
