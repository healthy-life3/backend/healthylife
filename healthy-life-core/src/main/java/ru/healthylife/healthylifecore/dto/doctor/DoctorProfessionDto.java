package ru.healthylife.healthylifecore.dto.doctor;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@Schema(name = "DoctorProfession")
public class DoctorProfessionDto {
    @NotNull
    @Schema(description = "Doctor profession's id")
    private Long id;
    @NotBlank
    @Schema(description = "Doctor profession name", example = "Therapist")
    private String name;
}
