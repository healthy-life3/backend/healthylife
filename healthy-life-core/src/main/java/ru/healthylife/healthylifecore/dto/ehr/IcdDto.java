package ru.healthylife.healthylifecore.dto.ehr;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@Schema(name = "Icd(International Classification of Diseases)")
public class IcdDto {
    @Schema(description = "Code from ICD", example = "G44.311")
    private String code;
    @Schema(description = "Code description", example = "Acute post traumatic headache, intractable")
    private String description;
}
