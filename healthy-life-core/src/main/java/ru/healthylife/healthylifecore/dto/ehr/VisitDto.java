package ru.healthylife.healthylifecore.dto.ehr;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import ru.healthylife.healthylifecore.consts.ApiProtocolConstants;
import ru.healthylife.healthylifecore.dto.doctor.DoctorVisitDto;

import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@Schema(name = "Visit")
public class VisitDto {
    @Schema(description = "Unique medical record id", example = "123e4567-e89b-42d3-a456-556642440000")
    private UUID medicalRecordUuid;
    @Schema(description = "Visit title", example = "Consultation by general practitioner")
    private String title;
    @Schema(description = "Created visit time (zero offset)")
    @JsonFormat(pattern = ApiProtocolConstants.DATE_TIME_WITHOUT_SECONDS)
    private LocalDateTime dateTime;
    @Schema(description = "Doctor who created visit record")
    private DoctorVisitDto doctor;
    @Schema(description = "Medical organization where visit was registered", example = "INVITRO")
    private String medicalOrganization;
    @Schema(description = "Visit icds")
    private List<IcdDto> icds;
    @Schema(description = "Complaints", example = "Severe cough and fever")
    private String complaints;
    @Schema(description = "Diagnosis", example = "Bilateral pneumonia")
    private String diagnosis;
    @Schema(description = "Treatment plan", example = "Stay at home and take pills")
    private String treatmentPlan;
    @Schema(description = "Prescriptions", example = "Antibiotics: Zithromax")
    private String prescriptions;
}
