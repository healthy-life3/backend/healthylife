package ru.healthylife.healthylifecore.dto.check;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
@Schema(name = "Check result")
public class CheckResult {
    private boolean exist;
}
