package ru.healthylife.healthylifecore.dto.ipfs.responses;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class AddResponse {
    @JsonProperty("Hash")
    private String hash;
    @JsonProperty("Name")
    private String name;
    @JsonProperty("Size")
    private Long size;
}
