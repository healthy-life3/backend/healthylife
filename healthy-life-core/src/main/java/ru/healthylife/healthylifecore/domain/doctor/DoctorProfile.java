package ru.healthylife.healthylifecore.domain.doctor;

import lombok.Data;
import ru.healthylife.healthylifecore.domain.patient.PhoneNumberInfo;
import ru.healthylife.healthylifecore.entity.user.Gender;

import java.time.LocalDate;

@Data
public class DoctorProfile {
    private String lastName;
    private String firstName;
    private String middleName;
    private Gender gender;
    private String email;
    private LocalDate birthday;
    private PhoneNumberInfo phoneNumber;
    private DoctorProfessionInfo profession;
    private MedicalOrganizationInfo medicalOrganization;
}
