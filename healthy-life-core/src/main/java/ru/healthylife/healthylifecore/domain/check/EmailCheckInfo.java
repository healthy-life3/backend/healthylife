package ru.healthylife.healthylifecore.domain.check;

import lombok.Data;

@Data
public class EmailCheckInfo {
    private String email;
}
