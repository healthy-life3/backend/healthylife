package ru.healthylife.healthylifecore.domain.user;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
public class MedicalUserInfo {
    private Long id;
    private String lastName;
    private String firstName;
    private String middleName;
    private String email;
    private Set<RoleType> roles = new HashSet<>();
}
