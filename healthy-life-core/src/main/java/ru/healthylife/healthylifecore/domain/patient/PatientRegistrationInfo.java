package ru.healthylife.healthylifecore.domain.patient;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import ru.healthylife.healthylifecore.entity.user.Gender;

import java.time.LocalDate;

@Getter
@Setter
@NoArgsConstructor
public class PatientRegistrationInfo {
    private String firstName;
    private String middleName;
    private String lastName;
    private PhoneNumberInfo phoneNumber;
    private LocalDate birthday;
    @ToString.Exclude
    private String password;
    private String email;
    private Gender gender;
    private String snils;
}
