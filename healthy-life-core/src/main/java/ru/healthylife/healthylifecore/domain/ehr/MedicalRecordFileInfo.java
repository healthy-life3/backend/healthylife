package ru.healthylife.healthylifecore.domain.ehr;

import lombok.Data;
import ru.healthylife.healthylifecore.domain.doctor.DoctorInfo;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class MedicalRecordFileInfo {
    private UUID medicalRecordUuid;
    private String title;
    private LocalDateTime dateTime;
    private DoctorInfo doctor;
    private String medicalOrganization;
    private FileInfo file;
}
