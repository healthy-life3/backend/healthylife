package ru.healthylife.healthylifecore.domain.ehr;

import lombok.Data;
import ru.healthylife.healthylifecore.domain.doctor.DoctorInfo;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class LaboratoryTestInfo {
    private UUID medicalRecordUuid;
    private String title;
    private LocalDateTime dateTime;
    private DoctorInfo doctor;
    private String medicalOrganization;
    private String paramName;
    private String paramValue;
    private String normalParamValue;
    private String unitMeasure;
    private String comment;
}
