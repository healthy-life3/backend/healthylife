package ru.healthylife.healthylifecore.domain.patient;

import lombok.Data;
import ru.healthylife.healthylifecore.entity.user.Gender;

import java.time.LocalDate;

@Data
public class PatientProfile {
    private String lastName;
    private String firstName;
    private String middleName;
    private Gender gender;
    private String email;
    private LocalDate birthday;
    private PhoneNumberInfo phoneNumber;
    private String snils;
}
