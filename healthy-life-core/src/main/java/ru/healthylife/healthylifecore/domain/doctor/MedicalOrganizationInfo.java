package ru.healthylife.healthylifecore.domain.doctor;

import lombok.Data;

@Data
public class MedicalOrganizationInfo {
    private Long id;
    private String name;
}
