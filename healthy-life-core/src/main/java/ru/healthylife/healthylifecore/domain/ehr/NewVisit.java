package ru.healthylife.healthylifecore.domain.ehr;

import lombok.Data;

import java.util.List;

@Data
public class NewVisit implements NewMedicalRecord {
    private String doctorEmail;
    private String patientEmail;
    private String title;
    private List<IcdInfo> icds;
    private String complaints;
    private String diagnosis;
    private String treatmentPlan;
    private String prescriptions;
}
