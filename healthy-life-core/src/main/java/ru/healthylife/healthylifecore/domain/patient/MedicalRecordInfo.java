package ru.healthylife.healthylifecore.domain.patient;

import lombok.Data;
import ru.healthylife.healthylifecore.entity.doctor.Doctor;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecordType;

import java.time.LocalDateTime;
import java.util.UUID;

@Data
public class MedicalRecordInfo {
    private UUID medicalRecordUuid;
    private MedicalRecordType medicalRecordType;
    private String title;
    private LocalDateTime dateTime;
    private Doctor doctor;
    private String medicalOrganization;
}
