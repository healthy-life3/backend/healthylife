package ru.healthylife.healthylifecore.domain.ehr;

import javax.annotation.Nonnull;

public interface NewMedicalRecord {

    @Nonnull
    String getDoctorEmail();

    @Nonnull
    String getPatientEmail();

    @Nonnull
    String getTitle();
}
