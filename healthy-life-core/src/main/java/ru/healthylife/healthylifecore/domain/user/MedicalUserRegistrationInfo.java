package ru.healthylife.healthylifecore.domain.user;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import ru.healthylife.healthylifecore.domain.patient.PhoneNumberInfo;
import ru.healthylife.healthylifecore.entity.user.Gender;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor
public class MedicalUserRegistrationInfo {
    private String lastName;
    private String firstName;
    private String middleName;
    private String email;
    @ToString.Exclude
    private String password;
    private LocalDate birthday;
    private Gender gender;
    private PhoneNumberInfo phoneNumber;
    private Set<RoleType> roles = new HashSet<>();
}
