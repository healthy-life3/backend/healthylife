package ru.healthylife.healthylifecore.domain.doctor;

import lombok.Data;
import lombok.ToString;
import ru.healthylife.healthylifecore.domain.patient.PhoneNumberInfo;
import ru.healthylife.healthylifecore.entity.user.Gender;

import java.time.LocalDate;

@Data
public class DoctorRegistrationInfo {
    private String firstName;
    private String middleName;
    private String lastName;
    private PhoneNumberInfo phoneNumber;
    private LocalDate birthday;
    @ToString.Exclude
    private String password;
    private String email;
    private Gender gender;
    private Long doctorProfessionId;
    private Long medicalOrganizationId;
}
