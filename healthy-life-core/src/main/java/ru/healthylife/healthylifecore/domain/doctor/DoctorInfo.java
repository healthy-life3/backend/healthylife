package ru.healthylife.healthylifecore.domain.doctor;

import lombok.Data;
import ru.healthylife.healthylifecore.domain.patient.PhoneNumberInfo;
import ru.healthylife.healthylifecore.entity.user.Gender;

import java.time.LocalDate;

@Data
public class DoctorInfo {
    private String lastName;
    private String firstName;
    private String middleName;
    private String email;
    private LocalDate birthday;
    private Gender gender;
    private PhoneNumberInfo phoneNumber;
    private DoctorProfessionInfo profession;
    private MedicalOrganizationInfo medicalOrganization;
}
