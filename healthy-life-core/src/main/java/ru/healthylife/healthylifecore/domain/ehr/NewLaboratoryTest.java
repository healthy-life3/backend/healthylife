package ru.healthylife.healthylifecore.domain.ehr;

import lombok.Data;

@Data
public class NewLaboratoryTest implements NewMedicalRecord {
    private String doctorEmail;
    private String patientEmail;
    private String title;
    private String paramName;
    private String paramValue;
    private String normalParamValue;
    private String unitMeasure;
    private String comment;
}
