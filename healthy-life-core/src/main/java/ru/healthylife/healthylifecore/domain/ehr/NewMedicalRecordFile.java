package ru.healthylife.healthylifecore.domain.ehr;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

@Data
public class NewMedicalRecordFile implements NewMedicalRecord {
    private String doctorEmail;
    private String patientEmail;
    private String title;
    private MultipartFile multipartFile;
}
