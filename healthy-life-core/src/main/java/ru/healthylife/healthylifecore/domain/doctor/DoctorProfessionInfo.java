package ru.healthylife.healthylifecore.domain.doctor;

import lombok.Data;

@Data
public class DoctorProfessionInfo {
    private Long id;
    private String name;
}
