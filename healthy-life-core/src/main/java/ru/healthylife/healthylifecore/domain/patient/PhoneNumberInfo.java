package ru.healthylife.healthylifecore.domain.patient;

import lombok.Data;

@Data
public class PhoneNumberInfo {
    private String fullNumber;
    private String countryCode;
    private String subscriberNumber;
}
