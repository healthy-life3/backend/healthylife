package ru.healthylife.healthylifecore.domain.check;

import lombok.Data;

@Data
public class PhoneNumberCheckInfo {
    private String fullNumber;
}
