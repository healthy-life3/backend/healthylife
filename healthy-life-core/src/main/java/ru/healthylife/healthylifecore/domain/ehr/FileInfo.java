package ru.healthylife.healthylifecore.domain.ehr;

import lombok.Data;

@Data
public class FileInfo {
    private String name;
    private String type;
    private byte[] data;
}
