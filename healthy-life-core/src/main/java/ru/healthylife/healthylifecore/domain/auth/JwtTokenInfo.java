package ru.healthylife.healthylifecore.domain.auth;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JwtTokenInfo {
    private String accessToken;
    private String refreshToken;
}
