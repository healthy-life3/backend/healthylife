package ru.healthylife.healthylifecore.domain.user;

public enum RoleType {
    DOCTOR,
    PATIENT
}
