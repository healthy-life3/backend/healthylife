package ru.healthylife.healthylifecore.domain.ehr;

import lombok.Data;

@Data
public class IcdInfo {
    private String code;
    private String description;
}
