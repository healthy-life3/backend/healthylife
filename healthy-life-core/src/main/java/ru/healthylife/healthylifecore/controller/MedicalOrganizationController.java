package ru.healthylife.healthylifecore.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.healthylife.healthylifecore.dto.doctor.MedicalOrganizationDto;
import ru.healthylife.healthylifecore.mapper.MedicalOrganizationMapper;
import ru.healthylife.healthylifecore.service.MedicalOrganizationService;

import java.util.Collection;

@RestController
@AllArgsConstructor
@Tag(name = "Medical organization", description = "Medical organization API")
@RequestMapping(path = MedicalOrganizationController.MEDICAL_ORGAIZATION, produces = MediaType.APPLICATION_JSON_VALUE)
public class MedicalOrganizationController {
    protected static final String MEDICAL_ORGAIZATION = "/medical-organizations";

    private final MedicalOrganizationMapper medicalOrganizationMapper;
    private final MedicalOrganizationService medicalOrganizationService;

    @GetMapping
    public ResponseEntity<Collection<MedicalOrganizationDto>> getAllMedicalOrganizations() {
        return ResponseEntity.ok(medicalOrganizationMapper.convertToDto(medicalOrganizationService.findAllMedicalOrganizations()));
    }
}
