package ru.healthylife.healthylifecore.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.healthylife.healthylifecore.configuration.SwaggerConfiguration;
import ru.healthylife.healthylifecore.domain.patient.MedicalRecordInfo;
import ru.healthylife.healthylifecore.domain.patient.PatientProfile;
import ru.healthylife.healthylifecore.dto.patient.MedicalRecordResponse;
import ru.healthylife.healthylifecore.dto.patient.PatientProfileResponse;
import ru.healthylife.healthylifecore.mapper.MedicalRecordMapper;
import ru.healthylife.healthylifecore.mapper.PatientMapper;
import ru.healthylife.healthylifecore.service.PatientService;

import javax.annotation.Nonnull;
import java.security.Principal;
import java.util.List;

@RestController
@AllArgsConstructor
@Tag(name = "Patient", description = "The Patient API")
@RequestMapping(value = PatientController.PATIENTS, produces = MediaType.APPLICATION_JSON_VALUE)
public class PatientController {
    public static final String PATIENTS = "/patients";
    public static final String PROFILE = "/profile";
    public static final String MEDICAL_RECORD = "/medical-records";

    private final PatientMapper patientMapper;
    private final PatientService patientService;
    private final MedicalRecordMapper medicalRecordMapper;

    @GetMapping(PROFILE)
    @SecurityRequirement(name = SwaggerConfiguration.HEALTHY_LIFE_OPEN_API_BEARER)
    public ResponseEntity<PatientProfileResponse> getProfile(@Nonnull Principal principal) {
        PatientProfile profileByEmail = patientService.getProfileByEmail(principal.getName());
        return ResponseEntity.ok(patientMapper.convert(profileByEmail));
    }

    @GetMapping(MEDICAL_RECORD)
    @SecurityRequirement(name = SwaggerConfiguration.HEALTHY_LIFE_OPEN_API_BEARER)
    public ResponseEntity<List<MedicalRecordResponse>> getAllMedicalData(@Nonnull Principal principal) {
        List<MedicalRecordInfo> patientMedicalRecordInfos = patientService.getAllMedicalRecords(principal.getName());
        return ResponseEntity.ok(medicalRecordMapper.convertToDto(patientMedicalRecordInfos));
    }
}
