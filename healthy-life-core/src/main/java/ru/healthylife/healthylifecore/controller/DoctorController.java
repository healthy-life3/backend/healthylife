package ru.healthylife.healthylifecore.controller;

import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.healthylife.healthylifecore.configuration.SwaggerConfiguration;
import ru.healthylife.healthylifecore.domain.doctor.DoctorProfile;
import ru.healthylife.healthylifecore.dto.doctor.DoctorProfileResponse;
import ru.healthylife.healthylifecore.mapper.DoctorMapper;
import ru.healthylife.healthylifecore.service.DoctorService;

import javax.annotation.Nonnull;
import java.security.Principal;

@Slf4j
@RestController
@AllArgsConstructor
@Tag(name = "Doctor", description = "The Doctor API")
@RequestMapping(path = DoctorController.DOCTORS, produces = MediaType.APPLICATION_JSON_VALUE)
public class DoctorController {
    public static final String DOCTORS = "/doctors";
    public static final String PROFILE = "/profile";

    private final DoctorMapper doctorMapper;
    private final DoctorService doctorService;

    @GetMapping(PROFILE)
    @SecurityRequirement(name = SwaggerConfiguration.HEALTHY_LIFE_OPEN_API_BEARER)
    public ResponseEntity<DoctorProfileResponse> getProfile(@Nonnull Principal principal) {
        DoctorProfile profileByEmail = doctorService.getProfileByEmail(principal.getName());
        return ResponseEntity.ok(doctorMapper.convert(profileByEmail));
    }
}
