package ru.healthylife.healthylifecore.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.healthylife.healthylifecore.dto.ehr.IcdDto;
import ru.healthylife.healthylifecore.mapper.IcdMapper;
import ru.healthylife.healthylifecore.service.IcdService;

import java.util.List;

@RestController
@AllArgsConstructor
@Tag(name = "Icd", description = "ICD(International Classification of Diseases) API")
@RequestMapping(path = IcdController.ICDS_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
public class IcdController {
    protected static final String ICDS_PATH = "/icds";

    private final IcdMapper icdMapper;
    private final IcdService icdService;

    @GetMapping
    public ResponseEntity<List<IcdDto>> getAllIcds() {
        return ResponseEntity.ok(icdMapper.convertToDto(icdService.findAllIcds()));
    }
}
