package ru.healthylife.healthylifecore.controller.error;

import lombok.AllArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.jwt.BadJwtException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import ru.healthylife.healthylifecore.dto.error.ErrorResponse;
import ru.healthylife.healthylifecore.exception.MedicalRecordFileUploadingException;
import ru.healthylife.healthylifecore.exception.NotFoundEntityException;
import ru.healthylife.healthylifecore.exception.OriginalFileNameException;
import ru.healthylife.healthylifecore.service.registration.exceptions.UserAlreadyExistException;

import javax.annotation.Nonnull;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@AllArgsConstructor
@RestControllerAdvice
public class GlobalControllerExceptionHandler {

    private final MessageSource messageSource;

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Map<String, String> handleValidationExceptions(@Nonnull MethodArgumentNotValidException ex) {
        Map<String, String> errors = new HashMap<>();
        ex.getBindingResult().getAllErrors().forEach(error -> {
            String fieldName = ((FieldError) error).getField();
            String errorMessage = error.getDefaultMessage();
            errors.put(fieldName, errorMessage);
        });
        return errors;
    }

    @ResponseStatus(value = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(UserAlreadyExistException.class)
    public ErrorResponse handleUserAlreadyExistExceptions(@Nonnull UserAlreadyExistException ex) {
        //TODO: localization
        return new ErrorResponse(ex.getMessage());
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(NotFoundEntityException.class)
    public ResponseEntity<ErrorResponse> handleNotFoundEntityExceptionExceptions(
            @Nonnull NotFoundEntityException ex,
            @Nonnull Locale locale
    ) {
        var message = messageSource.getMessage(ex.getMessageKey(), new Object[]{}, locale);
        return new ResponseEntity<>(new ErrorResponse(message), HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MedicalRecordFileUploadingException.class)
    public ResponseEntity<ErrorResponse> handleMedicalRecordFileUploadingExceptions(
            @Nonnull MedicalRecordFileUploadingException ex,
            @Nonnull Locale locale
    ) {
        var message = messageSource.getMessage(ex.getMessageKey(), new Object[]{}, locale);
        return new ResponseEntity<>(new ErrorResponse(message), HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(OriginalFileNameException.class)
    public ResponseEntity<ErrorResponse> handleOriginalFileNameExceptions(
            @Nonnull OriginalFileNameException ex,
            @Nonnull Locale locale
    ) {
        var message = messageSource.getMessage(ex.getMessageKey(), new Object[]{}, locale);
        return new ResponseEntity<>(new ErrorResponse(message), HttpStatus.BAD_REQUEST);
    }

    @ResponseStatus(code = HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BadJwtException.class)
    public ResponseEntity<ErrorResponse> handleBadJwtExceptions(@Nonnull BadJwtException ex) {
        return new ResponseEntity<>(new ErrorResponse(ex.getLocalizedMessage()), HttpStatus.BAD_REQUEST);
    }
}
