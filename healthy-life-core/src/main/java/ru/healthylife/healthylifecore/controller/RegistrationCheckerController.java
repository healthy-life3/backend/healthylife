package ru.healthylife.healthylifecore.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.healthylife.healthylifecore.dto.check.CheckResult;
import ru.healthylife.healthylifecore.dto.check.EmailDto;
import ru.healthylife.healthylifecore.dto.check.PhoneNumberCheckDto;
import ru.healthylife.healthylifecore.dto.check.SnilsDto;
import ru.healthylife.healthylifecore.mapper.CheckerMapper;
import ru.healthylife.healthylifecore.service.registration.RegistrationCheckerService;

@Slf4j
@RestController
@AllArgsConstructor
@Tag(name = "Registration checker controller", description = "Registration checker API")
@RequestMapping(path = RegistrationCheckerController.CHECK_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
public class RegistrationCheckerController {
    protected static final String CHECK_PATH = "/check";
    protected static final String EMAIL = "/email";
    protected static final String PHONE_NUMBER = "/phone-number";
    protected static final String SNILS = "/snils";

    private final CheckerMapper checkerMapper;
    private final RegistrationCheckerService registrationCheckerService;

    @GetMapping(EMAIL)
    public ResponseEntity<CheckResult> checkEmail(@RequestBody EmailDto email) {
        var result = registrationCheckerService.checkEmail(checkerMapper.convertToDomain(email));
        return ResponseEntity.ok(new CheckResult(result));
    }

    @GetMapping(PHONE_NUMBER)
    public ResponseEntity<CheckResult> checkPhoneNumber(@RequestBody PhoneNumberCheckDto phoneNumberCheck) {
        var result = registrationCheckerService.checkPhoneNumber(checkerMapper.convertToDomain(phoneNumberCheck));
        return ResponseEntity.ok(new CheckResult(result));
    }

    @GetMapping(SNILS)
    public ResponseEntity<CheckResult> checkSnils(@RequestBody SnilsDto snilsDto) {
        var result = registrationCheckerService.checkSnils(snilsDto.getSnils());
        return ResponseEntity.ok(new CheckResult(result));
    }
}
