package ru.healthylife.healthylifecore.controller;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import ru.healthylife.healthylifecore.configuration.SwaggerConfiguration;
import ru.healthylife.healthylifecore.dto.ipfs.responses.AddIpfsClusterResponse;
import ru.healthylife.healthylifecore.service.IpfsService;

/**
 * Experimental
 */
@RestController
@AllArgsConstructor
@Tag(name = "Medical files in ipfs", description = "Ipfs file's API")
@RequestMapping(path = IpfsFilesController.IPFS_FILES)
public class IpfsFilesController {
    protected static final String IPFS_FILES = "/medical-ipfs-files";
    protected static final String DOWNLOAD = "/download";
    protected static final String UPLOAD = "/upload";

    private final IpfsService ipfsService;


    @ApiResponse(responseCode = "400", description = "Medical record file wasn't found")
    @SecurityRequirement(name = SwaggerConfiguration.HEALTHY_LIFE_OPEN_API_BEARER)
    @GetMapping(value = DOWNLOAD + "/{cid}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Resource> downloadIpfsFile(@PathVariable String cid) {
        byte[] fileBytes = ipfsService.getFile(cid);
        return ResponseEntity.ok()
                .body(new ByteArrayResource(fileBytes));
    }

    @ApiResponse(responseCode = "400", description = "Failed while medical record file was uploading")
    @SecurityRequirement(name = SwaggerConfiguration.HEALTHY_LIFE_OPEN_API_BEARER)
    @PostMapping(value = UPLOAD, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<AddIpfsClusterResponse> uploadMedicalRecordFileInfo(
            @RequestPart("file") @NonNull MultipartFile multipartFile
    ) {
        return ResponseEntity.ok(ipfsService.addFile(multipartFile));
    }

}
