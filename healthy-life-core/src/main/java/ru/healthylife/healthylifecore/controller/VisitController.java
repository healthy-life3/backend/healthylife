package ru.healthylife.healthylifecore.controller;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.healthylife.healthylifecore.configuration.SwaggerConfiguration;
import ru.healthylife.healthylifecore.dto.ehr.NewVisitDto;
import ru.healthylife.healthylifecore.dto.ehr.VisitDto;
import ru.healthylife.healthylifecore.mapper.VisitMapper;
import ru.healthylife.healthylifecore.service.VisitService;

import javax.annotation.Nonnull;
import java.security.Principal;
import java.util.UUID;

@RestController
@AllArgsConstructor
@Tag(name = "Visit", description = "Patient's visits API")
@RequestMapping(path = VisitController.VISITS_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
public class VisitController {
    protected static final String VISITS_PATH = "/visits";

    private final VisitMapper visitMapper;
    private final VisitService visitService;

    @GetMapping("/{visitUuid}")
    @ApiResponse(responseCode = "400", description = "Visit record wasn't found")
    @SecurityRequirement(name = SwaggerConfiguration.HEALTHY_LIFE_OPEN_API_BEARER)
    public ResponseEntity<VisitDto> getVisit(
            @Parameter(description = "Visit uuid") @PathVariable UUID visitUuid
    ) {
        var visit = visitService.findVisitByUuid(visitUuid);
        return ResponseEntity.ok(visitMapper.convertToDto(visit));
    }

    @PostMapping
    @ApiResponse(responseCode = "200", description = "Visit was created successfully")
    @SecurityRequirement(name = SwaggerConfiguration.HEALTHY_LIFE_OPEN_API_BEARER)
    public void createVisit(@RequestBody NewVisitDto newVisitDto, @Nonnull Principal principal) {
        var newVisit = visitMapper.convertToDomain(newVisitDto, principal.getName());
        visitService.createNewMedicalRecord(newVisit);
    }
}
