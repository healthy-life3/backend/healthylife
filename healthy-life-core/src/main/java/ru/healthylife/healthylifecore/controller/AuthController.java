package ru.healthylife.healthylifecore.controller;

import com.nimbusds.jose.JOSEException;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.healthylife.healthylifecore.configuration.SwaggerConfiguration;
import ru.healthylife.healthylifecore.domain.auth.JwtTokenInfo;
import ru.healthylife.healthylifecore.domain.doctor.DoctorRegistrationInfo;
import ru.healthylife.healthylifecore.domain.patient.PatientRegistrationInfo;
import ru.healthylife.healthylifecore.domain.user.MedicalUserInfo;
import ru.healthylife.healthylifecore.dto.auth.JwtToken;
import ru.healthylife.healthylifecore.dto.doctor.DoctorRegistrationRequest;
import ru.healthylife.healthylifecore.dto.patient.PatientRegistrationRequest;
import ru.healthylife.healthylifecore.mapper.DoctorMapper;
import ru.healthylife.healthylifecore.mapper.PatientMapper;
import ru.healthylife.healthylifecore.mapper.TokenMapper;
import ru.healthylife.healthylifecore.service.TokenService;
import ru.healthylife.healthylifecore.service.registration.DoctorRegistrationService;
import ru.healthylife.healthylifecore.service.registration.PatientRegistrationService;

import javax.annotation.Nonnull;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Slf4j
@RestController
@AllArgsConstructor
@Tag(name = "Authentication controller", description = "The authentication API")
@RequestMapping(produces = MediaType.APPLICATION_JSON_VALUE)
public class AuthController {
    protected static final String SIGNUP_DOCTOR = "/signup/doctor";
    protected static final String SIGNUP_PATIENT = "/signup/patient";
    protected static final String SIGNIN = "/signin";
    protected static final String REFRESH_TOKEN = "/refresh-token";

    private final PatientMapper patientMapper;
    private final PatientRegistrationService patientRegistrationService;
    private final DoctorMapper doctorMapper;
    private final DoctorRegistrationService doctorRegistrationService;
    private final TokenMapper tokenMapper;
    private final TokenService tokenService;

    @PostMapping(SIGNIN)
    @SecurityRequirement(name = SwaggerConfiguration.HEALTHY_LIFE_OPEN_API_BASIC)
    public ResponseEntity<JwtToken> signin(@Nonnull Authentication authentication) throws JOSEException {
        JwtTokenInfo jwtToken = tokenService.getJwtTokens(authentication);
        return ResponseEntity.ok(tokenMapper.convertToDto(jwtToken));
    }

    @PostMapping(SIGNUP_DOCTOR)
    public ResponseEntity<JwtToken> signupNewDoctor(
            @RequestBody @Valid DoctorRegistrationRequest doctorRegistrationRequest
    ) throws JOSEException {
        DoctorRegistrationInfo doctorRegistrationInfo = doctorMapper.convert(doctorRegistrationRequest);
        MedicalUserInfo medicalUserInfo = doctorRegistrationService.registerNewMedicalUser(doctorRegistrationInfo);
        JwtTokenInfo jwtToken = tokenService.getJwtTokens(medicalUserInfo);
        return ResponseEntity.ok(tokenMapper.convertToDto(jwtToken));
    }

    @PostMapping(SIGNUP_PATIENT)
    public ResponseEntity<JwtToken> signupNewPatient(
            @RequestBody @Valid PatientRegistrationRequest patientRegistrationRequest
    ) throws JOSEException {
        PatientRegistrationInfo patientRegistrationInfo = patientMapper.convert(patientRegistrationRequest);
        MedicalUserInfo medicalUserInfo = patientRegistrationService.registerNewMedicalUser(patientRegistrationInfo);
        JwtTokenInfo jwtToken = tokenService.getJwtTokens(medicalUserInfo);
        return ResponseEntity.ok(tokenMapper.convertToDto(jwtToken));
    }

    @PostMapping(REFRESH_TOKEN)
    public ResponseEntity<JwtToken> refreshToken(
            @RequestBody @NotNull String refreshToken,
            @Nonnull Authentication authentication
    ) throws JOSEException {
        var newJwtToken = tokenService.refresh(authentication, refreshToken);
        return ResponseEntity.ok(tokenMapper.convertToDto(newJwtToken));
    }
}
