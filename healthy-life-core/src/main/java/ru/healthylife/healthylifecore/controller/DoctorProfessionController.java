package ru.healthylife.healthylifecore.controller;

import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ru.healthylife.healthylifecore.dto.doctor.DoctorProfessionDto;
import ru.healthylife.healthylifecore.mapper.DoctorProfessionMapper;
import ru.healthylife.healthylifecore.service.DoctorProfessionService;

import java.util.List;

@Slf4j
@RestController
@AllArgsConstructor
@Tag(name = "Doctor profession", description = "The Doctors profession's API")
@RequestMapping(path = DoctorProfessionController.DOCTOR_PROFESSION_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
public class DoctorProfessionController {
    protected static final String DOCTOR_PROFESSION_PATH = "/doctor-professions";

    private final DoctorProfessionMapper doctorProfessionMapper;
    private final DoctorProfessionService doctorProfessionService;

    @GetMapping
    public ResponseEntity<List<DoctorProfessionDto>> getAllProfessions() {
        return ResponseEntity.ok(doctorProfessionMapper.mapToDto(doctorProfessionService.findAllProfessions()));
    }
}
