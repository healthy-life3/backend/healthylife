package ru.healthylife.healthylifecore.controller;

import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.healthylife.healthylifecore.configuration.SwaggerConfiguration;
import ru.healthylife.healthylifecore.dto.ehr.LaboratoryTestDto;
import ru.healthylife.healthylifecore.dto.ehr.NewLaboratoryTestDto;
import ru.healthylife.healthylifecore.mapper.LaboratoryTestMapper;
import ru.healthylife.healthylifecore.service.LaboratoryTestService;

import javax.annotation.Nonnull;
import java.security.Principal;
import java.util.UUID;

@RestController
@AllArgsConstructor
@Tag(name = "Laboratory test", description = "Laboratory test's API")
@RequestMapping(path = LaboratoryTestController.LABORATORY_TESTS_PATH, produces = MediaType.APPLICATION_JSON_VALUE)
public class LaboratoryTestController {
    protected static final String LABORATORY_TESTS_PATH = "/laboratory-tests";

    private final LaboratoryTestMapper laboratoryTestMapper;
    private final LaboratoryTestService laboratoryTestService;

    @GetMapping("/{laboratoryTestUuid}")
    @ApiResponse(responseCode = "400", description = "Laboratory test wasn't found")
    @SecurityRequirement(name = SwaggerConfiguration.HEALTHY_LIFE_OPEN_API_BEARER)
    public ResponseEntity<LaboratoryTestDto> getLaboratoryTest(
            @Parameter(description = "Laboratory test uuid") @PathVariable UUID laboratoryTestUuid
    ) {
        var laboratoryTestByUuid = laboratoryTestService.getLaboratoryTestByUuid(laboratoryTestUuid);
        return ResponseEntity.ok(laboratoryTestMapper.convertToDto(laboratoryTestByUuid));
    }

    @PostMapping
    @ApiResponse(responseCode = "200", description = "Laboratory test was created successfully")
    @SecurityRequirement(name = SwaggerConfiguration.HEALTHY_LIFE_OPEN_API_BEARER)
    public void createLaboratoryTest(@RequestBody NewLaboratoryTestDto newLaboratoryTestDto, @Nonnull Principal principal) {
        var newLaboratoryTestInfo = laboratoryTestMapper.convertToDomain(newLaboratoryTestDto, principal.getName());
        laboratoryTestService.createNewMedicalRecord(newLaboratoryTestInfo);
    }
}
