package ru.healthylife.healthylifecore.controller;

import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import lombok.NonNull;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import ru.healthylife.healthylifecore.configuration.SwaggerConfiguration;
import ru.healthylife.healthylifecore.domain.ehr.FileInfo;
import ru.healthylife.healthylifecore.domain.ehr.MedicalRecordFileInfo;
import ru.healthylife.healthylifecore.dto.ehr.MedicalRecordFileDto;
import ru.healthylife.healthylifecore.dto.ehr.NewMedicalRecordFileDto;
import ru.healthylife.healthylifecore.mapper.MedicalRecordFileMapper;
import ru.healthylife.healthylifecore.service.MedicalRecordFileService;

import javax.annotation.Nonnull;
import javax.validation.Valid;
import java.security.Principal;
import java.text.MessageFormat;
import java.util.UUID;

@RestController
@AllArgsConstructor
@Tag(name = "Medical record file controller", description = "Medical record files API")
@RequestMapping(path = MedicalRecordFileController.MEDICAL_RECORD_FILES)
public class MedicalRecordFileController {
    public static final String MEDICAL_RECORD_FILES = "/medical-record-files";

    public static final String DOWNLOAD = "/download";
    public static final String UPLOAD = "/upload";

    private final MedicalRecordFileMapper medicalRecordFileMapper;
    private final MedicalRecordFileService medicalRecordFileService;

    @ApiResponse(responseCode = "400", description = "Medical record file wasn't found")
    @SecurityRequirement(name = SwaggerConfiguration.HEALTHY_LIFE_OPEN_API_BEARER)
    @GetMapping(value = "/{uuid}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<MedicalRecordFileDto> getMedicalRecordFileInfo(@PathVariable UUID uuid) {
        var medicalRecordFileInfo = medicalRecordFileService.getMedicalRecordFileInfo(uuid);
        return getMedicalRecordFileDto(medicalRecordFileInfo);
    }

    @ApiResponse(
            responseCode = "200",
            description = "Medical record file was found. The content type has the same value as it was loaded earlier(default=\"application/octet-stream\""
    )
    @ApiResponse(responseCode = "400", description = "Medical record file wasn't found")
    @SecurityRequirement(name = SwaggerConfiguration.HEALTHY_LIFE_OPEN_API_BEARER)
    @GetMapping(value = DOWNLOAD + "/{uuid}", produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
    public ResponseEntity<Resource> downloadMedicalRecordFile(@PathVariable UUID uuid) {
        var newMedicalRecordFileInfo = medicalRecordFileService.getMedicalRecordFileInfo(uuid);
        var file = newMedicalRecordFileInfo.getFile();
        return ResponseEntity.ok()
                .contentType(parseMediaType(file))
                .header(HttpHeaders.CONTENT_DISPOSITION, MessageFormat.format("attachment;filename=\"{0}\"", file.getName()))
                .body(new ByteArrayResource(file.getData()));
    }

    @Nonnull
    private MediaType parseMediaType(@Nonnull FileInfo file) {
        try {
            return MediaType.valueOf(file.getType());
        } catch (IllegalArgumentException exception) {
            return MediaType.APPLICATION_OCTET_STREAM;
        }
    }

    @ApiResponse(responseCode = "400", description = "Failed while medical record file was uploading")
    @SecurityRequirement(name = SwaggerConfiguration.HEALTHY_LIFE_OPEN_API_BEARER)
    @PostMapping(value = UPLOAD, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
    public ResponseEntity<MedicalRecordFileDto> uploadMedicalRecordFileInfo(
            @RequestPart("medicalRecord") @Valid NewMedicalRecordFileDto newMedicalRecordFileDto,
            @RequestPart("file") @NonNull MultipartFile multipartFile,
            @Nonnull Principal principal
    ) {
        var newMedicalRecordFile = medicalRecordFileMapper.convertToDomain(
                newMedicalRecordFileDto,
                principal.getName(),
                multipartFile
        );
        var medicalRecordFileInfo = medicalRecordFileService.createNewMedicalRecord(newMedicalRecordFile);
        return getMedicalRecordFileDto(medicalRecordFileInfo);
    }

    @Nonnull
    private ResponseEntity<MedicalRecordFileDto> getMedicalRecordFileDto(@Nonnull MedicalRecordFileInfo medicalRecordFileInfo) {
        var medicalRecordFileDto = medicalRecordFileMapper.convertToDto(medicalRecordFileInfo);
        var uriToFile = ServletUriComponentsBuilder.fromCurrentContextPath()
                .path(DOWNLOAD)
                .path("/" + medicalRecordFileDto.getMedicalRecordUuid())
                .toUriString();
        medicalRecordFileDto.getFileMetaData().setPath(uriToFile);
        return ResponseEntity.ok(medicalRecordFileDto);
    }
}
