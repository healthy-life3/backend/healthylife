package ru.healthylife.healthylifecore.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.validation.annotation.Validated;

@Data
@Validated
@Configuration
@ConfigurationProperties(prefix = "openapi")
public class SwaggerConfig {
    private String title;
    private String version;
    private String description;
}
