package ru.healthylife.healthylifecore.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.jose.jws.SignatureAlgorithm;
import org.springframework.validation.annotation.Validated;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.time.Duration;

@Data
@Validated
@Configuration
@ConfigurationProperties(prefix = "jwt.config")
public class JwtTokensConfig {
    @NotBlank
    private String issuer;
    @Valid
    private TokenConfig refreshToken;
    @Valid
    private TokenConfig accessToken;

    @Data
    public static class TokenConfig {
        @NotNull
        private SignatureAlgorithm algorithm;
        @NotNull
        private RSAPublicKey publicKey;
        @NotNull
        private RSAPrivateKey privateKey;
        @NotNull
        private Duration sessionTime = Duration.ofMillis(60_000);
    }
}
