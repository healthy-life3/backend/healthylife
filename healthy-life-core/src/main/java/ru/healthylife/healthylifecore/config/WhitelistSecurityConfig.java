package ru.healthylife.healthylifecore.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

@Data
@Configuration
@ConfigurationProperties(prefix = "security")
public class WhitelistSecurityConfig {
    private List<String> webWhitelist = new ArrayList<>();
    private List<String> httpWhitelist = new ArrayList<>();
}
