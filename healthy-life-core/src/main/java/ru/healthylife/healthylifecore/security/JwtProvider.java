package ru.healthylife.healthylifecore.security;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jose.JWSAlgorithm;
import com.nimbusds.jose.JWSHeader;
import com.nimbusds.jose.JWSSigner;
import com.nimbusds.jose.crypto.RSASSASigner;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import ru.healthylife.healthylifecore.config.JwtTokensConfig;

import javax.annotation.Nonnull;
import java.time.Duration;
import java.time.Instant;
import java.util.Date;
import java.util.Map;

@Service
@AllArgsConstructor
public class JwtProvider {
    private final JwtTokensConfig jwtTokensConfig;

    @Nonnull
    public String generateAccessToken(@Nonnull String subject, @Nonnull Map<String, String> claims) throws JOSEException {
        return generateToken(subject, claims, jwtTokensConfig.getAccessToken());
    }

    @Nonnull
    public String generateRefreshToken(@Nonnull String subject, @Nonnull Map<String, String> claims) throws JOSEException {
        return generateToken(subject, claims, jwtTokensConfig.getRefreshToken());
    }

    @Nonnull
    private String generateToken(
            @Nonnull String subject,
            @Nonnull Map<String, String> claims,
            @Nonnull JwtTokensConfig.TokenConfig accessTokenConfig
    ) throws JOSEException {
        var jwsHeader = new JWSHeader(JWSAlgorithm.parse(accessTokenConfig.getAlgorithm().name()));
        JWSSigner signer = new RSASSASigner(accessTokenConfig.getPrivateKey());

        var signedJWT = new SignedJWT(jwsHeader, generateJwtClaimsSet(subject, claims, accessTokenConfig.getSessionTime()));
        signedJWT.sign(signer);

        return signedJWT.serialize();
    }

    @Nonnull
    private JWTClaimsSet generateJwtClaimsSet(
            @Nonnull String subject,
            @Nonnull Map<String, String> claims,
            @Nonnull Duration sessionTime
    ) {
        JWTClaimsSet.Builder jwtBuilder = new JWTClaimsSet.Builder()
                .subject(subject)
                .issuer(jwtTokensConfig.getIssuer())
                .issueTime(Date.from(Instant.now()))
                .expirationTime(expireTimeFromNow(sessionTime));
        claims.forEach(jwtBuilder::claim);
        return jwtBuilder.build();
    }

    @Nonnull
    private Date expireTimeFromNow(@Nonnull Duration expireDuration) {
        return Date.from(Instant.now().plusNanos(expireDuration.toNanos()));
    }
}
