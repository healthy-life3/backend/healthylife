package ru.healthylife.healthylifecore.security;

import lombok.AllArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.configurers.AbstractHttpConfigurer;
import org.springframework.security.config.annotation.web.configurers.oauth2.server.resource.OAuth2ResourceServerConfigurer;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.jwt.JwtDecoder;
import org.springframework.security.oauth2.jwt.NimbusJwtDecoder;
import ru.healthylife.healthylifecore.config.JwtTokensConfig;
import ru.healthylife.healthylifecore.config.WhitelistSecurityConfig;

import javax.annotation.Nonnull;

@EnableWebSecurity
@AllArgsConstructor
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    private final WhitelistSecurityConfig whitelistSecurityConfig;

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers(whitelistSecurityConfig.getWebWhitelist().toArray(new String[0]));
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeHttpRequests(authorize ->
                        authorize
                                .antMatchers(whitelistSecurityConfig.getHttpWhitelist().toArray(new String[0]))
                                .permitAll()
                                .anyRequest().authenticated()
                )
                .csrf(AbstractHttpConfigurer::disable)
                .httpBasic(Customizer.withDefaults())
                .oauth2ResourceServer(OAuth2ResourceServerConfigurer::jwt)
                .sessionManagement(session -> session.sessionCreationPolicy(SessionCreationPolicy.STATELESS));
    }

    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    @Primary
    public JwtDecoder accessJwtDecoder(@Nonnull JwtTokensConfig jwtTokensConfig) {
        var accessTokenConfig = jwtTokensConfig.getAccessToken();
        return NimbusJwtDecoder
                .withPublicKey(accessTokenConfig.getPublicKey())
                .signatureAlgorithm(accessTokenConfig.getAlgorithm())
                .build();
    }

    @Bean
    public JwtDecoder refreshJwtDecoder(@Nonnull JwtTokensConfig jwtTokensConfig) {
        var refreshTokenConfig = jwtTokensConfig.getRefreshToken();
        return NimbusJwtDecoder
                .withPublicKey(refreshTokenConfig.getPublicKey())
                .signatureAlgorithm(refreshTokenConfig.getAlgorithm())
                .build();
    }
}
