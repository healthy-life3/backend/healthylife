package ru.healthylife.healthylifecore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.healthylife.healthylifecore.entity.user.Role;
import ru.healthylife.healthylifecore.entity.user.RoleName;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Set;

@Repository
public interface RoleRepository extends JpaRepository<Role, Long> {

    @Nonnull
    Role getRoleByName(RoleName name);

    @Nonnull
    Set<Role> findByNameIn(Collection<RoleName> name);
}
