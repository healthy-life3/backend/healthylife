package ru.healthylife.healthylifecore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.healthylife.healthylifecore.entity.patient.Patient;

import javax.annotation.Nonnull;

@Repository
public interface PatientRepository extends JpaRepository<Patient, Long> {

    @Nonnull
    Patient getByMedicalUserEmail(@Nonnull String email);

    boolean existsBySnils(@Nonnull String snils);
}
