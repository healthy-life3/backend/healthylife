package ru.healthylife.healthylifecore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecordFile;

import java.util.UUID;

@Repository
public interface MedicalRecordFileRepository extends JpaRepository<MedicalRecordFile, UUID> {
}
