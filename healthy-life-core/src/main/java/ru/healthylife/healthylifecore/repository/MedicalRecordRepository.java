package ru.healthylife.healthylifecore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.healthylife.healthylifecore.entity.ehr.MedicalRecord;

import javax.annotation.Nonnull;
import java.util.List;
import java.util.UUID;

@Repository
public interface MedicalRecordRepository extends JpaRepository<MedicalRecord, UUID> {

    @Nonnull
    List<MedicalRecord> findAllByPatientMedicalUserEmailOrderByDateTimeDesc(@Nonnull String email);
}
