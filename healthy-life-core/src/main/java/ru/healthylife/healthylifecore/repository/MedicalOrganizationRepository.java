package ru.healthylife.healthylifecore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.healthylife.healthylifecore.entity.ehr.MedicalOrganization;

@Repository
public interface MedicalOrganizationRepository extends JpaRepository<MedicalOrganization, Long> {

}