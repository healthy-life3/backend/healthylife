package ru.healthylife.healthylifecore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.healthylife.healthylifecore.entity.user.MedicalUser;

import javax.annotation.Nonnull;
import java.util.Optional;

@Repository
public interface MedicalUserRepository extends JpaRepository<MedicalUser, Long> {
    @Nonnull
    Optional<MedicalUser> findByEmail(String email);

    @Nonnull
    MedicalUser getByEmail(String email);

    boolean existsByEmail(String email);
}
