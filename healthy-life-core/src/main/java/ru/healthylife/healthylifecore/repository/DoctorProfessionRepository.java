package ru.healthylife.healthylifecore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.healthylife.healthylifecore.entity.doctor.DoctorProfession;

@Repository
public interface DoctorProfessionRepository extends JpaRepository<DoctorProfession, Long> {
}
