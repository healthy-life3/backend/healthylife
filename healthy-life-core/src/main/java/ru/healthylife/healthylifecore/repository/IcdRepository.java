package ru.healthylife.healthylifecore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.healthylife.healthylifecore.entity.ehr.Icd;

@Repository
public interface IcdRepository extends JpaRepository<Icd, String> {

}
