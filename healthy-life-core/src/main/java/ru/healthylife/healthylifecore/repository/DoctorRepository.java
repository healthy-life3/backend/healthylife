package ru.healthylife.healthylifecore.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.healthylife.healthylifecore.entity.doctor.Doctor;

import javax.annotation.Nonnull;

@Repository
public interface DoctorRepository extends JpaRepository<Doctor, Long> {

    Doctor getByMedicalUserEmail(@Nonnull String email);
}
