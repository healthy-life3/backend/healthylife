package ru.healthylife.healthylifecore.exception;

public class MedicalRecordFileUploadingException extends LocalizedException {

    public MedicalRecordFileUploadingException(String message) {
        super(message);
    }

    public MedicalRecordFileUploadingException(String message, Throwable cause) {
        super(message, cause);
    }

    public MedicalRecordFileUploadingException(Throwable cause) {
        super(cause);
    }
}
