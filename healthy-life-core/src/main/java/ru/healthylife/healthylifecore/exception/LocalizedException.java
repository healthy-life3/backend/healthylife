package ru.healthylife.healthylifecore.exception;

import lombok.Getter;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Locale;
import java.util.ResourceBundle;

@Getter
public class LocalizedException extends RuntimeException {
    private final String messageKey;
    private final Locale locale;

    public LocalizedException(@Nonnull String messageKey) {
        this(messageKey, Locale.getDefault());
    }

    public LocalizedException(@Nonnull String messageKey, @Nonnull Locale locale) {
        this.messageKey = messageKey;
        this.locale = locale;
    }

    public LocalizedException(@Nonnull String messageKey, @Nullable Throwable cause) {
        super(messageKey, cause);
        this.messageKey = messageKey;
        this.locale = Locale.getDefault();
    }

    public LocalizedException(@Nullable Throwable cause) {
        super(cause);
        this.messageKey = "error.default";
        this.locale = Locale.getDefault();
    }

    @Override
    public String getLocalizedMessage() {
        return ResourceBundle.getBundle("messages", locale).getString(messageKey);
    }
}
