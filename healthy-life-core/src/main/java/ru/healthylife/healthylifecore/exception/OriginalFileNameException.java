package ru.healthylife.healthylifecore.exception;

import lombok.NonNull;

import javax.annotation.Nullable;
import java.util.Locale;

public class OriginalFileNameException extends LocalizedException {

    public OriginalFileNameException(@NonNull String messageKey) {
        super(messageKey);
    }

    public OriginalFileNameException(@NonNull String messageKey, @NonNull Locale locale) {
        super(messageKey, locale);
    }

    public OriginalFileNameException(@NonNull String messageKey, @Nullable Throwable cause) {
        super(messageKey, cause);
    }

    public OriginalFileNameException(@Nullable Throwable cause) {
        super(cause);
    }
}
