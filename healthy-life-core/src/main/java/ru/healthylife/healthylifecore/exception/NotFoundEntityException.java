package ru.healthylife.healthylifecore.exception;

import java.util.Locale;

public class NotFoundEntityException extends LocalizedException {

    public NotFoundEntityException(String messageKey) {
        super(messageKey);
    }

    public NotFoundEntityException(String messageKey, Locale locale) {
        super(messageKey, locale);
    }
}
