package ru.healthylife.healthylifecore.configuration;

import feign.form.spring.SpringFormEncoder;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.cloud.openfeign.support.SpringEncoder;
import org.springframework.context.annotation.Bean;

import javax.annotation.Nonnull;

public class FeignMultipartSupportConfiguration {
    @Bean
    public SpringFormEncoder feignEncoder(@Nonnull ObjectFactory<HttpMessageConverters> messageConverters) {
        return new SpringFormEncoder(new SpringEncoder(messageConverters));
    }
}
