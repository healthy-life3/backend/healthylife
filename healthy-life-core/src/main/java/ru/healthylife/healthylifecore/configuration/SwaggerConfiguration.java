package ru.healthylife.healthylifecore.configuration;


import io.swagger.v3.oas.annotations.enums.SecuritySchemeIn;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.PathItem;
import io.swagger.v3.oas.models.info.Info;
import io.swagger.v3.oas.models.media.Content;
import io.swagger.v3.oas.models.responses.ApiResponse;
import io.swagger.v3.oas.models.responses.ApiResponses;
import org.springdoc.core.customizers.OpenApiCustomiser;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import ru.healthylife.healthylifecore.config.SwaggerConfig;
import ru.healthylife.healthylifecore.config.WhitelistSecurityConfig;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Consumer;
import java.util.function.Predicate;

@Configuration
@SecurityScheme(
        name = SwaggerConfiguration.HEALTHY_LIFE_OPEN_API_BASIC,
        scheme = "basic",
        type = SecuritySchemeType.HTTP,
        in = SecuritySchemeIn.HEADER
)
@SecurityScheme(
        name = SwaggerConfiguration.HEALTHY_LIFE_OPEN_API_BEARER,
        scheme = "bearer",
        type = SecuritySchemeType.HTTP,
        in = SecuritySchemeIn.HEADER
)
public class SwaggerConfiguration {
    public static final String HEALTHY_LIFE_OPEN_API_BASIC = "Basic scheme authentication";
    public static final String HEALTHY_LIFE_OPEN_API_BEARER = "Bearer scheme authentication";

    @Bean
    public OpenAPI openApi(@Nonnull SwaggerConfig swaggerConfig) {
        return new OpenAPI().info(new Info()
                .title(swaggerConfig.getTitle())
                .version(swaggerConfig.getVersion())
                .description(swaggerConfig.getDescription())
        );
    }

    @Bean
    public OpenApiCustomiser responseRegistrationCustomizer(@Nonnull WhitelistSecurityConfig whitelistSecurityConfig) {
        List<String> whitelist = whitelistSecurityConfig.getHttpWhitelist();
        return openApi -> {
            addApiResponses(
                    openApi,
                    path -> !whitelist.contains(path),
                    apiResponses -> {
                        apiResponses.addApiResponse("401", createApiResponse("Unauthorized"));
                        apiResponses.addApiResponse("403", createApiResponse("Forbidden"));
                    }
            );
            addApiResponses(
                    openApi,
                    paths -> true,
                    apiResponses -> {
                        apiResponses.addApiResponse("404", createApiResponse("Not Found"));
                        apiResponses.addApiResponse("500", createApiResponse("Server Error"));
                    }
            );
        };
    }

    private void addApiResponses(
            @Nonnull OpenAPI openApi,
            @Nonnull Predicate<String> pathsPredicate,
            @Nonnull Consumer<ApiResponses> apiResponsesConsumer
    ) {
        openApi.getPaths()
                .entrySet()
                .stream()
                .filter(pathToPathItem -> pathsPredicate.test(pathToPathItem.getKey()))
                .map(Map.Entry::getValue)
                .map(PathItem::readOperations)
                .flatMap(Collection::stream)
                .forEach(operation -> {
                    ApiResponses apiResponses = operation.getResponses();
                    apiResponsesConsumer.accept(apiResponses);
                });
    }

    private ApiResponse createApiResponse(@Nonnull String message) {
        var content = new Content();
        content.addMediaType(MediaType.APPLICATION_JSON_VALUE, null);
        return new ApiResponse()
                .description(message)
                .content(content);
    }
}
