package ru.healthylife.healthylifecore.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.Clock;
import java.time.LocalDateTime;

@Configuration
public class ClockConfiguration {

    /**
     * For test-friendly
     * @see LocalDateTime#now()
     * */
    @Bean
    public Clock clock() {
        return Clock.systemDefaultZone();
    }
}
