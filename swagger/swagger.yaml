openapi: 3.0.3
info:
  description: |
    Описание протокола взаимодействия с серверной частью электронных медицинских карт на базе технологии блокчейн.
  version: "1.0.0"
  title: HealthyLife Application API
  contact:
    email: d.navruzshoev@g.nsu.ru
servers:
  - description: Сервер для взаимодействия с протоколом.
    url: http://34.134.88.69
tags:
  - name: Doctor
    description: Доступ к операциям, к которым имеет доктор
  - name: Authentication
    description: Операции, связанные с аутентификацией и регистрацией пользователей в системе
  - name: Registration checker
    description: Методы,  позволяющие проверить на валидность и уникальность входных данных при регистрации пользователей
  - name: Icd
    description: Методы, получающие информацию о международной классификации болезней
  - name: Medical organization
    description: Методы, получающие информацию о медицинских организациях
  - name: Laboratory test
    description: Операции, работающие с лабараторными исследованиями пациентов
  - name: Patient
    description: Доступ к операциям, к которым имеет пациент
  - name: Visit
    description: Доступ к приемам, которые были осуществены пациентом
  - name: Medical record file controller
    description: Доступ к медицинским документам, к которым имеет пациент
  - name: Doctor profession
    description: Методы, получающие информацию о существующих профессией
paths:
  "/visits":
    post:
      tags:
        - Visit
      operationId: createVisit
      summary: Создание нового приема пациента
      description: На вход передаются данные, связанные с новым визитом к доктору
      security:
        - Basic: [ ]
        - Bearer: [ ]
      requestBody:
        content:
          application/json:
            schema:
              "$ref": "#/components/schemas/NewVisit"
        required: true

      responses:
        '200':
          description: Прием был создан успешно
        '400':
          description: Неверные данные были переданы на вход
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/ErrorResponse"
        '401':
          description: Пользователь не авторизован
        '403':
          description: Нет доступа до операции
        '404':
          description: Прием не был найден
  "/signup/patient":
    post:
      tags:
        - Authentication
      operationId: signupNewPatient
      summary: Регистрация нового пациента в системе
      description: На вход передаются данные, связанные с новым пациентом в системе
      requestBody:
        content:
          application/json:
            schema:
              "$ref": "#/components/schemas/PatientRegistration"
        required: true
      responses:
        '200':
          description: Пациент зарегистрирован успешно
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/JwtToken"
        '400':
          description: Неверные данные были переданы на вход
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/ErrorResponse"
  "/signup/doctor":
    post:
      tags:
        - Authentication
      operationId: signupNewDoctor
      summary: Регистрация нового доктора в системе
      description: На вход передаются данные, связанные с новым доктором в системе
      requestBody:
        content:
          application/json:
            schema:
              "$ref": "#/components/schemas/DoctorRegistration"
        required: true
      responses:
        '200':
          description: Доктор зарегистрирован успешно
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/JwtToken"
        '400':
          description: Неверные данные были переданы на вход
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/ErrorResponse"
  "/signin":
    post:
      tags:
        - Authentication
      operationId: signin
      summary: Вход в систему
      description: Метод аутентифицирует пользователей в системе
      security:
        - Basic: [ ]
      responses:
        '200':
          description: Аутентификация прошла успешно
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/JwtToken"
        '401':
          description: Пользователь ввел некорректные данные для аутентификации в системе
  "/refresh-token":
    post:
      tags:
        - Authentication
      operationId: refreshToken
      summary: Получение нового JWT-токена
      description: Метод получает новый JWT-токен
      security:
        - Bearer: [ ]
      requestBody:
        content:
          application/json:
            schema:
              type: string
              example: 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1c2VySWQiOiJiMDhmODZhZi0zNWRhLTQ4ZjItOGZhYi1jZWYzOTA0NjYwYmQifQ.-xN_h82PHVTCMA9vdoHrcZxH-x5mb11y1537t3rGzcM'
        required: true
      responses:
        '200':
          description: Получение нового JWT-токена прошло успешно
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/JwtToken"
        '401':
          description: Пользователь не авторизован
  "/medical-record-files/upload":
    post:
      tags:
        - Medical record file controller
      operationId: uploadMedicalRecordFileInfo
      summary: Загрузка медицинского документа на сервер
      description: Метод загружает новый медицинский документ на сервер
      security:
        - Bearer: [ ]
        - Basic: [ ]
      requestBody:
        content:
          multipart/form-data:
            schema:
              required:
                - file
                - medicalRecord
                - email
                - title
              type: object
              properties:
                email:
                  type: string
                  format: email
                  example: ivanov@gmail.com
                  description: Адрес электронной почты
                title:
                  type: string
                  description: Название лабораторного исследования
                  example: Анализ крови
                file:
                  type: string
                  format: binary
                  description: Файл
      responses:
        '200':
          description: Загрузка нового медицинского документа прошла успешно
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/MedicalRecordFile"
        '400':
          description: Загрузка нового медицинского документа провалилась, либо неверные данные были переданы на вход
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/ErrorResponse"
        '401':
          description: Пользователь не авторизован
  "/laboratory-tests":
    post:
      tags:
        - Laboratory test
      operationId: createLaboratoryTest
      summary: Создание нового лабораторного анализа
      description: Метод создает новый лабораторный анализ
      security:
        - Bearer: [ ]
        - Basic: [ ]
      requestBody:
        content:
          application/json:
            schema:
              "$ref": "#/components/schemas/NewLaboratoryTest"
        required: true
      responses:
        '200':
          description: Лабораторный анализ был создан успешно
        '400':
          description: Неверные данные были переданы на вход
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/ErrorResponse"
        '401':
          description: Пользователь не авторизован
  "/visits/{visitUuid}":
    get:
      tags:
        - Visit
      operationId: getVisit
      summary: Получение лабораторного анализа
      description: Метод получает конкретный лабораторный анализ
      security:
        - Bearer: [ ]
        - Basic: [ ]
      parameters:
        - name: visitUuid
          in: path
          description: Visit uuid
          required: true
          schema:
            description: Уникальный индентификатор медицинской записи
            format: uuid
            example: 123e4567-e89b-42d3-a456-556642440000
      responses:
        '200':
          description: Лабораторный анализ был успешно найден
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/Visit"
        '400':
          description: Лабораторный анализ не был найден
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/ErrorResponse"
        '401':
          description: Пользователь не авторизован
  "/patients/profile":
    get:
      tags:
        - Patient
      operationId: getProfile
      summary: Получение информации о профиле пациента
      description: Метод получает информацию о профиле пациента
      security:
        - Bearer: [ ]
        - Basic: [ ]
      responses:
        '200':
          description: Инормация о профиле пациента была получена успешна
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/PatientProfile"
        '401':
          description: Пользователь не авторизован
  "/patients/medical-records":
    get:
      tags:
        - Patient
      operationId: getAllMedicalData
      summary: Получение информации о медицинских записей пациента
      description: Метод получает информации о медицинских записей пациента
      security:
        - Bearer: [ ]
        - Basic: [ ]
      responses:
        '200':
          description: Медицинские данные были получены успешно
          content:
            application/json:
              schema:
                type: array
                items:
                  "$ref": "#/components/schemas/MedicalRecord"
        '401':
          description: Пользователь не авторизован
  "/medical-record-files/{uuid}":
    get:
      tags:
        - Medical record file controller
      operationId: getMedicalRecordFileInfo
      summary: Получение информации о медицинской записи пациента
      description: Метод получает информации о конкретной медицинской записи пациента
      security:
        - Bearer: [ ]
        - Basic: [ ]
      parameters:
        - name: uuid
          in: path
          required: true
          schema:
            type: string
            description: Уникальный индентификатор медицинской записи
            format: uuid
            example: 123e4567-e89b-42d3-a456-556642440000
      responses:
        '200':
          description: Медицинская запись пациента была получена успешно
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/MedicalRecordFile"
        '400':
          description: Медицинская запись пациента не была найдена
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/ErrorResponse"
        '401':
          description: Пользователь не авторизован
  "/medical-record-files/download/{uuid}":
    get:
      tags:
        - Medical record file controller
      operationId: downloadMedicalRecordFileInfo
      summary: Загрузка медицинского документа
      description: Метод получает медицинский документ пациента
      security:
        - Bearer: [ ]
        - Basic: [ ]
      parameters:
        - name: uuid
          in: path
          required: true
          schema:
            type: string
            description: Уникальный индентификатор медицинского документа
            format: uuid
            example: 123e4567-e89b-42d3-a456-556642440000
      responses:
        '200':
          description: Медицинский файл был загружен успешно
          content:
            application/octet-stream:
              schema:
                type: string
                format: binary
        '400':
          description: Медицинский файл не был найден
          content:
            application/octet-stream:
              schema:
                "$ref": "#/components/schemas/ErrorResponse"
        '401':
          description: Пользователь не авторизован
  "/medical-organizations":
    get:
      tags:
        - Medical organization
      operationId: getAllMedicalOrganizations
      summary: Получение списка медицинских организаций
      description: Метод получает список медицинских организаций
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  "$ref": "#/components/schemas/MedicalOrganization"
  "/laboratory-tests/{laboratoryTestUuid}":
    get:
      tags:
        - Laboratory test
      operationId: getLaboratoryTest
      summary: Получение информации о лабараторном анализе
      description: Метод получает информацию о конкретном лабараторном анализе пациента
      security:
        - Bearer: [ ]
        - Basic: [ ]
      parameters:
        - name: laboratoryTestUuid
          in: path
          description: Уникальный индентификатор медицинской записи
          required: true
          schema:
            type: string
            description: Уникальный индентификатор медицинской записи
            format: uuid
            example: 123e4567-e89b-42d3-a456-556642440000
      responses:
        '200':
          description: Лабораторный анализ был найден успешно
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/LaboratoryTest"
        '400':
          description: Лабораторный анализ не был найден
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/ErrorResponse"
        '401':
          description: Пользователь не авторизован
  "/icds":
    get:
      tags:
        - Icd
      operationId: getAllIcds
      summary: Получение списка МКБ
      description: Метод получает список международных кодов болезней
      responses:
        '200':
          description: Получение списка прошло успешно
          content:
            application/json:
              schema:
                type: array
                items:
                  "$ref": "#/components/schemas/Icd"
  "/doctors/profile":
    get:
      tags:
        - Doctor
      operationId: getDoctorProfile
      summary: Получение информации о профиле доктора
      description: Метод получает информацию о профиле доктора
      security:
        - Bearer: [ ]
        - Basic: [ ]
      responses:
        '200':
          description: Информация о профиле получена успешно
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/DoctorProfile"
        '401':
          description: Пользователь не авторизован
  "/doctor-professions":
    get:
      tags:
        - Doctor profession
      operationId: getAllProfessions
      summary: Получение списка профессией доктора
      description: Метод получает список профессией доктора
      responses:
        '200':
          description: OK
          content:
            application/json:
              schema:
                type: array
                items:
                  "$ref": "#/components/schemas/DoctorProfession"
  "/check/snils":
    get:
      tags:
        - Registration checker controller
      operationId: checkSnils
      summary: Проверка на уникальность СНИЛСА
      description: Метод проверяет существует ли пользователь с приведенным СНИЛСом
      parameters:
        - name: snilsDto
          in: query
          required: true
          schema:
            "$ref": "#/components/schemas/Snils"
      responses:
        '200':
          description: Запрос проверен успешно
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/CheckResult"
  "/check/phone-number":
    get:
      tags:
        - Registration checker controller
      operationId: checkPhoneNumber
      summary: Проверка на уникальность номера телефона
      description: Метод проверяет существует ли пользователь с приведенным номера телефона
      parameters:
        - name: phoneNumberCheck
          in: query
          required: true
          schema:
            "$ref": "#/components/schemas/FullPhoneNumber"
      responses:
        '200':
          description: Запрос проверен успешно
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/CheckResult"
  "/check/email":
    get:
      tags:
        - Registration checker controller
      operationId: checkEmail
      summary: Проверка на уникальность адрес электронной почты
      description: Метод проверяет существует ли пользователь с приведенным адрес электронной почты
      parameters:
        - name: email
          in: query
          required: true
          schema:
            "$ref": "#/components/schemas/Email"
      responses:
        '200':
          description: Запрос проверен успешно
          content:
            application/json:
              schema:
                "$ref": "#/components/schemas/CheckResult"
components:
  schemas:
    ErrorResponse:
      description: Описание ошибки
      type: object
      properties:
        error:
          type: string
          description: Текст ошибки
          example: "Поле email не должно быть пустым"
    Icd:
      description: Коды международной классифиакции болезней
      type: object
      properties:
        code:
          type: string
          description: Код из МКБ
          example: G44.311
        description:
          type: string
          description: Описание кода
          example: Острая посттравматическая головная боль, некупируемая
    NewVisit:
      description: Информация для создания нового приема
      required:
        - email
        - complaints
        - diagnosis
        - icds
        - prescriptions
        - title
        - treatmentPlan
      type: object
      properties:
        email:
          type: string
          format: email
          example: ivanov@gmail.com
          description: Адрес электронной почты пациента
        title:
          type: string
          description: Названия приема
          example: Консультация врача терапевта
        icds:
          type: array
          description: Коды международной классификации болезней
          items:
            "$ref": "#/components/schemas/Icd"
        complaints:
          type: string
          description: Жалобы
          example: Сильный кашель и лихорадка
        diagnosis:
          type: string
          description: Диагноз
          example: Двусторонняя пневмония
        treatmentPlan:
          type: string
          description: План лечения
          example: Оставайтесь дома и принимать таблетки
        prescriptions:
          type: string
          description: Рецепты
          example: 'Антибиотик: Zithromax'
    PatientRegistration:
      description: Информация для регистрации нового пациента в системе
      required:
        - birthday
        - email
        - firstName
        - gender
        - lastName
        - password
        - phoneNumber
        - snils
      type: object
      properties:
        lastName:
          type: string
          description: Фамилия пациента
          example: Иванов
        firstName:
          type: string
          description: Имя пациента
          example: Иван
        middleName:
          type: string
          description: Отчество пациента
          example: Иванович
        phoneNumber:
          "$ref": "#/components/schemas/PhoneNumber"
        birthday:
          type: string
          format: date
          description: Дата рождения пациента
          example: "2000-01-30"
        password:
          type: string
          description: Пароль пациента
          example: password
        email:
          type: string
          format: email
          description: Адрес электронной почты пациента
          example: ivanov@gmail.com
        gender:
          type: string
          enum:
            - UNKNOWN
            - MALE
            - FEMALE
          description: Пол пациента
          example: MALE
        snils:
          maxLength: 11
          minLength: 11
          type: string
          description: СНИЛС пациента
          example: 12345678901
    PhoneNumber:
      description: Номер телефона. Формат – E.164
      required:
        - countryCode
        - fullNumber
        - subscriberNumber
      type: object
      properties:
        fullNumber:
          maxLength: 15
          minLength: 10
          type: string
          description: Полный номер телефона
          example: 791391367890
        countryCode:
          maxLength: 3
          minLength: 1
          type: string
          description: Код страны
          example: 7
        subscriberNumber:
          maxLength: 12
          minLength: 9
          type: string
          example: 91391367890
          description: Абонентский номер
    JwtToken:
      type: object
      description: Содержит токены доступа и индентификации пользователей в системе
      properties:
        accessToken:
          type: string
          description: Токен доступа, предназанченный для индентификации пользователей в системе, а также для получения информации о привлелегиях пользователя(JWT-токен)
          example: xxxxx.yyyyy.zzzzz
        refreshToken:
          type: string
          description: Токен обновления. Он позволяет обновлять недолговечные токены доступа(JWT/access-токены)
            без необходимости собирать учетные данные(email и пароль) каждый раз
          example: xxxxx.yyyyy.zzzzz
    DoctorRegistration:
      description: Информация для регистрации нового доктора в системе
      required:
        - birthday
        - doctorProfessionId
        - email
        - firstName
        - gender
        - lastName
        - medicalOrganizationId
        - password
        - phoneNumber
      type: object
      properties:
        lastName:
          type: string
          description: Фамилия доктора
          example: Иванов
        firstName:
          type: string
          description: Имя доктора
          example: Иван
        middleName:
          type: string
          description: Отчество доктора
          example: Иванович
        phoneNumber:
          "$ref": "#/components/schemas/PhoneNumber"
        birthday:
          type: string
          format: date
          description: Дата рождения доктора
          example: "2000-01-30"
        password:
          type: string
          description: Пароль доктора
          example: password
        email:
          type: string
          format: email
          description: Адрес электронной почты доктора
          example: ivanov@gmail.com
        gender:
          type: string
          enum:
            - UNKNOWN
            - MALE
            - FEMALE
          description: Пол доктора
          example: MALE
        doctorProfessionId:
          type: integer
          format: int64
          description: Индентификатор профессии доктора
          example: 1
        medicalOrganizationId:
          type: integer
          format: int64
          description: Индентификатор медицинского учереждения, в котором работает доктор
          example: 1
    DoctorProfession:
      description: Профессия доктора
      required:
        - id
        - name
      type: object
      properties:
        id:
          type: integer
          description: Индентификатор профессии
          format: int64
          example: 1
        name:
          type: string
          description: Навзвание профессии
          example: Терапевт
    DoctorVisit:
      description: Информация о докторе, кто создал медицинскую запись
      type: object
      properties:
        lastName:
          type: string
          example: Иванов
        firstName:
          type: string
          example: Иван
        middleName:
          type: string
          example: Иванович
        email:
          type: string
          format: email
          description: Адрес электронной почты
          example: ivanov@gmail.com
        profession:
          "$ref": "#/components/schemas/DoctorProfession"
    FileMetaData:
      description: Дополнительная информация о файле
      type: object
      properties:
        name:
          type: string
          description: Название файла
          example: ivanovBloodLab.pdf
        type:
          type: string
          description: Тип файла
          example: application/pdf
        path:
          type: string
          description: Полный путь до файла
          example: http://healthy-life/medical-record-files/download/123e4567-e89b-42d3-a456-556642440000
    MedicalRecordFile:
      description: Информация о записи, содержащая медицинские документы
      type: object
      properties:
        medicalRecordUuid:
          type: string
          description: Уникальный индентификатор медицинской записи
          format: uuid
          example: 123e4567-e89b-42d3-a456-556642440000
        title:
          type: string
          description: Названия приема
          example: Консультация врача терапевта
        dateTime:
          type: string
          description: Дата создания лабораторного исследования
          format: date-time
          example: '2019-08-31T15:49:05.630Z'
        doctor:
          "$ref": "#/components/schemas/DoctorVisit"
        medicalOrganization:
          type: string
          description: Медицинская организация, где лаборатрное исследование было совершено
          example: INVITRO
        fileMetaData:
          "$ref": "#/components/schemas/FileMetaData"
    NewLaboratoryTest:
      description: Инормация для создания нового лабораторного анализа
      required:
        - email
        - title
      type: object
      properties:
        email:
          description: Адрес электронной почты
          type: string
          format: email
          example: ivanov@gmail.com
        title:
          type: string
          description: Название лабораторного анализа
          example: Анализ крови
        paramName:
          type: string
          description: Название параметра
          example: Лейкоциты
        paramValue:
          type: string
          description: Значение параметра
          example: '12'
        normalParamValue:
          type: string
          description: Норма
          example: 4-9
        unitMeasure:
          type: string
          description: Единица измерения
          example: 10^9/L
        comment:
          type: string
          description: Комментарии доктора
          example: Следует посетить терапевта
    Visit:
      description: Информация о приеме пациента
      type: object
      properties:
        medicalRecordUuid:
          type: string
          description: Уникальный индентификатор записи
          format: uuid
          example: 123e4567-e89b-42d3-a456-556642440000
        title:
          type: string
          description: Название приема
          example: Консультация врача терапевта
        dateTime:
          type: string
          description: Дата создания приема
          format: date-time
          example: '2019-08-31T15:49:05.630Z'
        doctor:
          "$ref": "#/components/schemas/DoctorVisit"
        medicalOrganization:
          type: string
          description: Медицинская организация, где прием был совершен
          example: INVITRO
        icds:
          type: array
          description: Коды МКБ
          items:
            "$ref": "#/components/schemas/Icd"
        complaints:
          type: string
          description: Жалобы
          example: Сильный кашель и лихорадка
        diagnosis:
          type: string
          description: Диагноз
          example: Двусторонняя пневмония
        treatmentPlan:
          type: string
          description: План лечения
          example: Оставайтесь дома и принимать таблетки
        prescriptions:
          type: string
          description: Рецепты
          example: 'Антибиотик: Zithromax'
    PatientProfile:
      description: Информаиця о профиле пациента
      type: object
      properties:
        lastName:
          type: string
          description: Фамилия пациента
          example: Иванов
        firstName:
          type: string
          description: Имя пациента
          example: Иван
        middleName:
          type: string
          description: Отчество пациента
          example: Иванович
        phoneNumber:
          "$ref": "#/components/schemas/PhoneNumber"
        birthday:
          type: string
          format: date
          description: Дата рождения пациента
          example: "2000-01-30"
        gender:
          type: string
          enum:
            - UNKNOWN
            - MALE
            - FEMALE
          description: Пол пациента
          example: MALE
        email:
          type: string
          format: email
          description: Адрес электронной почты
          example: ivanov@gmail.com
        snils:
          type: string
          description: СНИЛС
          example: 12345678909
    DoctorNames:
      description: ФИО доктора, кто создал прием
      type: object
      properties:
        lastName:
          type: string
          example: Иванов
        firstName:
          type: string
          example: Иван
        middleName:
          type: string
          example: Иванович
    MedicalRecord:
      description: Информация о медицинских данных пациента
      type: object
      properties:
        medicalRecordUuid:
          type: string
          description: Уникальный индентификатор медицинской записи
          format: uuid
          example: 123e4567-e89b-42d3-a456-556642440000
        title:
          type: string
          description: Названия приема
          example: Консультация врача терапевта
        dateTime:
          type: string
          description: Дата создания лабораторного исследования
          format: date-time
          example: '2019-08-31T15:49:05.630Z'
        medicalOrganization:
          type: string
          description: Медицинская организация, где лаборатрное исследование было совершено
          example: INVITRO
        medicalRecordType:
          type: string
          description: Тип медицинской записи
          enum:
            - LAB
            - VISIT
            - FILE
          example: VISIT
        doctor:
          "$ref": "#/components/schemas/DoctorNames"
    MedicalOrganization:
      description: Информация о медицинской организации
      type: object
      properties:
        medicalOrganizationId:
          type: integer
          format: int64
          description: Индентификатор медицинской организации
          example: 1
        medicalOrganizationName:
          type: string
          example: INVITRO
          description: Название медицинской организации
    LaboratoryTest:
      type: object
      properties:
        medicalRecordUuid:
          type: string
          description: Уникальный индентификатор медицинской записи
          format: uuid
          example: 123e4567-e89b-42d3-a456-556642440000
        title:
          type: string
          description: Название лабораторного анализа
          example: Анализ крови
        dateTime:
          type: string
          description: Дата создания приема
          format: date-time
          example: '2019-08-31T15:49:05.630Z'
        doctor:
          "$ref": "#/components/schemas/DoctorVisit"
        medicalOrganization:
          type: string
          description: Медицинская организация, где прием был совершен
          example: INVITRO
        paramName:
          type: string
          description: Название параметра
          example: Лейкоциты
        paramValue:
          type: string
          description: Значение параметра
          example: '12'
        normalParamValue:
          type: string
          description: Норма
          example: 4-9
        unitMeasure:
          type: string
          description: Единица измерения
          example: 10^9/L
        comment:
          type: string
          description: Комментарии доктора
          example: Следует посетить терапевта
    DoctorProfile:
      description: Информация о профиле доктора
      type: object
      properties:
        lastName:
          type: string
          example: Иванов
        firstName:
          type: string
          example: Иван
        middleName:
          type: string
          example: Иванович
        email:
          type: string
          format: email
          description: Адрес электронной почты
          example: ivanov@gmail.com
        profession:
          "$ref": "#/components/schemas/DoctorProfession"
        birthday:
          type: string
          format: date
          description: Дата рождения пациента
          example: "2000-01-30"
        gender:
          type: string
          enum:
            - UNKNOWN
            - MALE
            - FEMALE
          description: Пол доктора
          example: MALE
        phoneNumber:
          "$ref": "#/components/schemas/PhoneNumber"
    Snils:
      description: СНИЛС
      required:
        - snils
      type: object
      properties:
        snils:
          maxLength: 11
          minLength: 11
          type: string
          description: Snils
          example: '12345678902'
    CheckResult:
      description: Результат уникальности поля
      type: object
      properties:
        exist:
          type: boolean
          description: Если поле true, то поле уже присутствует в системе
          example: true
    FullPhoneNumber:
      description: Полный номер телефона
      required:
        - fullPhoneNumber
      type: object
      properties:
        fullPhoneNumber:
          maxLength: 15
          minLength: 10
          type: string
          example: '79134567898'
          description: Полный номер телефона
    Email:
      description: Адрес электронной почты
      type: object
      properties:
        email:
          type: string
          format: email
          description: Адрес электронной почты
          example: ivanov@gmail.com
  securitySchemes:
    Basic:
      type: http
      scheme: basic
    Bearer:
      type: http
      scheme: bearer