#!/bin/bash

kubectl delete -f secret.yml
kubectl delete -f configMap.yml
kubectl delete -f storage-class.yml
kubectl delete -f cluster-setup-confmap.yml
kubectl delete -f stateful.yml
kubectl delete -f service-headless.yml
kubectl delete -f service-lb.yml
