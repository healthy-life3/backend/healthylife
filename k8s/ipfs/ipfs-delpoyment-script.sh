#!/bin/bash

kubectl apply -f secret.yml
kubectl apply -f configMap.yml
kubectl apply -f storage-class.yml
kubectl apply -f cluster-setup-confmap.yml
kubectl apply -f service-headless.yml
kubectl apply -f service-lb.yml
kubectl apply -f stateful.yml
