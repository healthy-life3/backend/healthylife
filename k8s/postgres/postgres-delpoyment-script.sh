#!/bin/bash

kubectl apply -f configMap.yml
kubectl apply -f secret.yml
kubectl apply -f storage-class.yml
kubectl apply -f pvc.yml
kubectl apply -f deployment.yml
kubectl apply -f service.yml