#!/bin/bash

kubectl delete -f configMap.yml
kubectl delete -f secret.yml
kubectl delete -f storage-class.yml
kubectl delete -f pvc.yml
kubectl delete -f deployment.yml
kubectl delete -f service.yml